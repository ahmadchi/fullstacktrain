#!/usr/local/bin/python
import asyncio
import os

from dotenv import load_dotenv
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

# from chi import ChiWebServer
from chi.modules.inventoryMod import InvoiceController
import asyncio

class ChiServerSetup:
    def __init__(self):
        self.DebugDb = False

        self.DbConfig = {
            'user': os.environ.get('DB_USER'),
            'password': os.environ.get('DB_PASSWORD'),
            'host': os.environ.get('DB_HOST_MAIN'),
            'database': os.environ.get('DB_NAME')
        }

        dbUrl = 'mysql+mysqlconnector://{user}:{password}@{host}/{database}'.format(**self.DbConfig)

        self.Engine = create_engine(dbUrl, echo=self.DebugDb)

        self.SessionMaker = sessionmaker(bind=self.Engine)
        self.gdb = self.SessionMaker()


async def main():
    load_dotenv(verbose=False)

    print("working ....1")
    context = ChiServerSetup()
    print("working ....2")
    # theServer.StartServer()
    invController = InvoiceController(context)
    invController.prepare()
    result = await invController.readInvoices()
    for res in result["data"]:
        print(res)
    # theServer.RunLoop()
    return "test"


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())


