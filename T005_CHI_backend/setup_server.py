#!/usr/local/bin/python test

from __future__ import print_function

import os
import sys

from dotenv import load_dotenv
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy_utils import database_exists, create_database, drop_database

from chi.data.base_models import ChiBase
import chi.data.inventoryMod

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


class ChiServerSetup:
    def __init__(self):
        self.DebugDb = False

        self.DbConfig = {
            'user': os.environ.get('DB_USER'),
            'password': os.environ.get('DB_PASSWORD'),
            'host': os.environ.get('DB_HOST_MAIN'),
            'database': os.environ.get('DB_NAME')
        }

        dbUrl = 'mysql+mysqlconnector://{user}:{password}@{host}/{database}'.format(**self.DbConfig)

        self.Engine = create_engine(dbUrl, echo=self.DebugDb)
        self.SessionMaker = sessionmaker(bind=self.Engine)

    def DropDatabase(self):
        if database_exists(self.Engine.url):
            eprint('Dropping existing database -> {}'.format(self.DbConfig['database']))
            drop_database(self.Engine.url)
        return

    def CreateDatabase(self):
        if not database_exists(self.Engine.url):
            eprint('Creating new database -> {}'.format(self.DbConfig['database']))
            create_database(self.Engine.url)

    def CreateSchema(self):
        eprint('Creating schema')
        ChiBase.metadata.create_all(self.Engine)
        return


if __name__ == "__main__":
    load_dotenv(verbose=False)
    s = ChiServerSetup()

    s.DropDatabase()
    s.CreateDatabase()
    s.CreateSchema()
