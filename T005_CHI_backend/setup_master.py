#!/usr/local/bin/python

from __future__ import print_function

import os
import sys

from dotenv import load_dotenv
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import func
from sqlalchemy_utils import database_exists, create_database, drop_database

from chi.data.system_models import MasterBase, MASTER_DB_NAME, Group, User, Employee


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


class ChiServerSetup:
    def __init__(self):
        self.DebugDb = False
        self.gdb = None

        self.DbConfig = {
            'user': os.environ.get('DB_USER'),
            'password': os.environ.get('DB_PASSWORD'),
            'host': os.environ.get('DB_HOST_MAIN'),
            'database': MASTER_DB_NAME
        }

        dbUrl = 'mysql+mysqlconnector://{user}:{password}@{host}/{database}'.format(**self.DbConfig)

        self.Engine = create_engine(dbUrl, echo=self.DebugDb)
        self.SessionMaker = sessionmaker(bind=self.Engine)

    def DropDatabase(self):
        if database_exists(self.Engine.url):
            eprint('Dropping existing database -> {}'.format(self.DbConfig['database']))
            drop_database(self.Engine.url)
        return

    def CreateDatabase(self):
        if not database_exists(self.Engine.url):
            eprint('Creating new database -> {}'.format(self.DbConfig['database']))
            create_database(self.Engine.url)

    def CreateSchema(self):
        eprint('Creating schema')
        MasterBase.metadata.create_all(self.Engine)
        self.gdb = self.SessionMaker()

        try:
            self.CreateUser()
            self.gdb.commit()
        finally:
            self.gdb.close()
        return

    def CreateUser(self):
        gdb = self.gdb

        # add admin
        adminUID = 'admin@chi.com'
        query = gdb.query(User).filter(User.email == adminUID)
        admin = query.one_or_none()

        if admin is None:
            eprint('Creating Admin Group')
            # add group
            group = Group()
            group.group_id = 1
            group.group_type = 'Admin'
            group.group_name = 'Administrator'
            gdb.add(group)
            gdb.flush()

            # add user
            eprint('Creating new admin user')
            admin = User()
            admin.primary_group_id = 1
            admin.email = adminUID
            admin.username = 'admin'
            admin.password = func.md5('ch!123')
            admin.is_activated = True
            admin.email_verified = True
            admin.allow_multi_login = True
            gdb.add(admin)
            gdb.flush()

            eprint('Creating new admin employee')
            employee = Employee()
            employee.employee_id = 1
            employee.user_id = admin.user_id
            employee.first_name = 'Admin'
            employee.middle_name = ''
            employee.last_name = 'CHI'
            employee.gender = 'Male'
            gdb.add(employee)
            gdb.flush()

            return True

        return False


if __name__ == "__main__":
    load_dotenv(verbose=False)
    s = ChiServerSetup()

    s.DropDatabase()
    s.CreateDatabase()
    s.CreateSchema()
