# Filename: base_models.py

import json
import random
import string
import uuid
from decimal import Decimal

from sqlalchemy import Column
from sqlalchemy import TypeDecorator, types
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.inspection import inspect
from sqlalchemy.orm.collections import InstrumentedList

ChiBase = declarative_base()

DEFAULT_TABLE_ARGS = {'mysql_engine': 'InnoDB', 'mysql_charset': 'utf8', 'mysql_collate': 'utf8_general_ci'}


def generatePK():
    return str(uuid.uuid4()).replace("-", "")


def randomString(stringLength=6):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))


class Json(TypeDecorator):
    @property
    def python_type(self):
        return object

    impl = types.Text

    def process_bind_param(self, value, dialect):
        if value is None:
            value = {}

        return json.dumps(value)

    def process_literal_param(self, value, dialect):
        return value

    def process_result_value(self, value, dialect):
        try:
            return json.loads(value)
        except (ValueError, TypeError):
            return None


class CGColumn(Column):
    def __init__(self, *args, **kwargs):
        self.secure = kwargs.pop('secure', False)

        super().__init__(*args, **kwargs)


class Serializer:
    @staticmethod
    def get_foreign_keys():
        return {}

    def serializeRoot(self):
        obj = {}

        for c in inspect(self).attrs.keys():
            val = getattr(self, c)

            if isinstance(val, InstrumentedList):
                continue
            elif hasattr(val, 'serialize'):
                continue
            elif hasattr(val, '_toJson'):
                obj[c] = val._toJson()
            elif isinstance(val, Decimal):
                obj[c] = float(val)
            else:
                obj[c] = val

        return obj

    def serialize(self, originalClass=None, level=0, childAdded=None, rootRels=None, maxLevel=2):
        obj = {}

        if originalClass is None:
            originalClass = self.__class__
        if childAdded is None:
            childAdded = []
        if rootRels is None:
            rootRels = self.__mapper__.relationships._data.keys()

        allKeys = inspect(self).attrs.keys()
        allKeys = sorted(allKeys)

        for c in allKeys:
            val = getattr(self, c)

            if isinstance(val, InstrumentedList):
                continue
            elif hasattr(val, 'serialize'):
                # if val.__class__ != originalClass and level < maxLevel:
                if level < maxLevel:
                    if val.__class__ == originalClass:
                        obj[c] = val.serializeRoot()
                    elif level == 0:
                        childAdded.append(c)
                        obj2 = val.serialize(originalClass, level + 1, childAdded, rootRels, maxLevel)
                        obj[c] = obj2
                    elif c not in childAdded and c not in rootRels:
                        childAdded.append(c)
                        obj2 = val.serialize(originalClass, level + 1, childAdded, rootRels, maxLevel)
                        obj[c] = obj2

            elif hasattr(val, '_toJson'):
                obj[c] = val._toJson()
            elif isinstance(val, Decimal):
                obj[c] = float(val)
            else:
                obj[c] = val

        return obj

    @staticmethod
    def serialize_list(l):
        return [m.serialize() for m in l]


class CurrentSession:
    def __init__(self, **kwargs):
        self.session_id = kwargs.pop('session_id', None)
        self.device_id = kwargs.pop('device_id', None)
        self.user_id = kwargs.pop('user_id', None)

        self.group_id = kwargs.pop('group_id', None)
        self.group_type = kwargs.pop('group_type', None)
        self.Group = kwargs.pop('group', None)
        self.settings = kwargs.pop('settings', {})

        self.employee_id = kwargs.pop('employee_id', None)
        self.full_name = kwargs.pop('full_name', None)
        self.first_name = kwargs.pop('first_name', None)
        self.first_name_enc = kwargs.pop('first_name_enc', None)
        self.last_name = kwargs.pop('last_name', None)
        self.last_name_enc = kwargs.pop('last_name_enc', None)
