from sqlalchemy import ForeignKey, Integer, String, BigInteger, Text, Boolean, ForeignKeyConstraint
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, column_property
from sqlalchemy.sql import func

from .base_models import CGColumn, Serializer

MASTER_DB_NAME = 'chi-master'
DEFAULT_MASTER_TABLE_ARGS = {'schema': MASTER_DB_NAME, 'mysql_engine': 'InnoDB', 'mysql_charset': 'utf8',
                             'mysql_collate': 'utf8_general_ci'}

MasterBase = declarative_base()


class Group(MasterBase, Serializer):
    __tablename__ = 'groups'
    __primary_key__ = "group_id"
    __table_args__ = DEFAULT_MASTER_TABLE_ARGS

    group_id = CGColumn(Integer, primary_key=True, nullable=False, autoincrement=True)
    group_type = CGColumn(String(64), nullable=False)
    group_name = CGColumn(String(64), nullable=False, unique=True)
    anonymize = CGColumn(Boolean, default=False, nullable=False)

    permissions = CGColumn(Text(16777215), nullable=True)

    can_home_visit = CGColumn(Boolean, default=False, nullable=False)
    can_book_appointment = CGColumn(Boolean, default=False, nullable=False)

    date_added = CGColumn(BigInteger, nullable=False, default=func.unix_timestamp())
    date_updated = CGColumn(BigInteger, nullable=False, default=func.unix_timestamp(), onupdate=func.unix_timestamp())


class User(MasterBase, Serializer):
    __tablename__ = 'users'
    __primary_key__ = "user_id"
    __table_args__ = DEFAULT_MASTER_TABLE_ARGS

    user_id = CGColumn(Integer, primary_key=True, nullable=False, autoincrement=True)

    primary_group_id = CGColumn(ForeignKey(Group.group_id, onupdate='RESTRICT', ondelete='RESTRICT'), nullable=False)
    primary_group = relationship(Group, remote_side=Group.group_id, foreign_keys=primary_group_id, lazy='select')

    email = CGColumn(String(64), nullable=True)
    mobile = CGColumn(String(32), nullable=True)
    username = CGColumn(String(64), nullable=False, unique=True)
    password = CGColumn(String(32), nullable=False, secure=True)

    private_key = CGColumn(Text, nullable=True)

    last_success_login = CGColumn(BigInteger, nullable=True)
    last_failed_login = CGColumn(BigInteger, nullable=True)
    failed_login_attempts = CGColumn(Integer, default=0, nullable=False)
    is_activated = CGColumn(Boolean, default=True, nullable=False)
    email_verified = CGColumn(Boolean, default=False, nullable=False)
    phone_verified = CGColumn(Boolean, default=False, nullable=False)
    use_two_factor = CGColumn(Boolean, default=False, nullable=False)
    allow_multi_login = CGColumn(Boolean, default=False, nullable=False)
    is_blocked = CGColumn(Boolean, default=False, nullable=False)
    block_reason = CGColumn(String(64), nullable=True)

    date_added = CGColumn(BigInteger, nullable=False, default=func.unix_timestamp())
    date_updated = CGColumn(BigInteger, nullable=False, default=func.unix_timestamp(), onupdate=func.unix_timestamp())


class UserGroup(MasterBase, Serializer):
    __tablename__ = 'user_groups'
    __table_args__ = DEFAULT_MASTER_TABLE_ARGS

    membership_id = CGColumn(Integer, primary_key=True, nullable=False, autoincrement=True)
    user_id = CGColumn(ForeignKey(User.user_id, onupdate='RESTRICT', ondelete='RESTRICT'), nullable=False)
    user = relationship(User, remote_side=User.user_id, foreign_keys=user_id, lazy='select')

    group_id = CGColumn(ForeignKey(Group.group_id, onupdate='RESTRICT', ondelete='RESTRICT'), nullable=False)
    group = relationship(Group, remote_side=Group.group_id, foreign_keys=group_id, lazy='select')


class Employee(MasterBase, Serializer):
    __tablename__ = "employees"
    __table_args__ = (ForeignKeyConstraint(columns=('created_by_id',),
                                           refcolumns=['chi-master.employees.employee_id'],
                                           onupdate='RESTRICT', ondelete='RESTRICT', use_alter=True),
                      ForeignKeyConstraint(columns=('last_edited_by_id',),
                                           refcolumns=['chi-master.employees.employee_id'],
                                           onupdate='RESTRICT', ondelete='RESTRICT', use_alter=True),
                      DEFAULT_MASTER_TABLE_ARGS)
    __primary_key__ = 'employee_id'

    employee_id = CGColumn(Integer, primary_key=True, autoincrement=True, nullable=False)

    user_id = CGColumn(ForeignKey(User.user_id, onupdate='RESTRICT', ondelete='RESTRICT'), nullable=False)
    user = relationship(User, remote_side=User.user_id, foreign_keys=user_id, lazy='select')

    employee_no = CGColumn(String(64), nullable=True)
    external_id = CGColumn(String(128), nullable=True, index=True)

    image = CGColumn(String(256), nullable=True)
    first_name = CGColumn(String(64), nullable=False)
    middle_name = CGColumn(String(64), nullable=True)
    last_name = CGColumn(String(64), nullable=False)
    extra_name = CGColumn(String(64), nullable=True)

    full_name = column_property(first_name + " " + last_name)
    gender = CGColumn(String(64), nullable=False)
    birth_date = CGColumn(BigInteger, nullable=True)

    mobile_phone = CGColumn(String(64), nullable=True)
    land_line_phone = CGColumn(String(64), nullable=True)
    passport_id = CGColumn(String(64), nullable=True)
    nic = CGColumn(String(64), nullable=True)
    address_1 = CGColumn(String(512), nullable=True)
    address_2 = CGColumn(String(512), nullable=True)
    zip_code = CGColumn(String(64), nullable=True)

    emergency_contact_person = CGColumn(String(64), nullable=True)
    emergency_contact_phone = CGColumn(String(64), nullable=True)

    employment_start_date = CGColumn(BigInteger, nullable=True)
    employment_end_date = CGColumn(BigInteger, nullable=True)

    other_notes = CGColumn(String(64), nullable=True)

    is_deleted = CGColumn(Boolean, nullable=False, default=False)
    delete_comments = CGColumn(String(512), nullable=True)

    created_by_id = CGColumn(Integer, nullable=True)
    last_edited_by_id = CGColumn(Integer, nullable=True)

    date_added = CGColumn(BigInteger, nullable=False, default=func.unix_timestamp())
    date_updated = CGColumn(BigInteger, nullable=False, default=func.unix_timestamp(), onupdate=func.unix_timestamp())
