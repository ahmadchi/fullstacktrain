from sqlalchemy import BigInteger, Boolean, ForeignKey, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func

from .base_models import ChiBase, DEFAULT_TABLE_ARGS, CGColumn, Serializer
from .system_models import Employee


class PurchaseRequisition(ChiBase, Serializer):
    __tablename__ = "acc_purchase_requisitions"
    __primary_key__ = "purchase_requisition_number_id"
    __table_args__ = DEFAULT_TABLE_ARGS

    purchase_requisition_number_id = CGColumn(Integer, primary_key=True, autoincrement=True, nullable=False)
    status = CGColumn(String(128), default="Pending")
    remarks = CGColumn(String(1024), nullable=True)

    is_deleted = CGColumn(Boolean, nullable=False, default=False)
    delete_comments = CGColumn(String(512), nullable=True)

    created_by_id = CGColumn(ForeignKey(Employee.employee_id, onupdate='RESTRICT', ondelete='RESTRICT'), nullable=False)
    created_by = relationship(Employee, remote_side=Employee.employee_id, foreign_keys=created_by_id, lazy='select')

    last_edited_by_id = CGColumn(ForeignKey(Employee.employee_id, onupdate='RESTRICT', ondelete='RESTRICT'),
                                 nullable=False)

    date_added = CGColumn(BigInteger, nullable=False, default=func.unix_timestamp())
    date_updated = CGColumn(BigInteger, nullable=False, default=func.unix_timestamp(), onupdate=func.unix_timestamp())
