from sqlalchemy import BigInteger, Boolean, ForeignKey, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func

from .base_models import ChiBase, DEFAULT_TABLE_ARGS, CGColumn, Serializer
from .item_batches import ItemBatch
from .stores import Store
from .system_models import Employee


class InventoryStock(ChiBase, Serializer):
    __tablename__ = "acc_inventory_stocks"
    __primary_key__ = "inventory_stock_id"
    __table_args__ = DEFAULT_TABLE_ARGS

    inventory_stock_id = CGColumn(Integer, primary_key=True, autoincrement=True, nullable=False)
    inventory_batch_id = CGColumn(ForeignKey(ItemBatch.item_batches_id, onupdate='RESTRICT', ondelete='RESTRICT'))
    inventory_batch = relationship(ItemBatch, remote_side=ItemBatch.item_batches_id, foreign_keys=inventory_batch_id,
                                   lazy='select')
    quantity = CGColumn(Integer, nullable=False)
    store_id = CGColumn(ForeignKey(Store.store_id, onupdate='RESTRICT', ondelete='RESTRICT'))
    store = relationship(Store, remote_side=Store.store_id, foreign_keys=store_id, lazy='select')

    is_deleted = CGColumn(Boolean, nullable=False, default=False)
    delete_comments = CGColumn(String(512), nullable=True)

    created_by_id = CGColumn(ForeignKey(Employee.employee_id, onupdate='RESTRICT', ondelete='RESTRICT'), nullable=False)
    created_by = relationship(Employee, remote_side=Employee.employee_id, foreign_keys=created_by_id, lazy='select')

    last_edited_by_id = CGColumn(ForeignKey(Employee.employee_id, onupdate='RESTRICT', ondelete='RESTRICT'),
                                 nullable=False)

    date_added = CGColumn(BigInteger, nullable=False, default=func.unix_timestamp())
    date_updated = CGColumn(BigInteger, nullable=False, default=func.unix_timestamp(), onupdate=func.unix_timestamp())
