from sqlalchemy import BigInteger, Boolean, ForeignKey, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func

from .base_models import ChiBase, DEFAULT_TABLE_ARGS, CGColumn, Serializer
from .purchase_orders import PurchaseOrder
from .stores import Store
from .suppliers import Supplier
from .system_models import Employee


class GoodsReceivedNote(ChiBase, Serializer):
    __tablename__ = "acc_goods_received_notes"
    __primary_key__ = "grn_number_id"
    __table_args__ = DEFAULT_TABLE_ARGS

    grn_number_id = CGColumn(Integer, primary_key=True, autoincrement=True, nullable=False)
    status = CGColumn(String(128))
    remarks = CGColumn(String(1024))
    supplier_id = CGColumn(ForeignKey(Supplier.supplier_id, onupdate='RESTRICT', ondelete='RESTRICT'))
    supplier = relationship(Supplier, remote_side=Supplier.supplier_id, foreign_keys=supplier_id, lazy='select')
    supplier_chalan_number = CGColumn(String(128))
    supplier_chalan_date = CGColumn(BigInteger, default=func.unix_timestamp(), onupdate=func.unix_timestamp(), )
    payment_mode = CGColumn(String(128))
    transaction_type = CGColumn(String(128))
    purchase_order_id = CGColumn(
        ForeignKey(PurchaseOrder.purchase_order_number_id, onupdate='RESTRICT', ondelete='RESTRICT'))
    purchase_order = relationship(PurchaseOrder, remote_side=PurchaseOrder.purchase_order_number_id,
                                  foreign_keys=purchase_order_id, lazy='select')
    store_id = CGColumn(ForeignKey(Store.store_id, onupdate='RESTRICT', ondelete='RESTRICT'))
    store = relationship(Store, remote_side=Store.store_id, foreign_keys=store_id, lazy='select')

    is_deleted = CGColumn(Boolean, nullable=False, default=False)
    delete_comments = CGColumn(String(512), nullable=True)

    created_by_id = CGColumn(ForeignKey(Employee.employee_id, onupdate='RESTRICT', ondelete='RESTRICT'), nullable=False)
    created_by = relationship(Employee, remote_side=Employee.employee_id, foreign_keys=created_by_id, lazy='select')

    last_edited_by_id = CGColumn(ForeignKey(Employee.employee_id, onupdate='RESTRICT', ondelete='RESTRICT'),
                                 nullable=False)

    date_added = CGColumn(BigInteger, nullable=False, default=func.unix_timestamp())
    date_updated = CGColumn(BigInteger, nullable=False, default=func.unix_timestamp(), onupdate=func.unix_timestamp())
