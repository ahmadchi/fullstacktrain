from sqlalchemy import BigInteger, Boolean, ForeignKey, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func

from .base_models import ChiBase, DEFAULT_TABLE_ARGS, CGColumn, Serializer
from .stores import Store
from .system_models import Employee


class SalesInvoice(ChiBase, Serializer):
    __tablename__ = "acc_sales_invoices"
    __primary_key__ = "invoice_number"
    __table_args__ = DEFAULT_TABLE_ARGS
    # TODO: total and bill discount
    invoice_number = CGColumn(Integer, primary_key=True, autoincrement=True, nullable=False)
    status = CGColumn(String(128))
    remarks = CGColumn(String(1024))
    is_return = CGColumn(Boolean, default=0)
    is_credit = CGColumn(Boolean, default=0)
    return_against = CGColumn(Integer)
    payment_mode = CGColumn(String(128))
    store_id = CGColumn(ForeignKey(Store.store_id, onupdate='RESTRICT', ondelete='RESTRICT'))
    store = relationship(Store, remote_side=Store.store_id, foreign_keys=store_id, lazy='select')

    is_deleted = CGColumn(Boolean, nullable=False, default=False)
    delete_comments = CGColumn(String(512), nullable=True)

    created_by_id = CGColumn(ForeignKey(Employee.employee_id, onupdate='RESTRICT', ondelete='RESTRICT'), nullable=False)
    created_by = relationship(Employee, remote_side=Employee.employee_id, foreign_keys=created_by_id, lazy='select')

    last_edited_by_id = CGColumn(ForeignKey(Employee.employee_id, onupdate='RESTRICT', ondelete='RESTRICT'),
                                 nullable=False)

    date_added = CGColumn(BigInteger, nullable=False, default=func.unix_timestamp())
    date_updated = CGColumn(BigInteger, nullable=False, default=func.unix_timestamp(), onupdate=func.unix_timestamp())
