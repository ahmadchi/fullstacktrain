from sqlalchemy import BigInteger, Boolean, ForeignKey, Integer, String, Float
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func

from .base_models import ChiBase, DEFAULT_TABLE_ARGS, CGColumn, Serializer
from .item_batches import ItemBatch
from .sales_invoices import SalesInvoice
from .system_models import Employee


class InvoiceItem(ChiBase, Serializer):
    __tablename__ = "acc_invoice_items"
    __primary_key__ = "invoice_item_id"
    __table_args__ = DEFAULT_TABLE_ARGS

    invoice_item_id = CGColumn(Integer, primary_key=True, autoincrement=True, nullable=False)
    invoice_id = CGColumn(ForeignKey(SalesInvoice.invoice_number, onupdate='RESTRICT', ondelete='RESTRICT'))
    invoice = relationship(SalesInvoice, remote_side=SalesInvoice.invoice_number, foreign_keys=invoice_id,
                           lazy='select')
    inventory_batch_id = CGColumn(ForeignKey(ItemBatch.item_batches_id, onupdate='RESTRICT', ondelete='RESTRICT'))
    inventory_batch = relationship(ItemBatch, remote_side=ItemBatch.item_batches_id, foreign_keys=inventory_batch_id,
                                   lazy='select')
    quantity = CGColumn(Integer, nullable=False)
    sale_price = CGColumn(Float, nullable=False, default=None)
    discount = CGColumn(Float, default=None)
    discount_type = CGColumn(String(128))

    is_deleted = CGColumn(Boolean, nullable=False, default=False)
    delete_comments = CGColumn(String(512), nullable=True)

    created_by_id = CGColumn(ForeignKey(Employee.employee_id, onupdate='RESTRICT', ondelete='RESTRICT'), nullable=False)
    created_by = relationship(Employee, remote_side=Employee.employee_id, foreign_keys=created_by_id, lazy='select')

    last_edited_by_id = CGColumn(ForeignKey(Employee.employee_id, onupdate='RESTRICT', ondelete='RESTRICT'),
                                 nullable=False)

    date_added = CGColumn(BigInteger, nullable=False, default=func.unix_timestamp())
    date_updated = CGColumn(BigInteger, nullable=False, default=func.unix_timestamp(), onupdate=func.unix_timestamp())
