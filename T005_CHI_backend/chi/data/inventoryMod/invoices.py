from sqlalchemy import BigInteger, Boolean, ForeignKey, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func

from ..base_models import ChiBase, DEFAULT_TABLE_ARGS, CGColumn, Serializer
from ..system_models import Employee
from .customers import Customer


class Invoice(ChiBase, Serializer):
    __tablename__ = "acc_invoices"
    __primary_key__ = "invoice_id"
    __table_args__ = DEFAULT_TABLE_ARGS

    invoice_id = CGColumn(Integer, primary_key = True)
    custid = CGColumn(ForeignKey(Customer.customer_id))
    invno = CGColumn(Integer)
    amount = CGColumn(Integer)
    #customer = relationship(Customer, back_populates = "invoices1")

    created_by_id = CGColumn(ForeignKey(Employee.employee_id, onupdate='RESTRICT', ondelete='RESTRICT'), nullable=False)
    created_by = relationship(Employee, remote_side=Employee.employee_id, foreign_keys=created_by_id, lazy='select')

    last_edited_by_id = CGColumn(ForeignKey(Employee.employee_id, onupdate='RESTRICT', ondelete='RESTRICT'),
                                 nullable=False)

    date_added = CGColumn(BigInteger, nullable=False, default=func.unix_timestamp())
    date_updated = CGColumn(BigInteger, nullable=False, default=func.unix_timestamp(), onupdate=func.unix_timestamp())

#Customer.invoices1 = relationship(Invoice, order_by = Invoice.invoice_id, back_populates = "acc_customers")
