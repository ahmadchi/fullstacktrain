from sqlalchemy import BigInteger, Boolean, ForeignKey, Integer, String, Float
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func

from .base_models import ChiBase, DEFAULT_TABLE_ARGS, CGColumn, Serializer
from .goods_received_notes import GoodsReceivedNote
from .item_batches import ItemBatch
from .system_models import Employee


class GRNItem(ChiBase, Serializer):
    __tablename__ = "acc_grn_items"
    __primary_key__ = "grn_item_id"
    __table_args__ = DEFAULT_TABLE_ARGS

    grn_item_id = CGColumn(Integer, primary_key=True, autoincrement=True, nullable=False)
    grn_id = CGColumn(ForeignKey(GoodsReceivedNote.grn_number_id, onupdate='RESTRICT', ondelete='RESTRICT'))
    grn = relationship(GoodsReceivedNote, remote_side=GoodsReceivedNote.grn_number_id, foreign_keys=grn_id,
                       lazy='select')
    inventory_batch_id = CGColumn(ForeignKey(ItemBatch.item_batches_id, onupdate='RESTRICT', ondelete='RESTRICT'))
    inventory_batch = relationship(ItemBatch, remote_side=ItemBatch.item_batches_id, foreign_keys=inventory_batch_id,
                                   lazy='select')
    quantity = CGColumn(Integer, nullable=False)
    bonus = CGColumn(Integer, nullable=False)
    purchase_price = CGColumn(Float, nullable=False, default=None)
    discount = CGColumn(Float, default=None)
    discount_type = CGColumn(String(128))
    sales_tax = CGColumn(Float, default=None)
    sales_tax_type = CGColumn(String(128))
    loose_packing = CGColumn(Boolean, default=None)

    is_deleted = CGColumn(Boolean, nullable=False, default=False)
    delete_comments = CGColumn(String(512), nullable=True)

    created_by_id = CGColumn(ForeignKey(Employee.employee_id, onupdate='RESTRICT', ondelete='RESTRICT'), nullable=False)
    created_by = relationship(Employee, remote_side=Employee.employee_id, foreign_keys=created_by_id, lazy='select')

    last_edited_by_id = CGColumn(ForeignKey(Employee.employee_id, onupdate='RESTRICT', ondelete='RESTRICT'),
                                 nullable=False)

    date_added = CGColumn(BigInteger, nullable=False, default=func.unix_timestamp())
    date_updated = CGColumn(BigInteger, nullable=False, default=func.unix_timestamp(), onupdate=func.unix_timestamp())
