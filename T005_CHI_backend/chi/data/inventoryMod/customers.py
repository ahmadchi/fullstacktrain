from sqlalchemy import BigInteger, Boolean, ForeignKey, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func

from ..base_models import ChiBase, DEFAULT_TABLE_ARGS, CGColumn, Serializer
from ..system_models import Employee


class Customer(ChiBase, Serializer):
    __tablename__ = "acc_customers"
    __primary_key__ = "customer_id"
    __table_args__ = DEFAULT_TABLE_ARGS

    customer_id = CGColumn(Integer, primary_key = True)
    name = CGColumn(String(40))
    address = CGColumn(String(40))
    # address2 = Column(String(40))
    email = CGColumn(String(40))


    # formula_id = CGColumn(Integer, primary_key=True, autoincrement=True, nullable=False)
    # formula_name = CGColumn(String(128))
    # description = CGColumn(String(1024))

    # is_deleted = CGColumn(Boolean, nullable=False, default=False)
    # delete_comments = CGColumn(String(512), nullable=True)

    created_by_id = CGColumn(ForeignKey(Employee.employee_id, onupdate='RESTRICT', ondelete='RESTRICT'), nullable=False)
    created_by = relationship(Employee, remote_side=Employee.employee_id, foreign_keys=created_by_id, lazy='select')

    last_edited_by_id = CGColumn(ForeignKey(Employee.employee_id, onupdate='RESTRICT', ondelete='RESTRICT'),
                                 nullable=False)

    date_added = CGColumn(BigInteger, nullable=False, default=func.unix_timestamp())
    date_updated = CGColumn(BigInteger, nullable=False, default=func.unix_timestamp(), onupdate=func.unix_timestamp())
