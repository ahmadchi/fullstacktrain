from sqlalchemy import BigInteger, Boolean, ForeignKey, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func

from .base_models import ChiBase, DEFAULT_TABLE_ARGS, CGColumn, Serializer
from .stores import Store
from .system_models import Employee


class StockTransfer(ChiBase, Serializer):
    __tablename__ = "acc_stock_transfers"
    __primary_key__ = "stock_transfer_no"
    __table_args__ = DEFAULT_TABLE_ARGS

    stock_transfer_no = CGColumn(Integer, primary_key=True, autoincrement=True, nullable=False)
    from_store_id = CGColumn(ForeignKey(Store.store_id, onupdate='RESTRICT', ondelete='RESTRICT'))
    from_store = relationship(Store, remote_side=Store.store_id, foreign_keys=from_store_id, lazy='select')
    to_store_id = CGColumn(ForeignKey(Store.store_id, onupdate='RESTRICT', ondelete='RESTRICT'))
    to_store = relationship(Store, remote_side=Store.store_id, foreign_keys=to_store_id, lazy='select')
    remarks = CGColumn(String(1024))

    is_deleted = CGColumn(Boolean, nullable=False, default=False)
    delete_comments = CGColumn(String(512), nullable=True)

    created_by_id = CGColumn(ForeignKey(Employee.employee_id, onupdate='RESTRICT', ondelete='RESTRICT'), nullable=False)
    created_by = relationship(Employee, remote_side=Employee.employee_id, foreign_keys=created_by_id, lazy='select')

    last_edited_by_id = CGColumn(ForeignKey(Employee.employee_id, onupdate='RESTRICT', ondelete='RESTRICT'),
                                 nullable=False)

    date_added = CGColumn(BigInteger, nullable=False, default=func.unix_timestamp())
    date_updated = CGColumn(BigInteger, nullable=False, default=func.unix_timestamp(), onupdate=func.unix_timestamp())
