from sqlalchemy import BigInteger, Boolean, ForeignKey, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func

from .base_models import ChiBase, DEFAULT_TABLE_ARGS, CGColumn, Serializer
from .inventories import Inventory
from .suppliers import Supplier
from .system_models import Employee


class InventorySupplier(ChiBase, Serializer):
    __tablename__ = "acc_inventory_suppliers"
    __primary_key__ = "id"
    __table_args__ = DEFAULT_TABLE_ARGS

    id = CGColumn(Integer, primary_key=True, autoincrement=True, nullable=False)
    inventory_id = CGColumn(ForeignKey(Inventory.code, onupdate='RESTRICT', ondelete='RESTRICT'))
    inventory = relationship(Inventory, remote_side=Inventory.code, foreign_keys=inventory_id, lazy='select')
    supplier_id = CGColumn(ForeignKey(Supplier.supplier_id, onupdate='RESTRICT', ondelete='RESTRICT'))
    supplier = relationship(Supplier, remote_side=Supplier.supplier_id, foreign_keys=supplier_id, lazy='select')

    is_deleted = CGColumn(Boolean, nullable=False, default=False)
    delete_comments = CGColumn(String(512), nullable=True)

    created_by_id = CGColumn(ForeignKey(Employee.employee_id, onupdate='RESTRICT', ondelete='RESTRICT'), nullable=False)
    created_by = relationship(Employee, remote_side=Employee.employee_id, foreign_keys=created_by_id, lazy='select')

    last_edited_by_id = CGColumn(ForeignKey(Employee.employee_id, onupdate='RESTRICT', ondelete='RESTRICT'),
                                 nullable=False)

    date_added = CGColumn(BigInteger, nullable=False, default=func.unix_timestamp())
    date_updated = CGColumn(BigInteger, nullable=False, default=func.unix_timestamp(), onupdate=func.unix_timestamp())
