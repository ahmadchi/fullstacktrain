from sqlalchemy import BigInteger, Boolean, ForeignKey, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func

from .base_models import ChiBase, DEFAULT_TABLE_ARGS, CGColumn, Serializer
from .purchase_requisitions import PurchaseRequisition
from .suppliers import Supplier
from .system_models import Employee


class PurchaseOrder(ChiBase, Serializer):
    __tablename__ = "acc_purchase_orders"
    __primary_key__ = "purchase_order_number_id"
    __table_args__ = DEFAULT_TABLE_ARGS

    purchase_order_number_id = CGColumn(Integer, primary_key=True, autoincrement=True, nullable=False)
    status = CGColumn(String(128))
    remarks = CGColumn(String(1024))
    supplier_id = CGColumn(ForeignKey(Supplier.supplier_id, onupdate='RESTRICT', ondelete='RESTRICT'))
    supplier = relationship(Supplier, remote_side=Supplier.supplier_id, foreign_keys=supplier_id, lazy='select')
    supplier_quotation_number = CGColumn(String(128))
    supplier_quotation_date = CGColumn(BigInteger, default=func.unix_timestamp(), onupdate=func.unix_timestamp(), )
    payment_mode = CGColumn(String(128))
    document_mode = CGColumn(String(128))
    purchase_requisition_id = CGColumn(
        ForeignKey(PurchaseRequisition.purchase_requisition_number_id, onupdate='RESTRICT', ondelete='RESTRICT'))
    purchase_requisition = relationship(PurchaseRequisition,
                                        remote_side=PurchaseRequisition.purchase_requisition_number_id,
                                        foreign_keys=purchase_requisition_id, lazy='select')

    is_deleted = CGColumn(Boolean, nullable=False, default=False)
    delete_comments = CGColumn(String(512), nullable=True)

    created_by_id = CGColumn(ForeignKey(Employee.employee_id, onupdate='RESTRICT', ondelete='RESTRICT'), nullable=False)
    created_by = relationship(Employee, remote_side=Employee.employee_id, foreign_keys=created_by_id, lazy='select')

    last_edited_by_id = CGColumn(ForeignKey(Employee.employee_id, onupdate='RESTRICT', ondelete='RESTRICT'),
                                 nullable=False)

    date_added = CGColumn(BigInteger, nullable=False, default=func.unix_timestamp())
    date_updated = CGColumn(BigInteger, nullable=False, default=func.unix_timestamp(), onupdate=func.unix_timestamp())
