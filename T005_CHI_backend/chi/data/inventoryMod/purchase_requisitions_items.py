from sqlalchemy import BigInteger, Boolean, ForeignKey, Integer, String, Float
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func

from .base_models import ChiBase, DEFAULT_TABLE_ARGS, CGColumn, Serializer
from .inventories import Inventory
from .purchase_requisitions import PurchaseRequisition
from .system_models import Employee


class PurchaseRequisitionItem(ChiBase, Serializer):
    __tablename__ = "acc_purchase_requisitions_items"
    __primary_key__ = "purchase_requisition_items_id"
    __table_args__ = DEFAULT_TABLE_ARGS

    purchase_requisition_items_id = CGColumn(Integer, primary_key=True, autoincrement=True, nullable=False)
    purchase_requisition_id = CGColumn(
        ForeignKey(PurchaseRequisition.purchase_requisition_number_id, onupdate='RESTRICT', ondelete='RESTRICT'))
    purchase_requisition = relationship(PurchaseRequisition,
                                        remote_side=PurchaseRequisition.purchase_requisition_number_id,
                                        foreign_keys=purchase_requisition_id, lazy='select')
    inventory_id = CGColumn(ForeignKey(Inventory.code, onupdate='RESTRICT', ondelete='RESTRICT'))
    inventory = relationship(Inventory, remote_side=Inventory.code, foreign_keys=inventory_id, lazy='select')
    quantity = CGColumn(Integer, nullable=False)
    est_rate = CGColumn(Float, default=None)
    est_arrival_date = CGColumn(BigInteger, default=func.unix_timestamp(), onupdate=func.unix_timestamp(), )
    approved_quantity = CGColumn(Integer)

    is_deleted = CGColumn(Boolean, nullable=False, default=False)
    delete_comments = CGColumn(String(512), nullable=True)

    created_by_id = CGColumn(ForeignKey(Employee.employee_id, onupdate='RESTRICT', ondelete='RESTRICT'), nullable=False)
    created_by = relationship(Employee, remote_side=Employee.employee_id, foreign_keys=created_by_id, lazy='select')

    last_edited_by_id = CGColumn(ForeignKey(Employee.employee_id, onupdate='RESTRICT', ondelete='RESTRICT'),
                                 nullable=False)

    date_added = CGColumn(BigInteger, nullable=False, default=func.unix_timestamp())
    date_updated = CGColumn(BigInteger, nullable=False, default=func.unix_timestamp(), onupdate=func.unix_timestamp())
