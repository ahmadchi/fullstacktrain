from sqlalchemy import BigInteger, Boolean, ForeignKey, Integer, String, Float
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func

from .base_models import ChiBase, DEFAULT_TABLE_ARGS, CGColumn, Serializer
from .inventories import Inventory
from .system_models import Employee


class ItemBatch(ChiBase, Serializer):
    __tablename__ = "acc_item_batches"
    __primary_key__ = "item_batches_id"
    __table_args__ = DEFAULT_TABLE_ARGS

    item_batches_id = CGColumn(Integer, primary_key=True, autoincrement=True, nullable=False)
    inventory_id = CGColumn(ForeignKey(Inventory.code, onupdate='RESTRICT', ondelete='RESTRICT'))
    inventory = relationship(Inventory, remote_side=Inventory.code, foreign_keys=inventory_id, lazy='select')
    batch_no = CGColumn(String(128))
    expire_date = CGColumn(BigInteger)
    selling_price = CGColumn(Float)
    remarks = CGColumn(String(1024))

    is_deleted = CGColumn(Boolean, nullable=False, default=False)
    delete_comments = CGColumn(String(512), nullable=True)

    created_by_id = CGColumn(ForeignKey(Employee.employee_id, onupdate='RESTRICT', ondelete='RESTRICT'), nullable=False)
    created_by = relationship(Employee, remote_side=Employee.employee_id, foreign_keys=created_by_id, lazy='select')

    last_edited_by_id = CGColumn(ForeignKey(Employee.employee_id, onupdate='RESTRICT', ondelete='RESTRICT'),
                                 nullable=False)

    date_added = CGColumn(BigInteger, nullable=False, default=func.unix_timestamp())
    date_updated = CGColumn(BigInteger, nullable=False, default=func.unix_timestamp(), onupdate=func.unix_timestamp())
