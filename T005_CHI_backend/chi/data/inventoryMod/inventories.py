from sqlalchemy import BigInteger, Boolean, ForeignKey, Integer, String, Float
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func

from .base_models import ChiBase, DEFAULT_TABLE_ARGS, CGColumn, Serializer
from .manufacturers import Manufacturer
from .system_models import Employee
from .units import Unit


class Inventory(ChiBase, Serializer):
    __tablename__ = "acc_inventories"
    __primary_key__ = "code"
    __table_args__ = DEFAULT_TABLE_ARGS

    code = CGColumn(Integer, primary_key=True, autoincrement=True, nullable=False)
    baecode = CGColumn(String(128), unique=True)
    inventory_name = CGColumn(String(128), nullable=False, unique=True)
    description = CGColumn(String(1024))
    manufacturer_id = CGColumn(ForeignKey(Manufacturer.manufacturer_id, onupdate='RESTRICT', ondelete='RESTRICT'))
    manufacturer = relationship(Manufacturer, remote_side=Manufacturer.manufacturer_id, foreign_keys=manufacturer_id,
                                lazy='select')
    purchase_unit_id = CGColumn(ForeignKey(Unit.unit_id, onupdate='RESTRICT', ondelete='RESTRICT'))
    purchase_unit = relationship(Unit, remote_side=Unit.unit_id, foreign_keys=purchase_unit_id, lazy='select')
    storage_unit_id = CGColumn(ForeignKey(Unit.unit_id, onupdate='RESTRICT', ondelete='RESTRICT'))
    storage_unit = relationship(Unit, remote_side=Unit.unit_id, foreign_keys=storage_unit_id, lazy='select')
    selling_unit_id = CGColumn(ForeignKey(Unit.unit_id, onupdate='RESTRICT', ondelete='RESTRICT'))
    selling_unit = relationship(Unit, remote_side=Unit.unit_id, foreign_keys=selling_unit_id, lazy='select')
    purchase_to_storage = CGColumn(Float, nullable=False, default=None)
    storage_to_selling = CGColumn(Float, nullable=False, default=None)
    expiry_date_enabled = CGColumn(Boolean, nullable=False, default=None)
    narcotics = CGColumn(Boolean, nullable=False, default=None)

    is_deleted = CGColumn(Boolean, nullable=False, default=False)
    delete_comments = CGColumn(String(512), nullable=True)

    created_by_id = CGColumn(ForeignKey(Employee.employee_id, onupdate='RESTRICT', ondelete='RESTRICT'), nullable=False)
    created_by = relationship(Employee, remote_side=Employee.employee_id, foreign_keys=created_by_id, lazy='select')

    last_edited_by_id = CGColumn(ForeignKey(Employee.employee_id, onupdate='RESTRICT', ondelete='RESTRICT'),
                                 nullable=False)

    date_added = CGColumn(BigInteger, nullable=False, default=func.unix_timestamp())
    date_updated = CGColumn(BigInteger, nullable=False, default=func.unix_timestamp(), onupdate=func.unix_timestamp())
