import concurrent.futures
import logging
import os
import signal
import sys
from logging import Logger

import tornado.ioloop
import tornado.options
import tornado.web
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

import chi.modules.inventoryMod
from chi.common import HealthHandler
from chi.web import MainWebHandler, GeneralApiHandler


class Deployment:
    pass


class ChiWebServer(tornado.web.Application):
    def __init__(self):
        self.SessionMaker = None
        self.SessionReadOnly = None

        self.DebugDb = True if os.environ.get('DB_DEBUG', '0') == '1' else False

        logging.basicConfig(
            level=logging.INFO,
            format='%(asctime)15s |%(levelname)8s | %(name)10s: %(message)s',
            stream=sys.stderr,
        )
        self.Logger: Logger = logging.getLogger('server')

        self.ListenPort = int(os.environ.get('API_LISTEN_PORT', '8080'))
        self.LoadConfig()

        self.Registry = chi.modules.inventoryMod.get_registry()
        self.Executor = concurrent.futures.ThreadPoolExecutor(max_workers=8)

        self.SystemGroups = {}
        self.SystemSettings = {}

        handlers = self.GetHandlers()

        settings = {
            'debug': True,
            'serve_traceback': True,
            'compress_response': True,
            'template_path': 'templates/',
            'static_url_prefix': '/abcd/',
            'static_path': 'assets',
            'autoreload': True,
            'xsrf_cookies': False,
        }

        tornado.web.Application.__init__(self, handlers, **settings)
        return

    def StartServer(self):
        xheaders = True
        self.listen(
            self.ListenPort,
            address='127.0.0.1',
            xheaders=xheaders
        )
        return

    def RunLoop(self):
        signal.signal(signal.SIGTERM, self.SigHandler)
        signal.signal(signal.SIGINT, self.SigHandler)

        self.Logger.info('Web Api Server is ready @ {}'.format(self.ListenPort))
        tornado.ioloop.IOLoop.current().start()

        self.Logger.info('Web Api Server is Stopped')
        return

    def GetHandlers(self):
        self.Logger.info('Making Handlers')
        handlers = [
            (r"/health", HealthHandler),

            (r"/api/(.*)/(.*)", GeneralApiHandler),
            (r"/(.*)", MainWebHandler),
        ]

        return handlers

    def Shutdown(self):
        self.Logger.info('Trying to Stop the Server')
        tornado.ioloop.IOLoop.instance().stop()

        return

    def SigHandler(self, sigName, frame):
        self.Logger.info('Got SIG {}, Web Api Server will stop'.format(sigName))
        tornado.ioloop.IOLoop.instance().add_callback_from_signal(self.Shutdown)

        return

    def LoadConfig(self):
        db_config = {
            'user': os.environ.get('DB_USER'),
            'password': os.environ.get('DB_PASSWORD'),
            'host': os.environ.get('DB_HOST_MAIN'),
            'database': os.environ.get('DB_NAME')
        }

        dbUrl = 'mysql+mysqlconnector://{user}:{password}@{host}/{database}'.format(**db_config)

        db_engine = create_engine(dbUrl, echo=self.DebugDb, pool_recycle=3600)
        self.SessionMaker = sessionmaker(bind=db_engine)

        db_config['host'] = os.environ.get('DB_HOST_READER')

        db_url = 'mysql+mysqlconnector://{user}:{password}@{host}/{database}'.format(**db_config)
        engine = create_engine(db_url, echo=self.DebugDb, pool_recycle=3600)
        self.SessionReadOnly = sessionmaker(bind=engine)

        self.SiteCode = os.environ.get('SITE_CODE')
        self.BaseUrl = os.environ.get('CHARMS_PORTAL_URL')

        self.DataStorageRoot = os.environ.get('DATA_STORAGE_ROOT')
        self.UserFilesRoot = '{}/uploads'.format(self.DataStorageRoot)

        self.Deployment = Deployment()
        self.Deployment.DeploymentName = os.environ.get('DEPLOYMENT_NAME')
        self.Deployment.DeploymentVersion = os.environ.get('DEPLOYMENT_VERSION')
        self.Deployment.ClientLogo = os.environ.get('CLIENT_LOGO')
