from chi.common import CGBaseController
from chi.data import InventoryStock


class InventoryStockController(CGBaseController):
    def prepare(self):
        self.Model = InventoryStock
        self.Slug = 'inventory-stocks'

        super().prepare()
