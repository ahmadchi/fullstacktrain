from chi.common import CGBaseController
from chi.data import Manufacturer


class ManufacturerController(CGBaseController):
    def prepare(self):
        self.Model = Manufacturer
        self.Slug = 'manufacturers'

        super().prepare()
