from chi.common import CGBaseController
from chi.data import InvoiceItem


class InvoiceItemController(CGBaseController):
    def prepare(self):
        self.Model = InvoiceItem
        self.Slug = 'invoice-items'

        super().prepare()
