from chi.common import CGBaseController
from chi.data import InventoryLabel


class InventoryLabelController(CGBaseController):
    def prepare(self):
        self.Model = InventoryLabel
        self.Slug = 'inventory-labels'

        super().prepare()
