from chi.common import CGBaseController
from chi.data import InventoryFormula


class InventoryFormulaController(CGBaseController):
    def prepare(self):
        self.Model = InventoryFormula
        self.Slug = 'inventory-formulas'

        super().prepare()
