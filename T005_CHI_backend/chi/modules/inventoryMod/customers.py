from chi.common import CGBaseController
from chi.data.inventoryMod import Customer


class CustomerController(CGBaseController):
    def prepare(self):
        self.Model = Customer
        self.Slug = 'customers'

        super().prepare()

