from operator import and_

from sqlalchemy import func

from chi.common import CGBaseController
from chi.common.exceptions import QuantityExceedsLimit, RequiredFieldMissing, ExceptionWithMessage
from chi.data import SalesInvoice, ItemBatch, InventoryStock, InvoiceItem


def check_if_item_already_exists(items_list, item):
    for index, single_item in enumerate(items_list):
        if single_item['inventory_id'] == item.inventory_batch.inventory_id:
            return index


class SalesInvoiceController(CGBaseController):
    def prepare(self):
        self.Model = SalesInvoice
        self.Slug = 'sales-invoices'

        super().prepare()

    async def Create(self):
        if self.Data['is_return']:
            if not self.Data['return_against']:
                raise RequiredFieldMissing('return_against')
        else:
            if self.Data['return_against'] is not None:
                del self.Data['return_against']

        invoice_id = await super().Create()
        invoice_id = invoice_id['data']

        for item in self.Data['items']:
            if not self.Data['is_return']:
                total_available_quantity = self.gdb.query(func.sum(InventoryStock.quantity).label('quantity')).join(
                    ItemBatch).filter(and_(ItemBatch.inventory_id == item['inventory_id'],
                                           InventoryStock.store_id == self.Data['store_id'])).first()

                if total_available_quantity.quantity and total_available_quantity.quantity >= item['quantity']:
                    stocks = self.gdb.query(InventoryStock).join(ItemBatch) \
                        .filter(
                        and_(
                            ItemBatch.inventory_id == item['inventory_id'],
                            InventoryStock.store_id == self.Data['store_id']
                        )) \
                        .filter(InventoryStock.quantity > 0) \
                        .order_by(ItemBatch.item_batches_id).all()

                    remaining_quantity = item['quantity']

                    for stock in stocks:
                        if remaining_quantity > 0:
                            quantity_to_update = remaining_quantity
                            if stock.quantity < quantity_to_update:
                                quantity_to_update = stock.quantity

                            self.gdb.query(InventoryStock) \
                                .filter(
                                and_(
                                    InventoryStock.store_id == self.Data['store_id'],
                                    InventoryStock.inventory_batch_id == stock.inventory_batch.item_batches_id
                                )) \
                                .update({InventoryStock.quantity: InventoryStock.quantity - quantity_to_update})

                            remaining_quantity = remaining_quantity - quantity_to_update

                            self.add_new_invoice_item(item, invoice_id, quantity_to_update,
                                                      stock.inventory_batch.item_batches_id)

                        else:
                            break
                else:
                    raise QuantityExceedsLimit(item['inventory_id'])
            else:
                remaining_quantity = item['quantity']
                total_quantity_sale = self.gdb.query(func.sum(InvoiceItem.quantity).label('quantity')) \
                    .join(ItemBatch) \
                    .filter(
                    and_(
                        InvoiceItem.invoice_id == self.Data['return_against'],
                        ItemBatch.inventory_id == item['inventory_id']
                    )).first()

                returned_quantity = self.gdb.query(func.sum(InvoiceItem.quantity).label('quantity')) \
                    .join(SalesInvoice) \
                    .join(ItemBatch) \
                    .filter(SalesInvoice.is_return == 1) \
                    .filter(
                    and_(
                        SalesInvoice.return_against == self.Data['return_against'],
                        ItemBatch.inventory_id == item['inventory_id']
                    )).first()

                if returned_quantity.quantity is None:
                    returned_quantity = 0
                else:
                    returned_quantity = returned_quantity.quantity

                if total_quantity_sale.quantity is None:
                    total_quantity_sale = 0
                else:
                    total_quantity_sale = total_quantity_sale.quantity

                total_returnable_quantity = total_quantity_sale - returned_quantity

                if total_returnable_quantity > 0:
                    return_against_items = self.gdb.query(InvoiceItem).join(ItemBatch) \
                        .filter(
                        and_(
                            InvoiceItem.invoice_id == self.Data['return_against'],
                            ItemBatch.inventory_id == item['inventory_id']
                        )) \
                        .order_by(InvoiceItem.invoice_item_id).all()

                    for return_against_item in return_against_items:
                        quantity_to_update = remaining_quantity

                        if quantity_to_update > return_against_item.quantity:
                            quantity_to_update = return_against_item.quantity

                        self.gdb.query(InventoryStock) \
                            .filter(
                            and_(
                                InventoryStock.store_id == self.Data['store_id'],
                                InventoryStock.inventory_batch_id == return_against_item.inventory_batch.item_batches_id
                            )) \
                            .update({InventoryStock.quantity: InventoryStock.quantity + quantity_to_update})

                        remaining_quantity = remaining_quantity - quantity_to_update

                        self.add_new_invoice_item(item, invoice_id, quantity_to_update,
                                                  return_against_item.inventory_batch.item_batches_id)
                else:
                    raise QuantityExceedsLimit(item['inventory_id'])

    def add_new_invoice_item(self, item, invoice_id, quantity, inventory_batch_id):
        new_invoice_item = InvoiceItem()
        new_invoice_item.quantity = quantity
        new_invoice_item.inventory_batch_id = inventory_batch_id
        new_invoice_item.invoice_id = invoice_id
        new_invoice_item.discount = item['discount']
        new_invoice_item.discount_type = item['discount_type']
        new_invoice_item.sale_price = item['sale_price']
        new_invoice_item.created_by_id = self.Session.employee_id
        new_invoice_item.last_edited_by_id = self.Session.employee_id

        self.gdb.add(new_invoice_item)

    async def Single(self):
        data = await super().Single()
        data = data['data']

        if 'items' in self.Data:
            items = []
            if 'inventory_id' in self.Data['items']:
                self.Data['items'].remove('inventory_id')
            if 'batches' in self.Data['items']:
                self.Data['items'].remove('batches')

            for item in self.gdb.query(InvoiceItem).filter(InvoiceItem.invoice_id == self.Data['oid']).all():
                batches = []
                # TODO: batches should also be excludeable from frontend
                is_item_exists = check_if_item_already_exists(items, item)
                if is_item_exists is None:
                    batches.append({
                        'batch_no': item.inventory_batch.batch_no,
                        'expire_date': item.inventory_batch.expire_date,
                        'selling_price': item.inventory_batch.selling_price,
                        'remarks': item.inventory_batch.remarks,
                    })
                    if len(self.Data['items']) == 0:
                        items.append({
                            'inventory_id': item.inventory_batch.inventory_id,
                            'quantity': item.quantity,
                            'discount': item.discount,
                            'sale_price': item.sale_price,
                            'batches': batches
                        })
                    else:
                        try:
                            item_to_append = {
                                'inventory_id': item.inventory_batch.inventory_id,
                                'batches': batches
                            }
                            for key in self.Data['items']:
                                item_to_append[key] = getattr(item, key)

                            items.append(item_to_append)
                        except AttributeError as error:
                            raise ExceptionWithMessage(error, 1205)
                else:
                    if 'quantity' in self.Data['items']:
                        items[is_item_exists]['quantity'] += item.quantity
                    items[is_item_exists]['batches'].append({
                        'batch_no': item.inventory_batch.batch_no,
                        'expire_date': item.inventory_batch.expire_date,
                        'selling_price': item.inventory_batch.selling_price,
                        'remarks': item.inventory_batch.remarks,
                    })

            if len(items) > 0:
                data['items'] = items

        return {'data': data}
