from operator import itemgetter

from chi.common import CGBaseController
from chi.common.exceptions import RequiredFieldMissing, ExceptionWithMessage, QuantityShouldBeEqual
from chi.data import PurchaseOrder, PurchaseOrderItems, PurchaseRequisitionItem


class PurchaseOrderController(CGBaseController):
    def prepare(self):
        self.Model = PurchaseOrder
        self.Slug = 'purchase-orders'

        super().prepare()

    def check_if_items_in_purchase_requisition(self):
        pr_items = self.gdb.query(PurchaseRequisitionItem)\
            .filter(PurchaseRequisitionItem.purchase_requisition_id == self.Data['purchase_requisition_id'])\
            .order_by(PurchaseRequisitionItem.inventory_id).all()

        index = 0
        for pr_item in pr_items:
            if not (self.Data['items'][index]['inventory_id'] == pr_item.inventory_id and self.Data['items'][index]['quantity'] == pr_item.approved_quantity):
                raise QuantityShouldBeEqual(self.Data['items'][index]['inventory_id'])
            index = index + 1

    def check_if_item_already_exists(self):
        temp_items_ids = []
        temp_items = []
        for index, single_item in enumerate(self.Data['items']):
            if single_item['inventory_id'] not in temp_items_ids:
                temp_items_ids.append(single_item['inventory_id'])
                temp_items.append(single_item)
            else:
                temp_items[temp_items_ids.index(single_item['inventory_id'])]['quantity'] += single_item[
                    'quantity']

        temp_items.sort(key=itemgetter('inventory_id'), reverse=False)
        self.Data['items'] = temp_items

    async def Create(self):
        self.Data['status'] = "PENDING"
        if self.Data['document_mode'] == 'PURCHASE_REQUISITION':
            if not self.Data['purchase_requisition_id']:
                raise RequiredFieldMissing('purchase_requisition_id')

        purchase_order_id = await super().Create()
        purchase_order_id = purchase_order_id['data']

        self.check_if_item_already_exists()
        if self.Data['document_mode'] == 'PURCHASE_REQUISITION':
            self.check_if_items_in_purchase_requisition()
        self.add_po_items(purchase_order_id)

    async def Update(self):
        if 'document_mode' in self.Data:
            del self.Data['document_mode']
        if 'purchase_requisition_id' in self.Data:
            del self.Data['purchase_requisition_id']

        purchase_order_id = await super().Update()
        purchase_order_id = purchase_order_id['data']

        if 'items' in self.Data:
            if self.Data['document_mode'] == 'PURCHASE_REQUISITION':
                self.check_if_items_in_purchase_requisition()
            self.gdb.query(PurchaseOrderItems).filter(
                PurchaseOrderItems.purchase_order_id == purchase_order_id).delete()
            self.add_po_items(purchase_order_id)

    def add_po_items(self, purchase_order_id):
        items_list = self.Data['items']

        for item in items_list:
            new_po_item = PurchaseOrderItems()
            new_po_item.purchase_order_id = purchase_order_id
            new_po_item.inventory_id = item['inventory_id']
            new_po_item.quantity = item['quantity']
            new_po_item.rate = item['rate']
            new_po_item.discount_type = item['discount_type']
            new_po_item.discount = item['discount']
            new_po_item.est_arrival_date = item['est_arrival_date']
            new_po_item.sales_tax = item['sales_tax']
            new_po_item.sales_tax_type = item['sales_tax_type']
            new_po_item.created_by_id = self.Session.employee_id
            new_po_item.last_edited_by_id = self.Session.employee_id
            self.gdb.add(new_po_item)

    async def Single(self):
        is_items = False
        if 'items' in self.Data['columns']:
            self.Data['columns'].remove('items')
            is_items = True

        data = await super().Single()
        data = data['data']

        if is_items:
            items = []
            for item in self.gdb.query(PurchaseOrderItems).filter(
                    PurchaseOrderItems.purchase_order_id == self.Data['oid']).all():

                if len(self.Data['items']) == 0:
                    items.append({
                        'inventory_id': item.inventory_id,
                        'quantity': item.quantity,
                        'rate': item.rate,
                        'discount_type': item.discount_type,
                        'discount': item.discount,
                        'sales_tax': item.sales_tax,
                        'sales_tax_type': item.sales_tax_type,
                        'est_arrival_date': item.est_arrival_date
                    })
                else:
                    try:
                        item_to_append = {}
                        for key in self.Data['items']:
                            item_to_append[key] = getattr(item, key)

                        items.append(item_to_append)
                    except AttributeError as error:
                        raise ExceptionWithMessage(error, 1205)

            if len(items) > 0:
                data['items'] = items

        return {'data': data}
