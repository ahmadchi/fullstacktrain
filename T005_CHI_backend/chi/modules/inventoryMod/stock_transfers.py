from sqlalchemy import func, and_

from chi.common import CGBaseController
from chi.common.exceptions import QuantityExceedsLimit, ExceptionWithMessage
from chi.data import StockTransfer, InventoryStock, ItemBatch, StockTransferItem


class StockTransferController(CGBaseController):
    def prepare(self):
        self.Model = StockTransfer
        self.Slug = 'stock-transfers'

        super().prepare()

    async def Create(self):
        items = self.Data['items']

        stock_transfer_items = []
        for item in items:
            total_available_quantity = self.gdb.query(func.sum(InventoryStock.quantity).label('quantity')).join(
                ItemBatch).filter(and_(ItemBatch.inventory_id == item['inventory_id'],
                                       InventoryStock.store_id == self.Data['from_store_id'])).first()

            if total_available_quantity.quantity and total_available_quantity.quantity >= item['quantity']:
                stocks = self.gdb.query(InventoryStock).join(ItemBatch).filter(
                    and_(ItemBatch.inventory_id == item['inventory_id'],
                         InventoryStock.store_id == self.Data['from_store_id'], InventoryStock.quantity > 0)).order_by(
                    ItemBatch.item_batches_id).all()
                remaining_quantity = item['quantity']

                for stock in stocks:
                    if remaining_quantity > 0:
                        quantity_to_update = remaining_quantity
                        if stock.quantity < quantity_to_update:
                            quantity_to_update = stock.quantity
                        is_stock_updated = self.gdb.query(InventoryStock).filter(
                            and_(InventoryStock.store_id == self.Data['to_store_id'],
                                 InventoryStock.inventory_batch_id == stock.inventory_batch.item_batches_id)).update(
                            {InventoryStock.quantity: InventoryStock.quantity + quantity_to_update})

                        if not is_stock_updated:
                            new_inventory_stock = InventoryStock()
                            new_inventory_stock.quantity = quantity_to_update
                            new_inventory_stock.inventory_batch_id = stock.inventory_batch.item_batches_id
                            new_inventory_stock.store_id = self.Data['to_store_id']
                            new_inventory_stock.created_by_id = self.Session.employee_id
                            new_inventory_stock.last_edited_by_id = self.Session.employee_id

                            self.gdb.add(new_inventory_stock)

                        remaining_quantity = remaining_quantity - quantity_to_update

                        self.gdb.query(InventoryStock) \
                            .filter(
                            and_(
                                InventoryStock.store_id == self.Data['from_store_id'],
                                InventoryStock.inventory_stock_id == stock.inventory_stock_id
                            )) \
                            .update({InventoryStock.quantity: InventoryStock.quantity - quantity_to_update})

                        stock_transfer_items.append({
                            'quantity': quantity_to_update,
                            'inventory_batch_id': stock.inventory_batch.item_batches_id
                        })
            else:
                raise QuantityExceedsLimit(item['inventory_id'])

        stock_transfer_id = await super().Create()
        stock_transfer_id = stock_transfer_id['data']

        if items is not None:
            for item in stock_transfer_items:
                new_stock_transfer_item = StockTransferItem()
                new_stock_transfer_item.quantity = item['quantity']
                new_stock_transfer_item.stock_transfer_id = stock_transfer_id
                new_stock_transfer_item.inventory_batch_id = item['inventory_batch_id']
                new_stock_transfer_item.created_by_id = self.Session.employee_id
                new_stock_transfer_item.last_edited_by_id = self.Session.employee_id

                self.gdb.add(new_stock_transfer_item)

    async def Delete(self):
        stock_transfer_id = self.Data['oid']

        stock_transfer = self.gdb.query(StockTransfer).filter(
            StockTransfer.stock_transfer_no == stock_transfer_id).first()

        stock_transfer_items = self.gdb.query(StockTransferItem).filter(
            StockTransferItem.stock_transfer_id == stock_transfer_id).all()

        for item in stock_transfer_items:
            self.gdb.query(InventoryStock) \
                .filter(and_(InventoryStock.store_id == stock_transfer.from_store_id,
                             InventoryStock.inventory_batch_id == item.inventory_batch_id)) \
                .update({InventoryStock.quantity: InventoryStock.quantity + item.quantity})

        await super().Delete()

    async def Single(self):
        data = await super().Single()
        data = data['data']

        if 'items' in self.Data:
            items = []
            for item in self.gdb.query(StockTransferItem).filter(
                    StockTransferItem.purchase_return_item_id == self.Data['oid']).all():

                if len(self.Data['items']) == 0:
                    items.append({
                        'inventory_batch_id': item.inventory_batch_id,
                        'quantity': item.quantity
                    })
                else:
                    try:
                        item_to_append = {}
                        for key in self.Data['items']:
                            item_to_append[key] = getattr(item, key)

                        items.append(item_to_append)
                    except AttributeError as error:
                        raise ExceptionWithMessage(error, 1205)

            if len(items) > 0:
                data['items'] = items

        return {'data': data}
