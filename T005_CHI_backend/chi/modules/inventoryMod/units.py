from operator import and_

from chi.common import CGBaseController
from chi.data import Unit


class UnitController(CGBaseController):
    def prepare(self):
        self.Model = Unit
        self.Slug = 'units'

        super().prepare()

    async def Create(self):
        if self.Data['is_default']:
            self.gdb.query(Unit).filter(Unit.is_default == 1).update({Unit.is_default: 0})

        await super().Create()

    async def Update(self):
        if self.Data['is_default']:
            self.gdb.query(Unit).filter(Unit.is_default == 1).update({Unit.is_default: 0})

        await super().Update()

    async def Delete(self):
        unit = self.gdb.query(Unit).filter(Unit.unit_id == self.Data['oid']).first()

        if unit.is_default:
            random_unit = self.gdb.query(Unit).filter(
                and_(Unit.is_deleted == 0, Unit.unit_id != unit.unit_id)).first()
            if random_unit is not None:
                self.gdb.query(Unit).filter(Unit.unit_id == random_unit.unit_id).update(
                    {Unit.is_default: 1})

        await super().Delete()
