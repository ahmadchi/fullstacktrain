from chi.common import CGBaseController
from chi.data.inventoryMod import Formula


class FormulaController(CGBaseController):
    def prepare(self):
        self.Model = Formula
        self.Slug = 'formulas'

        super().prepare()

