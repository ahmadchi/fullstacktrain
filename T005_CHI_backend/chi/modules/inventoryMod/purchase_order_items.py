from chi.common import CGBaseController
from chi.data import PurchaseOrderItems


class PurchaseOrderItemsController(CGBaseController):
    def prepare(self):
        self.Model = PurchaseOrderItems
        self.Slug = 'purchase-order-items'

        super().prepare()
