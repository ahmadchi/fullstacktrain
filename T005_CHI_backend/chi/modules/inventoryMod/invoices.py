from chi.common import CGBaseController
from chi.data.inventoryMod import Invoice
from chi.data.inventoryMod import Customer


class InvoiceController(CGBaseController):
    def prepare(self):
        self.Model = Invoice
        self.Slug = 'invoices'
        self.PublicMethods = ['readInvoices']  # customAPI

        super().prepare()

    async def readInvoices(self):
        qResult = self.gdb.query(self.Model, Customer).outerjoin(Customer).all()
        # print(qResult)
        outResult = []
        #print(self.Data)
        # print(self.Data[''])

        for inv, cust in qResult:
            inv = inv.serialize()
            if cust == None:
                cust = "NULL"
            else:
                cust = cust.serialize()
            outResult.append((inv, cust))
            #print(outResult)
        resp = {
            'data': outResult,
            'total_records': len(qResult),
        }
        #val = await super().List()
        return resp

    # async def readInvoices(self):
    #     qResult = self.gdb.query(self.Model, Customer).outerjoin(Customer).all()
    #     print(qResult)
    #     outResult = []
    #     print(self.Data)
    #     # print(self.Data[''])
    #     for inv, cust in qResult:
    #         inv = inv.serialize()
    #         # cust = cust.serialize()
    #         outResult.append((inv, cust))
    #         print(outResult)

