from chi.common import CGBaseController
from chi.data import PurchaseRequisitionItem


class PurchaseRequisitionItemController(CGBaseController):
    def prepare(self):
        self.Model = PurchaseRequisitionItem
        self.Slug = 'purchase-requisitions-items'

        super().prepare()
