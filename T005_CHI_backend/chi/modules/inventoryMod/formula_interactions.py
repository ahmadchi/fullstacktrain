from chi.common import CGBaseController
from chi.data.inventoryMod import FormulaInteraction


class FormulaInteractionController(CGBaseController):
    def prepare(self):
        self.Model = FormulaInteraction
        self.Slug = 'formula-interactions'

        super().prepare()
