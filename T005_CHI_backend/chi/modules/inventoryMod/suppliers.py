from chi.common import CGBaseController
from chi.data import Supplier


class SupplierController(CGBaseController):
    def prepare(self):
        self.Model = Supplier
        self.Slug = 'suppliers'

        super().prepare()
