from .formula_interactions import FormulaInteractionController
from .formulas import FormulaController
from .customers import CustomerController
from .invoices import InvoiceController
# from .goods_received_notes import GoodsReceivedNoteController
# from .grn_items import GRNItemController
# from .inventories import InventoryController
# from .inventory_formulas import InventoryFormulaController
# from .inventory_labels import InventoryLabelController
# from .inventory_stocks import InventoryStockController
# from .inventory_suppliers import InventorySupplierController
# from .invoice_items import InvoiceItemController
# from .item_batches import ItemBatchController
# from .labels import LabelController
# from .manufacturers import ManufacturerController
# from .purchase_order_items import PurchaseOrderItemsController
# from .purchase_orders import PurchaseOrderController
# from .purchase_requisitions import PurchaseRequisitionController
# from .purchase_requisitions_items import PurchaseRequisitionItemController
# from .purchase_return_items import PurchaseReturnItemController
# from .purchase_returns import PurchaseReturnController
# from .sales_invoices import SalesInvoiceController
# from .stock__transfer_items import StockTransferItemController
# from .stock_transfers import StockTransferController
# from .stores import StoreController
# from .suppliers import SupplierController
# from .units import UnitController


def get_registry():
    reg = {
        'formulas': FormulaController,
        'formula-interactions': FormulaInteractionController,
        'customers': CustomerController,
        'invoices': InvoiceController,
    }

    return reg
