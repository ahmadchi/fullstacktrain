from chi.common import CGBaseController
from chi.data import Label


class LabelController(CGBaseController):
    def prepare(self):
        self.Model = Label
        self.Slug = 'labels'

        super().prepare()
