from chi.common import CGBaseController
from chi.data import Inventory, InventoryFormula, InventoryLabel
from chi.common.exceptions import ExceptionWithMessage, QuantityExceedsLimit

class InventoryController(CGBaseController):
    def prepare(self):
        self.Model = Inventory
        self.Slug = 'inventories'

        super().prepare()

    # Override create function
    async def Create(self):
        id = await super().Create()
        self.add_many2many_details(id['data'])

    async def Update(self):
        id = await super().Update()
        id = id['data']
        # Delete old Inventory Formulas
        self.gdb.query(InventoryFormula).filter(InventoryFormula.inventory_id == id).delete()
        # Delete old Inventory Labels
        self.gdb.query(InventoryLabel).filter(InventoryLabel.inventory_id == id).delete()
        # Update leaf tables
        self.add_many2many_details(id)

    def add_many2many_details(self, id):

        # fetch InventoryFormula list from post data
        list = self.Data['items']
        for item in list:
            form = InventoryFormula()
            form.inventory_id = id
            form.formula_id = item['formula_id']
            form.created_by_id = self.Session.employee_id
            form.last_edited_by_id = self.Session.employee_id
            self.gdb.add(form)

        # fetch InventoryLabel list from post data
        list = self.Data['items']
        for item in list:
            form = InventoryLabel()
            form.inventory_id = id
            form.label_id = item['label_id']
            form.created_by_id = self.Session.employee_id
            form.last_edited_by_id = self.Session.employee_id
            self.gdb.add(form)

    async def Single(self):
        is_formulas = False
        if 'formulas' in self.Data['columns']:
            self.Data['columns'].remove('formulas')
            is_formulas = True

        is_labels = False
        if 'labels' in self.Data['columns']:
            self.Data['columns'].remove('labels')
            is_labels = True

        data = await super().Single()
        data = data['data']

        if is_formulas:
            formulas = []
            for formula in self.gdb.query(InventoryFormula).filter(
                    InventoryFormula.inventory_id == self.Data['oid']).all():
                formulas.append(
                    {'formula_name': formula.formula.formula_name, 'formula_id': formula.formula.formula_id})

            if len(formulas) > 0:
                data['formulas'] = formulas

        if is_labels:
            labels = []
            for label in self.gdb.query(InventoryLabel).filter(InventoryLabel.inventory_id == self.Data['oid']).all():
                labels.append({'label_name': label.label.label_name, 'label_id': label.label.label_id})

            if len(labels) > 0:
                data['labels'] = labels
        return {'data': data}
