from sqlalchemy import and_

from chi.common import CGBaseController
from chi.common.exceptions import ExceptionWithMessage
from chi.data import PurchaseReturn, PurchaseReturnItem, InventoryStock


class PurchaseReturnController(CGBaseController):
    def prepare(self):
        self.Model = PurchaseReturn
        self.Slug = 'purchase-returns'

        super().prepare()

    def check_if_item_already_exists(self):
        temp_items_ids = []
        temp_items = []
        for index, single_item in enumerate(self.Data['items']):
            if single_item['inventory_batch_id'] not in temp_items_ids:
                temp_items_ids.append(single_item['inventory_batch_id'])
                temp_items.append(single_item)
            else:
                temp_items[temp_items_ids.index(single_item['inventory_batch_id'])]['quantity'] += single_item[
                    'quantity']

        self.Data['items'] = temp_items

    async def Create(self):
        pr_id = await super().Create()
        pr_id = pr_id['data']

        self.check_if_item_already_exists()
        self.add_pr_items(pr_id)

    async def Update(self):
        pass

    async def Delete(self):
        pr_id = await super().Delete()
        pr_id = pr_id['data']
        pr_id = pr_id[0]
        pr_items = self.gdb.query(PurchaseReturnItem).filter(PurchaseReturnItem.purchase_return_id == pr_id).all()

        for item in pr_items:
            self.gdb.query(InventoryStock).filter(InventoryStock.inventory_batch_id == item.inventory_batch_id).update(
                {InventoryStock.quantity: InventoryStock.quantity + item.quantity})

    def add_pr_items(self, pr_id):
        items_list = self.Data['items']
        for item in items_list:
            new_pr_item = PurchaseReturnItem()
            new_pr_item.purchase_return_id = pr_id
            new_pr_item.inventory_batch_id = item['inventory_batch_id']
            new_pr_item.quantity = item['quantity']
            new_pr_item.return_price = item['return_price']
            new_pr_item.created_by_id = self.Session.employee_id
            new_pr_item.last_edited_by_id = self.Session.employee_id

            self.gdb.add(new_pr_item)

            self.gdb.query(InventoryStock) \
                .filter(
                and_(
                    InventoryStock.inventory_batch_id == item['inventory_batch_id'],
                    InventoryStock.store_id == self.Data['store_id']
                )).update({InventoryStock.quantity: InventoryStock.quantity - item['quantity']})

    async def Single(self):
        data = await super().Single()
        data = data['data']

        if 'items' in self.Data:
            items = []
            for item in self.gdb.query(PurchaseReturnItem).filter(
                    PurchaseReturnItem.purchase_return_id == self.Data['oid']).all():

                if len(self.Data['items']) == 0:
                    items.append({
                        'inventory_batch_id': item.inventory_batch_id,
                        'quantity': item.quantity,
                        'return_price': item.return_price
                    })
                else:
                    try:
                        item_to_append = {}
                        for key in self.Data['items']:
                            item_to_append[key] = getattr(item, key)

                        items.append(item_to_append)
                    except AttributeError as error:
                        raise ExceptionWithMessage(error, 1205)

            if len(items) > 0:
                data['items'] = items

        return {'data': data}
