from chi.common import CGBaseController
from chi.data import PurchaseReturnItem


class PurchaseReturnItemController(CGBaseController):
    def prepare(self):
        self.Model = PurchaseReturnItem
        self.Slug = 'purchase-return-items'

        super().prepare()
