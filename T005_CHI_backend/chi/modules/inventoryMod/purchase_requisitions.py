from chi.common import CGBaseController
from chi.common.exceptions import ExceptionWithMessage, QuantityExceedsLimit
from chi.data import PurchaseRequisition, PurchaseRequisitionItem


class PurchaseRequisitionController(CGBaseController):
    def prepare(self):
        self.Model = PurchaseRequisition
        self.Slug = 'purchase-requisitions'

        super().prepare()

    def check_if_item_already_exists(self):
        temp_items_ids = []
        temp_items = []
        for index, single_item in enumerate(self.Data['items']):
            approved_quantity = None
            if 'approved_quantity' in single_item:
                approved_quantity = single_item['approved_quantity']

            if approved_quantity is None or single_item['approved_quantity'] > 0:
                single_item['approved_quantity'] = 0

            if single_item['inventory_id'] not in temp_items_ids:
                temp_items_ids.append(single_item['inventory_id'])
                temp_items.append(single_item)
            else:
                temp_items[temp_items_ids.index(single_item['inventory_id'])]['quantity'] += single_item[
                    'quantity']

        self.Data['items'] = temp_items

    async def Create(self):
        self.Data['status'] = "PENDING"
        pr_id = await super().Create()
        pr_id = pr_id['data']
        record = await self.dbqh.executeGetFirstQuery(PurchaseRequisition, self.gdb.query(PurchaseRequisition).filter(PurchaseRequisition.purchase_requisition_number_id == pr_id))
        print(record)
        self.check_if_item_already_exists()
        self.add_pr_items(pr_id)

    def check_approved_quantity(self):
        for item in self.Data['items']:
            if item['approved_quantity'] > item['quantity']:
                raise QuantityExceedsLimit(item['inventory_id'])

    async def Update(self):
        pr_id = await super().Update()
        pr_id = pr_id['data']

        if 'items' in self.Data:
            self.check_approved_quantity()
            self.gdb.query(PurchaseRequisitionItem).filter(
                PurchaseRequisitionItem.purchase_requisition_id == pr_id).delete()
            self.add_pr_items(pr_id)

    def add_pr_items(self, pr_id):
        items_list = self.Data['items']
        for item in items_list:
            new_pr_item = PurchaseRequisitionItem()
            new_pr_item.purchase_requisition_id = pr_id
            new_pr_item.inventory_id = item['inventory_id']
            new_pr_item.quantity = item['quantity']
            new_pr_item.est_rate = item['est_rate']
            new_pr_item.est_arrival_date = item['est_arrival_date']
            new_pr_item.approved_quantity = item['approved_quantity']
            new_pr_item.created_by_id = self.Session.employee_id
            new_pr_item.last_edited_by_id = self.Session.employee_id
            self.gdb.add(new_pr_item)

    async def Single(self):
        data = await super().Single()
        data = data['data']

        if 'items' in self.Data:
            items = []
            # TODO: make this query async
            for item in self.gdb.query(PurchaseRequisitionItem).filter(
                    PurchaseRequisitionItem.purchase_requisition_id == self.Data['oid']).all():

                if len(self.Data['items']) == 0:
                    items.append({
                        'inventory_id': item.inventory_id,
                        'quantity': item.quantity,
                        'est_rate': item.est_rate,
                        'est_arrival_date': item.est_arrival_date,
                        'approved_quantity': item.approved_quantity,
                        'created_by_id': item.created_by_id,
                        'last_edited_by_id': item.last_edited_by_id
                    })
                else:
                    try:
                        item_to_append = {}
                        for key in self.Data['items']:
                            item_to_append[key] = getattr(item, key)

                        items.append(item_to_append)
                    except AttributeError as error:
                        raise ExceptionWithMessage(error, 1205)

            if len(items) > 0:
                data['items'] = items

        return {'data': data}
