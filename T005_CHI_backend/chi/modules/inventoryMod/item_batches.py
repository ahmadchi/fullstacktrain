from chi.common import CGBaseController
from chi.data import ItemBatch


class ItemBatchController(CGBaseController):
    def prepare(self):
        self.Model = ItemBatch
        self.Slug = 'item-batches'

        super().prepare()
