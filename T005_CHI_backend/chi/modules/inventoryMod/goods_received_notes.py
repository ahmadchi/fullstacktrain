from sqlalchemy import and_

from chi.common import CGBaseController
from chi.common.exceptions import RequiredFieldMissing
from chi.data import GoodsReceivedNote, GRNItem, InventoryStock, ItemBatch, Inventory


class GoodsReceivedNoteController(CGBaseController):
    def prepare(self):
        self.Model = GoodsReceivedNote
        self.Slug = 'goods-received-notes'

        super().prepare()

    async def Create(self):
        # TODO: also add check if items are equals to purchase order items
        if self.Data['transaction_type'] == 'PURCHASE_ORDER':
            if not self.Data['purchase_order_id']:
                raise RequiredFieldMissing('purchase_order_id')

        grn_id = await super().Create()
        grn_id = grn_id['data']

        # TODO: duplicate entries check
        self.update_stock(grn_id)

    # async def Update(self):
    #     # TODO: Should not be able to update once created (If not so manage quantity properly)
    #     await super().Update()
    #
    async def Delete(self):
        grn_id = await super().Delete()
        grn_id = grn_id['data']
        grn_id = grn_id[0]

        grn_items = self.gdb.query(GRNItem).filter(GRNItem.grn_id == grn_id).all()

        # TODO: View for batches update
        for item in grn_items:
            quantity = 0

            batch = self.gdb.query(InventoryStock, ItemBatch, Inventory) \
                .join(ItemBatch, ItemBatch.item_batches_id == InventoryStock.inventory_batch_id) \
                .join(Inventory, Inventory.code == ItemBatch.inventory_id) \
                .filter(InventoryStock.inventory_batch_id == item.inventory_batch_id).first()

            if item.loose_packing:
                quantity = item.quantity
            else:
                quantity = item.quantity * batch.Inventory.storage_to_selling

            self.gdb.query(InventoryStock).filter(InventoryStock.inventory_batch_id == item.inventory_batch_id).update(
                {InventoryStock.quantity: InventoryStock.quantity - quantity})

    def update_stock(self, grn_id):
        items_list = self.Data['items']
        for item in items_list:
            inventory_item = self.gdb.query(Inventory).filter(Inventory.code == item['inventory_id']).first()

            existing_batch = self.gdb.query(ItemBatch).filter(
                and_(ItemBatch.inventory_id == item['inventory_id'], ItemBatch.batch_no == item['batch_no'])).first()

            # TODO: Store quantity in db for check purposes
            quantity = 0

            if item['loose_packing']:
                quantity = item['quantity']
            else:
                quantity = inventory_item.storage_to_selling * item['quantity']

            if item['bonus']:
                quantity += item['bonus']

            if existing_batch is not None:
                existing_stock = self.gdb.query(InventoryStock) \
                    .filter(
                    and_(
                        InventoryStock.store_id == self.Data['store_id'],
                        InventoryStock.inventory_batch_id == existing_batch.item_batches_id
                    )).update(
                    {
                        InventoryStock.quantity:
                            InventoryStock.quantity + quantity
                    })

                if existing_stock is None:
                    self.add_new_stock(existing_batch.item_batches_id, quantity)

                self.add_new_GRN_items(grn_id, item, existing_batch.item_batches_id)
            else:
                new_batch = ItemBatch()
                new_batch.batch_no = item['batch_no']
                new_batch.inventory_id = item['inventory_id']
                new_batch.expire_date = item['expire_date']
                new_batch.selling_price = item['selling_price']
                new_batch.created_by_id = self.Session.employee_id
                new_batch.last_edited_by_id = self.Session.employee_id

                self.gdb.add(new_batch)
                self.gdb.flush()

                self.add_new_stock(new_batch.item_batches_id, quantity)

                self.add_new_GRN_items(grn_id, item, new_batch.item_batches_id)

    def add_new_GRN_items(self, grn_id, item, batch_id):
        new_grn_item = GRNItem()
        new_grn_item.grn_id = grn_id
        new_grn_item.inventory_batch_id = batch_id
        new_grn_item.bonus = item['bonus']
        new_grn_item.quantity = item['quantity']
        new_grn_item.purchase_price = item['purchase_price']
        new_grn_item.discount = item['discount']
        new_grn_item.discount_type = item['discount_type']
        new_grn_item.sales_tax = item['sales_tax']
        new_grn_item.sales_tax_type = item['sales_tax_type']
        new_grn_item.loose_packing = item['loose_packing']
        new_grn_item.created_by_id = self.Session.employee_id
        new_grn_item.last_edited_by_id = self.Session.employee_id

        self.gdb.add(new_grn_item)

    def add_new_stock(self, batch_id, quantity):
        new_stock = InventoryStock()
        new_stock.store_id = self.Data['store_id']
        new_stock.inventory_batch_id = batch_id
        new_stock.quantity = quantity
        new_stock.created_by_id = self.Session.employee_id
        new_stock.last_edited_by_id = self.Session.employee_id

        self.gdb.add(new_stock)

    async def Single(self):
        is_items = False
        if 'items' in self.Data['columns']:
            self.Data['columns'].remove('items')
            is_items = True

        data = await super().Single()
        data = data['data']

        if is_items:
            items = []
            for item in self.gdb.query(GRNItem, ItemBatch, Inventory) \
                    .join(ItemBatch, ItemBatch.item_batches_id == GRNItem.inventory_batch_id) \
                    .join(Inventory, Inventory.code == ItemBatch.inventory_id) \
                    .filter(GRNItem.grn_id == self.Data['oid']).all():
                # TODO: Only send that field which is required from frontend
                items.append({
                    'inventory_id': item.Inventory.code,
                    'quantity': item.GRNItem.quantity,
                    'purchase_price': item.GRNItem.purchase_price,
                    'discount': item.GRNItem.discount,
                    'discount_type': item.GRNItem.discount_type,
                    'sales_tax': item.GRNItem.sales_tax,
                    'sales_tax_type': item.GRNItem.sales_tax_type,
                    'loose_packing': item.GRNItem.loose_packing,
                    'batch_no': item.ItemBatch.batch_no,
                    'expire_date': item.ItemBatch.expire_date,
                    'selling_price': item.ItemBatch.selling_price,
                    'bonus': item.GRNItem.bonus
                })

            if len(items) > 0:
                data['items'] = items

        return {'data': data}
