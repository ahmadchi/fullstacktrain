from chi.common import CGBaseController
from chi.data import InventorySupplier


class InventorySupplierController(CGBaseController):
    def prepare(self):
        self.Model = InventorySupplier
        self.Slug = 'inventory-suppliers'

        super().prepare()
