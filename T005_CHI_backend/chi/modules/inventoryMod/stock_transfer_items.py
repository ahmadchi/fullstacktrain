from chi.common import CGBaseController
from chi.data import StockTransferItem


class StockTransferItemController(CGBaseController):
    def prepare(self):
        self.Model = StockTransferItem
        self.Slug = 'stock--transfer-items'

        super().prepare()
