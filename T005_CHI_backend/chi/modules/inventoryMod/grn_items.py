from chi.common import CGBaseController
from chi.data import GRNItem


class GRNItemController(CGBaseController):
    def prepare(self):
        self.Model = GRNItem
        self.Slug = 'grn-items'

        super().prepare()
