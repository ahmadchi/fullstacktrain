from chi.common import CGBaseController
from chi.data import Store


class StoreController(CGBaseController):
    def prepare(self):
        self.Model = Store
        self.Slug = 'stores'

        super().prepare()
