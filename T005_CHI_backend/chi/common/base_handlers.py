import time
from typing import Dict, Any

import tornado.escape
import tornado.ioloop
import tornado.web

from chi.data.base_models import CurrentSession
from .constants import Constants
from .exceptions import CGBaseException
from .httperrors import HttpErrors


class BaseHandler(tornado.web.RequestHandler):
    def prepare(self):
        super().prepare()

        ses = {
            'session_id': 'dev-test',
            'device_id': 1,
            'user_id': 1,
            'group_id': 1,
            'employee_id': 1,
            'full_name': 'Admin CHI',
            'first_name': 'Admin',
            'last_name': 'CHI'
        }
        self.Session = CurrentSession(**ses)

        self.Data: Dict[str, Any] = {}
        self.Files: Any = []

        try:
            self.gdb = self.application.SessionMaker()
        except Exception:
            raise tornado.web.HTTPError(503)

    def on_finish(self):
        self.gdb.close()

        super().on_finish()
        return


class BaseWebHandler(BaseHandler):
    def prepare(self):
        super().prepare()

        self.set_header("Content-Type", "text/html;charset=utf-8")

    def write_error(self, status_code, **kwargs):
        code = '{}'.format(status_code)
        title = "Unknown Error"
        message = "Unknown Error"

        if 'exc_info' in kwargs:
            ex = kwargs['exc_info'][1]
            if isinstance(ex, CGBaseException):
                self.render('error-page.html', Code=ex.ErrorCode, Title='API Error!', Message=ex.ErrorMessage)
                return

        if code in HttpErrors:
            title = HttpErrors[code]['title']
            message = HttpErrors[code]['message']

        self.render('error-page.html', Code=code, Title=title, Message=message)
        return


class BaseApiHandler(BaseHandler):
    def prepare(self):
        super().prepare()

        self.set_header("Content-Type", "application/json")

        try:
            if hasattr(self.request, 'files') and len(self.request.files) > 0:
                self.Files = self.request.files
            elif hasattr(self.request, 'body') and (len(self.request.body) > 0):
                self.Data = tornado.escape.json_decode(self.request.body)
        except Exception:
            raise tornado.web.HTTPError(503)

        self.RequestStart = time.time()

    def write_error(self, status_code, **kwargs):
        # self.gdb.rollback()

        message = None
        code = '{}'.format(status_code)

        if 'exc_info' in kwargs:
            ex = kwargs['exc_info'][1]
            if isinstance(ex, CGBaseException):
                message = ex.ErrorMessage
                code = ex.ErrorCode
                status_code = 200
        if message is None:
            message = 'Unknown Error'
            if code in HttpErrors:
                message = '{}-{}'.format(HttpErrors[code]['title'], HttpErrors[code]['message'])

        resp = {
            'Status': Constants.StatusError,
            'ErrorCode': code,
            'ErrorMessage': message
        }

        self.set_header("Content-Type", "application/json")
        self.set_status(status_code)
        self.write(resp)
        self.finish()
        return

    async def SendSuccessResponse(self, **kwargs):
        loop = tornado.ioloop.IOLoop.current()
        await loop.run_in_executor(None, self.gdb.commit)
        # self.gdb.commit()
        await self.SendResponse(Constants.StatusSuccess, **kwargs)

    async def SendErrorResponse(self, **kwargs):
        loop = tornado.ioloop.IOLoop.current()
        await loop.run_in_executor(None, self.gdb.rollback)
        # self.gdb.rollback()
        await self.SendResponse(Constants.StatusError, **kwargs)

    async def SendResponse(self, status, **kwargs):
        resp = {}

        for k, v in kwargs.items():
            resp[k] = v

        resp['Status'] = status
        resp['Time'] = int(time.time() * 1000)
        resp['Duration'] = '{:.03f} ms'.format(1000 * (time.time() - self.RequestStart))

        self.set_header("Content-Type", "application/json")
        self.set_status(200)
        self.write(resp)
        await self.finish()
