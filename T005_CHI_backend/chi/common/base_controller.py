import asyncio

from sqlalchemy.exc import IntegrityError

from chi.data.base_models import CurrentSession
# from chi.data.base_models import CurrentSession

from .exceptions import RequiredFieldMissing, ChildRecordExists, ExceptionWithMessage, RecordNotFound
from .helper import CGDbHelperV2, CGQueryHelper


class CGBaseController:
    def __init__(self, context):
        self.dbqh = CGQueryHelper()
        self.context = context

        self.gdb = context.gdb

        self.Data = context.Data if hasattr(context, 'Data') else None
        self.Session: CurrentSession = context.Session if hasattr(context, 'Session') else None

        self.Model = None
        self.Slug = None
        self.dbh = None
        self.PublicMethods = []

    def prepare(self):
        if self.Model is not None:
            self.dbh = CGDbHelperV2(self.gdb, self.Model)

        self.PublicMethods.extend(['List', 'Single', 'Create', 'Update', 'Delete', 'FilterOptions', 'SelectOptions'])

    def _loadController(self, slug, data=None):

        ctrl_cls = self.context.application.Registry.get(slug, None)
        if ctrl_cls is None:
            raise ExceptionWithMessage('Controller not found for slug {} not found'.format(slug))

        ctrl = ctrl_cls(self.context)
        if data is not None:
            ctrl.Data = data

        ctrl.prepare()

        return ctrl

    # Utility Methods
    def AssertData(self, *keys):
        for key in keys:
            if key not in self.Data or self.Data[key] is None:
                raise KeyError(key)
        return

    def AssertDataWithCleaning(self, *keys):
        for key in keys:
            if key not in self.Data or self.Data[key] is None:
                raise KeyError(key)

        for key in self.Data:
            if not any(key in k for k in keys):
                self.Data[key] = None
        return

    def addTopOrder(self, column, direction):
        top_orders = [{
            'column': column,
            'dir': direction
        }]

        orders = self.Data['order'] if 'order' in self.Data else {}
        if len(orders) > 0:
            orders = top_orders + orders
        else:
            orders = top_orders

        self.Data['order'] = orders
        return

    def addTopWhereDict(self, whr, wheres=None):
        if wheres is None:
            wheres = self.Data['where'] if 'where' in self.Data else {}

        if len(wheres) == 0:
            wheres = whr
        else:
            wh2 = {
                'group': 'and',
                'children': [whr],
            }

            wh2['children'].append(wheres)
            wheres = wh2

        self.Data['where'] = wheres
        return

    def addTopWhere(self, column, value, op='eq', wheres=None):

        if wheres is None:
            wheres = self.Data['where'] if 'where' in self.Data else {}

        if len(wheres) == 0:
            wheres = {
                'column': column,
                'search': value,
                'op': op
            }
        else:
            wh2 = {
                'group': 'and',
                'children': [{
                    'column': column,
                    'search': value,
                    'op': op
                }],
            }

            wh2['children'].append(wheres)
            wheres = wh2

        self.Data['where'] = wheres
        return

    # Common Controller Methods
    async def List(self):
        columns = self.Data['columns'] if 'columns' in self.Data else []
        wheres = self.Data['where'] if 'where' in self.Data else {}
        alchemy_where = self.Data.get('alchemy_where', None)
        orders = self.Data['order'] if 'order' in self.Data else []
        limit = self.Data.get('limit', 150)
        offset = self.Data.get('offset', 0)
        searchKey = self.Data['search'] if 'search' in self.Data and self.Data['search'] != '' else None

        columns, data, total = await self.dbh.getList(columns=columns, wheres=wheres, orders=orders, limit=limit,
                                                      offset=offset, searchKey=searchKey,
                                                      alchemy_where=alchemy_where)
        resp = {
            'data': data,
            'total_records': total,
        }

        return resp

    async def Single(self):
        if 'oid' not in self.Data:
            raise RequiredFieldMissing('oid')

        data = None

        if 'columns' in self.Data:
            data = await self.dbh.getSingleForColumns(self.Data['oid'], self.Data['columns'])
        else:
            rec = await self.dbh.getSingle(self.Data['oid'])
            if rec is not None:
                data = rec.serializeRoot()

        if data is not None:
            if 'sections' in self.Data:
                sections = self.Data['sections']
                for refCol in sections:
                    table = sections[refCol]['table']
                    columns = sections[refCol]['columns']
                    ctrl = self._loadController(table)
                    ctrl.Data = {'oid': data[ctrl.dbh.PrimaryKey], 'columns': columns}
                    rec = await ctrl.Single()
                    if rec is not None:
                        for k in rec:
                            data[k] = rec[k]

            sections = self.Data['foreign_keys'] if 'foreign_keys' in self.Data else self.Model.get_foreign_keys()
            for refCol in sections:
                f_section = sections[refCol]
                data_sub = {'columns': f_section['columns'],
                            'where': {'column': f_section['foreign_column'], 'search': data[refCol]}}
                ctrl = self._loadController(f_section['table'], data_sub)
                dataKey = '{0}_data'.format(refCol)
                data[dataKey] = await ctrl.LoadSelectOptions()

        # await asyncio.sleep(0.00001)
        return {'data': data}

    async def Create(self):
        if 'sections' in self.Data and self.Data['sections'] is not None:
            sections = self.Data['sections']
            for refCol in sections:
                table = sections[refCol]['table']
                data = sections[refCol]['data']
                ctrl = self._loadController(table, data=data)
                _id = await ctrl.Create()
                self.Data[refCol] = _id

        col = getattr(self.Model, 'created_by_id', None)
        if col is not None:
            self.Data['created_by_id'] = self.Session.employee_id
        col = getattr(self.Model, 'last_edited_by_id', None)
        if col is not None:
            self.Data['last_edited_by_id'] = self.Session.employee_id

        row_id = await self.dbh.insertRecord(self.Data)

        await asyncio.sleep(0.00001)
        return {'data': row_id}

    async def Update(self):
        oid = self.Data[self.dbh.PrimaryKey]

        rec = await self._single_for_approval(oid)
        if rec is None:
            raise RecordNotFound(self.Slug, self.Model.__primary_key__, oid)

        if 'sections' in self.Data and self.Data['sections'] is not None:
            sections = self.Data['sections']
            for refCol in sections:
                table = sections[refCol]['table']
                data = sections[refCol]['data']

                ctrl = self._loadController(table, data=data)
                data[refCol] = await self.Data[ctrl.dbh.PrimaryKey]
                self.Data[refCol] = ctrl.Update()

            self.Data['pending_approval'] = False

        col = getattr(self.Model, 'last_edited_by_id', None)
        if col is not None:
            self.Data['last_edited_by_id'] = self.Session.employee_id
        row_id = await self.dbh.updateRecord(self.Data)

        await asyncio.sleep(0.00001)
        return {'data': row_id}

    async def Delete(self):
        oids = self.Data['oid']
        if not isinstance(oids, list):
            oids = [oids]

        for oid in oids:
            comments = self.Data.get('comments', None)

            rec = await self._single_for_approval(oid)
            if rec is None:
                raise RecordNotFound(self.Slug, self.Model.__primary_key__, oid)

            try:
                await self.dbh.deleteRecord(oid, comments)
            except IntegrityError:
                raise ChildRecordExists()

        await asyncio.sleep(0.00001)
        return {'data': oids}

    async def FilterOptions(self):
        self.AssertData('column')

        columns = [self.Data['column']]
        searchKey = self.Data['search'] if 'search' in self.Data and self.Data['search'] != '' else None
        self.addTopWhere(columns[0], None, 'ne')

        wheres = self.Data['where'] if 'where' in self.Data else {}
        orders = self.Data['order'] if 'order' in self.Data else [{'column': '{}'.format(columns[0]), 'dir': 'asc'}]

        columns, values, total = await self.dbh.getList(columns=columns, wheres=wheres, orders=orders, limit=25,
                                                        searchKey=searchKey, distinct=True, linear=True, noCount=True)

        data = [{'value': v[0]} for v in values]

        await asyncio.sleep(0.00001)
        return {'data': data}

    async def LoadSelectOptions(self):
        columns = self.dbh.getDropdownColumns()
        primaryKey = self.dbh.PrimaryKey

        if 'columns' in self.Data and len(self.Data['columns']) > 0:
            columns = self.Data['columns']

        if primaryKey not in columns:
            columns.append(primaryKey)

        orders = self.Data['order'] if 'order' in self.Data else [{'column': '{}'.format(columns[0]), 'dir': 'asc'}]
        searchKey = self.Data['search'] if 'search' in self.Data and self.Data['search'] != '' else None
        wheres = self.Data['where'] if 'where' in self.Data else {}
        limit = self.Data['limit'] if 'limit' in self.Data else 100

        params = {
            'columns': columns,
            'wheres': wheres,
            'orders': orders,
            'searchKey': searchKey,
            'limit': limit,
            'noCount': True
        }

        cols, data, total = await self.dbh.getList(**params)

        for obj in data:
            obj['id'] = obj[primaryKey]
            # obj['value'] = obj[columns[0]]

        return {'data': data}

    async def SelectOptions(self):
        self.AssertData('slug')

        ctrl = self._loadController(self.Data['slug'], self.Data)
        return await ctrl.LoadSelectOptions()

    # Approval System Helpers
    async def _single_for_approval(self, oid):
        data = None
        rec = await self.dbh.getSingle(oid)
        if rec is not None:
            data = rec.serializeRoot()
            if hasattr(rec, 'pending_approval'):
                rec.pending_approval = True

        if data is not None:
            sections = self.Model.get_foreign_keys()
            for refCol in sections:
                f_section = sections[refCol]
                data_sub = {'columns': f_section['columns'],
                            'where': {'column': f_section['foreign_column'], 'search': data[refCol]}}
                ctrl = self._loadController(f_section['table'], data_sub)
                data_key = '{0}_data'.format(refCol)
                data[data_key] = await ctrl.LoadSelectOptions()

        return data


#test2