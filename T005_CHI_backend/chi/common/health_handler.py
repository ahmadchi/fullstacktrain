import tornado.web


class HealthHandler(tornado.web.RequestHandler):
    def get(self, *args, **kwargs):
        self.write("Ok")
        self.finish()
        return
