import traceback
from decimal import Decimal

import tornado.ioloop
from sqlalchemy import or_, and_, desc, asc
from sqlalchemy.orm import aliased
from sqlalchemy.sql import func
from sqlalchemy.sql.elements import Label
from sqlalchemy.sql.expression import literal_column

from .exceptions import InvalidColumn, TableColumnNotFound, RecordNotFound


class Context:
    def __init__(self, searchKey):
        self.OrWheres = []
        self.Joins = []
        self.SelectedColumns = []
        self.SearchKey = searchKey
        self.JoinRefs = {}


class CGDbHelperV2:
    def __init__(self, gdb, model):
        self.gdb = gdb

        self.Model = model
        self.PrimaryKey = self.Model.__primary_key__

        self.EnableAttachment = False
        self.PrimaryKeyType = 'INT'
        self.PrimarykeyAutoIncrement = True
        self.EnableChangeLog = False

    def getVisibleColumns(self):
        if hasattr(self.Model, 'getVisibleColumns'):
            return self.Model.getVisibleColumns()

        return []

    def getDropdownColumns(self):
        if hasattr(self.Model, 'getDropdownColumns'):
            return self.Model.getDropdownColumns()

        return []

    def getRequiredFields(self):
        if hasattr(self.Model, 'getRequiredFields'):
            return self.Model.getRequiredFields()

        return []

    def table_name(self):
        return self.Model.__tablename__

    def primary_key(self):
        return self.Model.__primary_key__

    def _addDeletedWhere(self, wheres):
        col = getattr(self.Model, 'is_deleted', None)
        if col is not None:
            if len(wheres) == 0:
                wheres = {
                    'column': 'is_deleted',
                    'search': [0],
                    'op': 'eq'
                }
            else:
                wh2 = {
                    'group': 'and',
                    'children': [{
                        'column': 'is_deleted',
                        'search': [0],
                        'op': 'eq'
                    }],
                }

                wh2['children'].append(wheres)
                wheres = wh2

        return wheres

    def _applyConditions(self, context, query, groupBy):
        for j in context.Joins:
            if j['type'] == 'inner':
                query = query.join(j['join'])
            elif j['type'] == 'outer':
                query = query.outerjoin(j['join'])

        if len(context.OrWheres) > 0:
            query = query.filter(or_(*context.OrWheres))

        if groupBy is not None:
            gb_col = self.GetFinalColumn(self.Model, groupBy, context)
            if gb_col is None:
                raise InvalidColumn(groupBy)

            query = query.group_by(gb_col)

        return query

    async def getList(self, **kwargs):
        columns = kwargs.pop('columns', [])
        wheres = kwargs.pop('wheres', {})
        orders = kwargs.pop('orders', [])
        limit = kwargs.pop('limit', None)
        offset = kwargs.pop('offset', None)
        searchKey = kwargs.pop('searchKey', None)
        groupBy = kwargs.pop('groupBy', None)
        distinct = kwargs.pop('distinct', False)
        skipDelCheck = kwargs.pop('skipDelCheck', False)
        linear = kwargs.pop('linear', False)
        noCount = kwargs.pop('noCount', False)
        alchemyWhere = kwargs.pop('alchemy_where', None)
        # callJoins = kwargs.pop('joins', None)

        if '__status__' in columns:
            columns.remove('__status__')

        if not skipDelCheck:
            wheres = self._addDeletedWhere(wheres)

        if len(columns) == 0:
            columns = self.getVisibleColumns()

        if distinct is False:
            if self.PrimaryKey in columns:
                columns.remove(self.PrimaryKey)

            columns.insert(0, self.PrimaryKey)

        context = Context(searchKey)

        self.CheckColumns(columns, context)

        query = self.gdb.query(*context.SelectedColumns)
        criteria = self._AddWheres(wheres, context)

        if criteria is not None:
            query = query.filter(criteria)

        if alchemyWhere is not None:
            query = query.filter(alchemyWhere)

        if len(orders) > 0:
            for order in orders:
                col = self.GetFinalColumn(self.Model, order['column'], context)

                if col is None:
                    raise InvalidColumn(order['column'])

                if str(order['dir']).lower() == "desc":
                    query = query.order_by(desc(col))
                else:
                    query = query.order_by(asc(col))

        query = self._applyConditions(context, query, groupBy)

        if limit is not None:
            query = query.limit(limit)
        if offset is not None:
            query = query.offset(offset)
        if distinct is True:
            query = query.distinct()

        total = 0
        loop = tornado.ioloop.IOLoop.current()
        if not noCount:
            statement = query.statement.with_only_columns([func.count()]).order_by(None).limit(None).offset(None)
            total_r = await loop.run_in_executor(None, self.gdb.execute, statement)
            # total_r = self.gdb.execute()
            total = total_r.scalar()

        data = await loop.run_in_executor(None, query.all)
        # data = query.all()
        rows = data

        if not linear:
            rows = CGDbHelperV2._MakeDict(columns, data)

        return columns, rows, total

    def getAggregate(self, columns=None, wheres=None, alchemyWhere=None, groupBy=None, skipDelCheck=False,
                     linear=False):
        if columns is None:
            columns = []
        if wheres is None:
            wheres = {}

        if not skipDelCheck:
            wheres = self._addDeletedWhere(wheres)

        if len(columns) == 0:
            return None

        context = Context(None)
        resultColumns = []

        allRelations = self.Model.__mapper__.relationships._data.keys()

        for col2 in columns:
            if 'func' not in col2:
                col2['func'] = 'none'

            col = col2['column']
            if 'as' in col2:
                resultColumns.append(col2['as'])
            else:
                resultColumns.append(col)

            if col in allRelations and '.' not in col:
                raise InvalidColumn(col)

            column = self.GetFinalColumn(self.Model, col, context)
            if column is None:
                raise InvalidColumn(col)

            aggrCol = None
            if col2['func'].lower() == 'sum':
                aggrCol = func.sum(column)
            elif col2['func'].lower() == 'avg':
                aggrCol = func.avg(column)
            elif col2['func'].lower() == 'min':
                aggrCol = func.min(column)
            elif col2['func'].lower() == 'max':
                aggrCol = func.max(column)
            elif col2['func'].lower() == 'count':
                aggrCol = func.count(column)
            elif col2['func'].lower() == 'none':
                aggrCol = column

            if aggrCol is None:
                raise InvalidColumn('{} for {}'.format(col2['func'], col))

            context.SelectedColumns.append(aggrCol)

        # query = self.gdb.query(self.Model)
        query = self.gdb.query(*context.SelectedColumns)
        conditions = self._AddWheres(wheres, context)

        if conditions is not None:
            query = query.filter(conditions)

        if alchemyWhere is not None:
            query = query.filter(alchemyWhere)

        query = self._applyConditions(context, query, groupBy)

        data = query.all()
        rows = data
        if not linear:
            rows = CGDbHelperV2._MakeDict(resultColumns, data)

        return resultColumns, rows

    async def getSingle(self, oid):
        if oid is None:
            return None

        query = self.gdb.query(self.Model)

        col = getattr(self.Model, self.PrimaryKey)
        query = query.filter((col == oid))

        col = getattr(self.Model, 'is_deleted', None)
        if col is not None:
            query = query.filter(col.is_(False))

        loop = tornado.ioloop.IOLoop.current()
        data = await loop.run_in_executor(None, query.one_or_none)
        # data = query.one_or_none()
        if data is None:
            return None

        return data

    async def getSingleForColumns(self, oid, columns=None):
        if oid is None:
            return None
        if columns is None:
            columns = []

        wheres = {
            'column': self.PrimaryKey,
            'search': oid
        }

        columns, data, total = await self.getList(columns=columns, wheres=wheres, skipDelCheck=True, noCount=True)

        if len(data) > 0:
            return data[0]
        else:
            return None

    async def insertRecord(self, data):
        if data is None:
            return None

        modelObj = self.Model()

        for k, v in data.items():
            if isinstance(v, str) and v == '':
                v = None

            attr = getattr(self.Model, k, None)
            if attr is not None:
                if 'hybrid_property' in attr.__class__.__name__:
                    continue
                # if hasattr(modelObj, k):
                setattr(modelObj, k, v)

        loop = tornado.ioloop.IOLoop.current()
        await loop.run_in_executor(None, self.gdb.add, modelObj)
        await loop.run_in_executor(None, self.gdb.flush)
        # self.gdb.add(modelObj)
        # self.gdb.flush()

        rowId = getattr(modelObj, self.PrimaryKey)
        return rowId

    async def updateRecord(self, data):
        if data is None:
            return None

        row_id = data[self.PrimaryKey]
        modelObj = await self.getSingle(row_id)

        if modelObj is None:
            raise RecordNotFound('updateRecord', '__id__|{}'.format(self.PrimaryKey), row_id)

        for k, v in data.items():
            if k == self.PrimaryKey:
                continue

            if isinstance(v, str) and v == '':
                v = None

            attr = getattr(self.Model, k, None)
            if attr is not None:
                # if 'hybrid_property' in attr.__class__.__name__:
                #     continue
                # if hasattr(modelObj, k):
                setattr(modelObj, k, v)

        loop = tornado.ioloop.IOLoop.current()
        await loop.run_in_executor(None, self.gdb.flush)
        # self.gdb.flush()

        return row_id

    async def deleteRecord(self, oids, comments):
        if not isinstance(oids, list):
            oids = [oids]

        for oid in oids:
            modelObj = await self.getSingle(oid)
            if modelObj is None:
                print('Delete, record not found')
                continue

            col = getattr(self.Model, 'is_deleted', None)
            if col is not None:
                modelObj.is_deleted = True
                modelObj.delete_comments = comments
            else:
                loop = tornado.ioloop.IOLoop.current()
                await loop.run_in_executor(None, self.gdb.delete, modelObj)
                # self.gdb.delete(modelObj)

        return

    async def getFilterOptions(self, column, wheres=None):
        if wheres is None:
            wheres = []

        _column = None

        allColumns = self.Model.__table__.columns._data.keys()
        allRelations = self.Model.__mapper__.relationships._data.keys()

        origCol = column
        cols = []
        if '.' in column:
            cols = column.split('.')
            if len(cols) > 2:
                raise InvalidColumn(column)

            column = cols[0]

        if column in allColumns:
            _column = getattr(self.Model, column, None)
            if _column is None:
                raise TableColumnNotFound(origCol)
        elif column in allRelations:
            if '.' not in origCol:
                raise InvalidColumn(origCol)

            Class = self.Model.__mapper__.relationships._data[column].mapper.class_
            if Class is None:
                raise TableColumnNotFound(origCol)

            _column = getattr(Class, cols[1])
        else:
            raise TableColumnNotFound(origCol)

        query = self.gdb.query(_column)

        for wh in wheres:
            column = wh['column']
            localWheres = []

            for search in wh['search']:
                localWheres.append((_column == search))

            if len(localWheres) > 0:
                query = query.filter(or_(*localWheres))

        query = query.order_by(_column)
        query = query.distinct(_column)

        loop = tornado.ioloop.IOLoop.current()
        data = await loop.run_in_executor(None, query.all)
        # data = query.all()
        data = [{'value': obj[0]} for obj in data]

        return data

    def CheckColumns(self, columns, context):
        allRelations = self.Model.__mapper__.relationships._data.keys()

        for column in columns:
            if column in allRelations and '.' not in column:
                raise InvalidColumn(column)

            _column = self.GetFinalColumn(self.Model, column, context)
            if _column is None:
                raise InvalidColumn(column)

            if self._IsColumnSecure(_column) is False:
                if context.SearchKey is not None:
                    context.OrWheres.append(_column.like("%%%s%%" % context.SearchKey))

                context.SelectedColumns.append(_column)
            else:
                _l_col = literal_column("'<<<encrypted>>>'").label(_column.name)

                context.SelectedColumns.append(_l_col)

        return

    def _IsColumnSecure(self, _column):
        if hasattr(_column, 'secure'):
            return _column.secure

        if isinstance(_column, Label):
            for c in _column.base_columns:
                if hasattr(c, 'secure'):
                    return c.secure

        return False

    def _AddWheres(self, where, context):
        print('{0}\nWHERES\n{1}\n{0}'.format('-' * 40, where))
        whType = 'none'
        if 'group' in where and where['group'] is not None:
            whType = where['group'].lower()

        if whType == 'none':
            return self._MakeCriteria(where, context)

        _whereGroup = []
        for ch in where['children']:
            criteria = self._AddWheres(ch, context)
            if criteria is not None:
                _whereGroup.append(criteria)

        if whType == 'or':
            return or_(*_whereGroup)
        if whType == 'and':
            return and_(*_whereGroup)

        return None

    def _MakeCriteria(self, where, context):
        if 'column' not in where:
            return None

        _column = self.GetFinalColumn(self.Model, where['column'], context)

        if _column is None:
            raise InvalidColumn(where['column'])

        operator = where['op'].lower() if 'op' in where else 'eq'

        if operator == 'eq':
            return CGDbHelperV2._MakeCriteria_Eq(_column, where['search'])
        if operator == 'ne':
            return CGDbHelperV2._MakeCriteria_Ne(_column, where['search'])
        if operator == 'lt':
            return CGDbHelperV2._MakeCriteria_Lt(_column, where['search'])
        if operator == 'le':
            return CGDbHelperV2._MakeCriteria_Le(_column, where['search'])
        if operator == 'gt':
            return CGDbHelperV2._MakeCriteria_Gt(_column, where['search'])
        if operator == 'ge':
            return CGDbHelperV2._MakeCriteria_Ge(_column, where['search'])
        if operator == 'like':
            return CGDbHelperV2._MakeCriteria_Like(_column, where['search'])
        if operator == 'bt':
            return CGDbHelperV2._MakeCriteria_Between(_column, where['search'])

        return None

    def GetFinalColumn(self, model, column, context):
        cols = column.split('.')

        _column = getattr(model, cols[0], None)

        if _column is None:
            return None

        if len(cols) > 1:
            insp = _column.property.local_columns
            column = '.'.join(cols[1:])
            alias = None

            if cols[0] not in context.JoinRefs:
                alias = aliased(_column.mapper.class_)
                ln = {'join': (alias, _column)}

                if tuple(enumerate(insp))[0][1].nullable:
                    ln['type'] = 'outer'
                else:
                    ln['type'] = 'inner'

                context.Joins.append(ln)
                context.JoinRefs[cols[0]] = {'alias': alias, 'column': _column}

            else:
                alias = context.JoinRefs[cols[0]]['alias']

            return self.GetFinalColumn(alias, column, context)
        else:
            clsName = str(_column)
            if 'AliasedClass' in clsName:
                return _column.label('{}.{}'.format(model.__table__, cols[0]))

            return _column

    @staticmethod
    def _MakeCriteria_Eq(_column, search):
        if not isinstance(search, list):
            return _column == search

        if len(search) == 1:
            return _column == search[0]

        return _column.in_(search)

    @staticmethod
    def _MakeCriteria_Ne(_column, search):
        if not isinstance(search, list):
            return _column != search

        if len(search) == 1:
            return _column != search[0]

        return _column.notin_(search)

    @staticmethod
    def _MakeCriteria_Lt(_column, search):
        if not isinstance(search, list):
            return _column < search

        if len(search) == 1:
            return _column < search[0]

        _group = []

        for s in search:
            _group.append((_column < s))

        return or_(*_group)

    @staticmethod
    def _MakeCriteria_Le(_column, search):
        if not isinstance(search, list):
            return _column <= search

        if len(search) == 1:
            return _column <= search[0]

        _group = []

        for s in search:
            _group.append((_column <= s))

        return or_(*_group)

    @staticmethod
    def _MakeCriteria_Gt(_column, search):
        if not isinstance(search, list):
            return _column > search

        if len(search) == 1:
            return _column > search[0]

        _group = []

        for s in search:
            _group.append((_column > s))

        return or_(*_group)

    @staticmethod
    def _MakeCriteria_Ge(_column, search):
        if not isinstance(search, list):
            return _column >= search

        if len(search) == 1:
            return _column >= search[0]

        _group = []

        for s in search:
            _group.append((_column >= s))

        return or_(*_group)

    @staticmethod
    def _MakeCriteria_Like(_column, search):
        if not isinstance(search, list):
            return _column.like("%%%s%%" % search)

        if len(search) == 1:
            return _column.like("%%%s%%" % search[0])

        _group = []

        for s in search:
            _group.append((_column.like("%%%s%%" % s)))

        return or_(*_group)

    @staticmethod
    def _MakeCriteria_Between(_column, search):
        return _column.between(search[0], search[1])

    @staticmethod
    def _GetFirstColumn(model, column):
        cols = column.split('.')
        _column = getattr(model, cols[0], None)

        return _column

    @staticmethod
    def _GetValue(val):
        if hasattr(val, '_toJson'):
            return val._toJson()

        if isinstance(val, Decimal):
            return float(val)

        return val

    @staticmethod
    def _MakeDict(columns, data):
        data2 = []
        nColumns = len(columns)

        try:
            for row in data:
                r = {columns[i]: CGDbHelperV2._GetValue(row[i]) for i in range(0, nColumns)}
                data2.append(r)
        except IndexError:
            pass
            traceback.print_exc()

        return data2


class CGQueryHelper:

    async def executeGetFirstQuery(self, model, query):
        loop = tornado.ioloop.IOLoop.current()
        data = await loop.run_in_executor(None, query.one_or_none)

        return data