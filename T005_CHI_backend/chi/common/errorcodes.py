"""
# Filename: errorcodes.py
"""


class ErrorCodes():
    ERROR_EXCEPTION = -3
    ERROR_UNKNOWN = -1
    SESSION_EXPIRED = -2
    ERROR_SUCCESS = 0

    ERROR_TABLE_NOT_FOUND = 1098
    ERROR_PERMISSION_DENIED = 1099
    ERROR_USERPASS_MISMATCH = 1000
    ERROR_SESSION_EXPIRED = 1001
    ERROR_USER_BLOCKED = 1002
    ERROR_ACCOUNT_NOT_ACTIVE = 1003
    ERROR_VERIFICATION_CODE_INVALID = 1004
    ERROR_VERIFICATION_CODE_EXPIRED = 1005
    ERROR_INCORRECT_PASSWORD = 1006
    ERROR_DEVICE_NOT_AUTHORIZED = 1007

    ERROR_USERNAME_KEY_MISSING = 1101
    ERROR_PASSWORD_KEY_MISSING = 1102
    ERROR_DEVICE_ID_KEY_MISSING = 1103

    ERROR_REQUIRED_FIELD_MISSING = 1201
    ERROR_REQUIRED_FIELD_EMPTY = 1202
    ERROR_FOREIGN_KEY_CONSTRAINT_FAILED = 1203
    ERROR_INVALID_COLUMN = 1204
    ERROR_TABLE_COLUMN_NOT_FOUND = 1205
    ERROR_RECORD_NOT_FOUND = 1206

    ERROR_KEY_NOT_FOUND = 1207

    ERROR_PHONE_NOT_VERIFIED = 1208
    ERROR_INVALID_CAPTCHA = 1209
    ERROR_USER_NOT_FOUND = 1210

    ERROR_QUANTITY_LIMIT_EXCEEDS = 1211
