import calendar
import time
from datetime import datetime, timedelta, timezone


def _getCurrentTimeStamp(time_zone) -> tuple:
    tz = timezone(timedelta(seconds=time_zone))
    current_date = datetime.now(tz=tz)
    current_day = str(current_date.strftime("%a")).lower()

    current_time = datetime(1970, 1, 1, hour=current_date.hour, minute=current_date.minute, second=current_date.second,
                            microsecond=0, tzinfo=current_date.tzinfo)

    current_timestamp = int(current_time.timestamp())
    current_day_time = (current_day, current_timestamp)
    return current_day_time


def _get_day_start(time_zone) -> int:
    tz = timezone(timedelta(seconds=time_zone))
    current_date = datetime.now(tz=tz)
    current_time = datetime(current_date.year, current_date.month, current_date.day,
                            hour=0, minute=0, second=0, microsecond=0,
                            tzinfo=current_date.tzinfo)
    return int(current_time.timestamp())


def _get_monday(timestamp) -> int:
    dt = datetime.fromtimestamp(timestamp)
    previous_monday = dt - timedelta(days=dt.weekday())
    return int(previous_monday.timestamp())


def _get_month(timestamp) -> int:
    dt = datetime.fromtimestamp(timestamp)
    first = dt.replace(day=1)
    return int(first.timestamp())


def get_time_stamp_from_formatted_date(strdate, format) -> float:
    dt = datetime.strptime(strdate, format)
    return round(dt.timestamp())


def _get_month_days(date) -> tuple:
    return calendar.monthrange(date.year, date.month)


def current_time():
    return int(time.time())


def time_format(timestamp: int, frmt: str = None) -> str:
    if frmt is None:
        frmt = '%Y-%m-%d %H:%M'

    if timestamp in [0, None]:
        return ''

    return datetime.utcfromtimestamp(timestamp).strftime(frmt)


def _getDistance(target_location, src_location):
    if src_location in [None, '', '0,0']:
        return 0

    lat1, lng1 = src_location.split(',')
    lat2, lng2 = target_location.split(',')

    from math import sin, cos, sqrt, atan2, radians

    # approximate radius of earth in meters
    R = 6371000.0

    lat1 = radians(float(lat1))
    lng1 = radians(float(lng1))
    lat2 = radians(float(lat2))
    lng2 = radians(float(lng2))

    dlon = lng2 - lng1
    dlat = lat2 - lat1

    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    # in meters
    distance = R * c * 1.80
    return distance


def _responseDirection(routes):
    return {
        'distance': routes['routes'][0]['legs'][0]['distance']['value'],
        'duration': routes['routes'][0]['legs'][0]['duration']['value']
    }


def _get_next_period(period_type='hour', period_unit=1, hour=3600) -> int:
    now_nexts = {
        'hour': hour,
        'day': hour * 24,
        'week': hour * 24 * 7,
        'month': hour * 24 * 30,
        'year': hour * 24 * 365
    }
    return now_nexts[str(period_type).lower()] * period_unit


def snake_to_camel(word):
    return ' '.join(x.capitalize() or '_' for x in word.split('_'))
