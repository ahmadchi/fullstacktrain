import tornado.web

from chi.common import BaseWebHandler


class MainWebHandler(BaseWebHandler):
    async def prepare(self):
        await super().prepare()

        self.PublicMethods = ['Index']

    async def get(self, *args):
        if len(args) != 1:
            raise tornado.web.HTTPError(404)

        method = args[0]
        if method == '' or '/' in method or method not in self.PublicMethods:
            method = 'Index'

        call_method = getattr(self, method, None)
        if call_method is None:
            call_method = getattr(self, 'Index', None)

        await call_method()

    async def Index(self):
        await self.render("index-app.html")
