import tornado.web
from sqlalchemy.exc import IntegrityError

from chi.common import BaseApiHandler, CGBaseException


class GeneralApiHandler(BaseApiHandler):
    def prepare(self):
        super().prepare()

    async def post(self, *args):
        # /api/<slug>/<Method>
        if len(args) != 2 or '/' in args[0] or '/' in args[1]:
            raise tornado.web.HTTPError(404)

        slug = args[0]
        method = args[1]

        controller_cls = self.application.Registry.get(slug, None)

        if controller_cls is None:
            raise tornado.web.HTTPError(404)

        controller = controller_cls(self)
        controller.prepare()

        if method not in controller.PublicMethods:
            raise tornado.web.HTTPError(404)

        api_method = getattr(controller, method, None)
        if api_method is None:
            raise tornado.web.HTTPError(404)

        resp = None

        try:
            data = await api_method()
            if data is not None:
                await self.SendSuccessResponse(**data)
            else:
                await self.SendSuccessResponse()

        except IntegrityError as e:
            msg: str = e.orig.msg.split(' (')[0]
            msg = msg.replace('for key', 'for column')
            resp = {
                'ErrorCode': e.orig.errno,
                'ErrorMessage': msg
            }
        except KeyError as e:
            resp = {
                'ErrorCode': -3,
                'ErrorMessage': "Field '{}' is required".format(e.args[0])
            }
        except CGBaseException as e:
            # traceback.print_exc()
            resp = {
                'ErrorCode': e.ErrorCode,
                'ErrorMessage': e.ErrorMessage
            }

        if resp is not None:
            await self.SendErrorResponse(**resp)
