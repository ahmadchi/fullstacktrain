#!/usr/local/bin/python

from dotenv import load_dotenv

from chi import ChiWebServer


def main():
    load_dotenv(verbose=False)

    theServer = ChiWebServer()
    theServer.StartServer()

    theServer.RunLoop()
    return


if __name__ == "__main__":
    main()
