import { Component } from '@angular/core';
import { OnDestroy } from '@angular/core';
import { OnInit } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { Input } from '@angular/core';
import { Output } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { trigger, state, style, transition, animate } from '@angular/animations';

import { Subject } from 'rxjs';

import { Platform } from '@angular/cdk/platform';
import { ApiService } from '../../../services';
import { ChiConfigService } from '../../../services/app.config.service';
import { ControllerConfig, TableConfig, FormConfig } from '../../../models/general-models';


@Component({
    selector: 'chi-general-page',
    templateUrl: './general-page.html',
    styleUrls: ['./general-page.scss'],
    providers: [ApiService],
    animations: [
        trigger('tableExpand', [
            state('collapsed', style({ 'max-width': '{{tableWidth}}%', 'margin-right': '8px' }), {params: {tableWidth: 70}}),
            state('expanded', style({ 'max-width': '{{tableWidth}}%' }), {params: {tableWidth: 100}}),
            transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
        ]),
        trigger('formExpand', [
            state('collapsed', style({ display: 'none', 'max-width': '0' })),
            state('expanded', style({ 'max-width': '{{formWidth}}%' }), {params: {formWidth: 30}}),
            transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
        ]),
    ]

})
export class GeneralPageComponent implements OnInit, OnDestroy
{
    @Input() config: ControllerConfig;
    @Input() actions: Subject<any>;

    @Input() foreignKey: string;
    @Input() foreignKeyVal: any;
    @Input() asChild: boolean;
    @Input() isDialog: boolean;
    @Input() isSmallTable: boolean;
    @Output() pageSignals = new EventEmitter();

    tableWidth = 100;
    formWidth = 0;

    tableConfig: TableConfig;
    formConfig: FormConfig;

    oid: any;
    isMobile: boolean;

    constructor(
        protected apiService: ApiService,
        protected _route: ActivatedRoute,
        private _platform: Platform,
        private _configService: ChiConfigService)
    {
        this.config = null;
        this.oid = null;
        this.actions = new Subject();

        this.isSmallTable = false;
    }

    ngOnInit(): void
    {
        if ( this._platform.ANDROID || this._platform.IOS )
        {
            this.isMobile = true;
        }

        if (this.config == null)
        {
            const rd = this._route.snapshot.data;
            if (rd != null && rd.name !== void 0)
            {
                const ctrl = this._configService.getController(rd.name);
                if (ctrl !== void 0)
                {
                    this.config = new ControllerConfig(ctrl);
                }
            }

        }

        if (this.config != null)
        {
            this.apiService.apiSlug = this.config.slug;
            this.apiService.primaryKey = this.config.key;
            this.tableConfig = this.config.table;
            // this.formConfig = this.config.form;
        }
    }

    ngOnDestroy(): void
    {

    }

    onClick(e: any): void
    {
        if (this.formWidth === 0)
        {
            this.formWidth = 30;
            this.tableWidth = 70;
        }
        else
        {
            this.formWidth = 0;
            this.tableWidth = 100;
        }

        this.actions.next(
            // {action: 'reload'}
        );
    }

    onTableSignal(e: any)
    {
        if (e.type === 'OpenForm')
        {
            this.formConfig = (new FormConfig(this.config));
            this.oid = null;
            if (this.formWidth === 0)
            {
                this.formWidth = 30;
                this.tableWidth = 70;
            }
        }
        else if (e.type === 'CloseForm')
        {
            if (this.formWidth !== 0)
            {
                this.formWidth = 0;
                this.tableWidth = 100;
            }
        }
        else if (e.type === 'OnEdit')
        {
            this.formConfig = (new FormConfig(this.config));
            this.oid = e.row[this.config.key];

            if(this.formWidth === 0)
            {
                this.formWidth = 30;
                this.tableWidth = 70;
            }
        }
        else if (e.type === 'OnDelete')
        {

        }
        else if (e.type === 'OnDetails')
        {
            console.log('Details Signal -> ', e.row);
        }

        this.pageSignals.emit(e);

    }
}