import { Component } from '@angular/core';
import { Input } from '@angular/core';
import { OnInit, AfterViewInit } from '@angular/core';
import { OnChanges } from '@angular/core';
import { SimpleChanges } from '@angular/core';
import { OnDestroy } from '@angular/core';
import { Output } from '@angular/core';
import { ViewChild } from '@angular/core';
import { HostBinding } from '@angular/core';
import { Optional } from '@angular/core';
import { Self } from '@angular/core';
import { ChangeDetectorRef } from '@angular/core';
import { EventEmitter } from '@angular/core';

import { MatSelect } from '@angular/material/select';
import { MatFormFieldControl } from '@angular/material/form-field';

import { NgControl } from '@angular/forms';

import {coerceBooleanProperty} from '@angular/cdk/coercion';

import { Subject } from 'rxjs';
import { ApiService } from '../../../services';
import { GenericApiResponse } from '../../../models';
import { FormField, FormConfig, WhereData } from '../../../models/general-models';


@Component({
    selector: 'chi-select',
    templateUrl: './picker.html',
    styleUrls: ['./picker.scss'],
    providers: [
        ApiService,
        {provide: MatFormFieldControl, useExisting: ForeignPickerComponent}
    ]
})
export class ForeignPickerComponent implements OnInit, AfterViewInit, MatFormFieldControl<string>, OnDestroy, OnChanges
{

    constructor(
        private apiService: ApiService,
        private _cd: ChangeDetectorRef,
        @Optional() @Self() public ngControl: NgControl
        )
    {
        this.where = null;
        this.selectedOption = null;
    }

    get value(): string | null
    {
        return this.mySelect ? this.mySelect.value : null;
    }
    set value(val: string | null)
    {
        if(this.mySelect)
        {
            this.mySelect.value = val;
            this.stateChanges.next();
        }
    }

    get placeholder()
    {
        if (this.field === void 0)
            return '';

        return this.field.placeholder;
    }

    get focused()
    {
        return this.mySelect ? this.mySelect.focused : false;
    }

    get empty()
    {
        return this.mySelect ? this.mySelect.empty : true;
    }

    @HostBinding('class.floating')
    get shouldLabelFloat()
    {
        return this.focused || !this.empty;
    }

    get required()
    {
        if (this.field === void 0)
            return false;

        return this.field.required;
    }

    @Input()
    get disabled() {
        return this.field.formControl.disabled;
        // return this._disabled;
        // return true;
    }
    set disabled(dis) {
        this._disabled = coerceBooleanProperty(dis);
        // this.field.formControl.disabled = this._disabled;

        // if (this.focused)
        // {
        //     this.mySelect.focused = false;
        //     this.stateChanges.next();
        // }

        // this.mySelect.disabled = this._disabled;

        this.stateChanges.next();
    }


    get errorState()
    {
        if (this.mySelect === void 0)
            return false;

        return this.mySelect.errorState;
    }


    get displayValue()
    {
        if (this.selectedOption != null)
        {
            return this.selectedOption[this.displayKey];
        }

        return null;
    }

    get displayMulValue()
    {
        if (this.selectedOption != null)
        {
            return this.selectedOption[this.field.foreign.columns[1]];
        }

        return null;
    }
    static nextId = 0;
    _previousValue: any;

    @Input() field: FormField;
    @Input() config: FormConfig;
    @Input() where: WhereData;
    @Input() tabindex:any;

    @Output() selected: EventEmitter<any> = new EventEmitter<any>();
    @Output() changeSelection: EventEmitter<any> = new EventEmitter<any>();
    @Output() signals: EventEmitter<any> = new EventEmitter<any>();

    @ViewChild(MatSelect) mySelect: MatSelect;

    stateChanges = new Subject<void>();
    selectedOption: any;
    displayKey: string;

    @HostBinding()
    id = `chi-select-${ForeignPickerComponent.nextId++}`;
    private _disabled = false;

    controlType = 'chi-select';

    autofilled?: boolean;

    @HostBinding('attr.aria-describedby') describedBy = '';

    ngOnInit()
    {
        this.apiService.apiSlug = this.field.foreign.foreign_table;
        this.apiService.primaryKey = this.field.foreign.foreign_column;
        this.displayKey = this.field.foreign.columns[0];

        if (this.field.foreign.where !== void 0)
            this.where = this.field.foreign.where;

        this.field.formControl.statusChanges.subscribe(s =>
        {
            this.stateChanges.next();
        });

        if (this.field.value != null) {
            this.loadSelectedData(this.field.value);
        }

        this.field.formControl.registerOnChange((e: any) =>
        {
            this.loadSelectedData(e);
        });

        // if (this.field.value == null && this.field.foreign.loadDefault)
        // {
        //     this.loadSelectedData(null);
        // }

        this._cd.detectChanges();
    }

    ngAfterViewInit(): void
    {
        console.log('MySelect = ', this.mySelect);
    }

    ngOnChanges(changes: SimpleChanges)
    {
        if (changes.disabled)
        {
            this.stateChanges.next();
        }

        if (this.where != null && changes.where)
        {
            this.loadForeignData('');
        }

        // this.mySelect.updateErrorState();

        // const newValue = this.field.formControl.value;
        // if (this._previousValue !== newValue)
        // {
        //     this._previousValue = newValue;
        //     this.stateChanges.next();
        // }

        // if (this.ngControl)
        // {
        //     this.updateErrorState();
        // }

        // this._dirtyCheckNativeValue();
    }

    ngOnDestroy()
    {
        this.stateChanges.complete();
    }

    setDescribedByIds(ids: string[])
    {
        this.describedBy = ids.join(' ');
    }

    onContainerClick(event: MouseEvent)
    {
        this.mySelect.open();
    }


    // ControlValueAccessor
    writeValue(obj: any): void
    {
        // console.log('writeValue => ', obj);
    }

    registerOnChange(fn: any): void
    {
        // console.log('registerOnChange => ', fn);
    }

    registerOnTouched(fn: any): void
    {
        // console.log('registerOnTouched => ', fn);
    }

    setDisabledState?(isDisabled: boolean): void
    {
        // console.log('setDisabledState => ', isDisabled);
    }

    // \ControlValueAccessor
    // errorState: boolean = false;

    updateErrorState()
    {

        this.mySelect.updateErrorState();
        // const oldState = this.errorState;
        // const parent = this._parentFormGroup || this._parentForm;
        // const matcher = this._defaultErrorStateMatcher;
        // const control = this.ngControl ? this.ngControl.control as FormControl : null;
        // const newState = matcher.isErrorState(control, parent);

        // if (newState !== oldState)
        // {
        //   this.errorState = newState;
          this.stateChanges.next();
        // }
    }

    onSelect(ev: any, data: any)
    {
        if (ev.isUserInput)
        {
            // console.log("onSelect->", ev, data);
            // this.setValue(data.id, data);

            // if value chagnes then call this
            if (data == null || this.field.value !== data.id) {
                this.changeSelection.emit({new: data, old: this.selectedOption});
            }

            if (data != null) {
                this.selectedOption = data;
                this.field.value = data.id;
                this.selected.emit(data);
            }
            else {
                this.selectedOption = null;
                this.field.value = null;
            }
        }
    }

    foreignOpenedChange(opened: boolean, search: string)
    {
        this.signals.emit({type: 'RVSelectOpened'});
        if (opened)
        {
            this.loadForeignData(search);
        }

        this.stateChanges.next();
    }

    searchRecords(search: string)
    {
        if(search !== '')
        {
            this.loadForeignData(search);
            this.stateChanges.next();
        }
        else
        {
            this.loadForeignData(search);
        }
    }

    advanceSearch(ev: any)
    {
        const e = {
            type: 'AdvanceSearch'
        }

        this.signals.emit(e);
    }

    loadSelectedData(val: any)
    {
        const payload = {
            columns: this.field.foreign.columns,
            slug: this.field.foreign.foreign_table,
            where: new WhereData({column: this.field.foreign.foreign_column, search: val})
        };

        // if (val)
        // {
        //     payload.where = new WhereData({column: this.field.foreign.foreign_column, search: val});
        // }
        // else if (this.where)
        // {
        //     payload.where = this.where;
        // }

        if (this.field.foreign.query_params) {
            for (const key in this.field.foreign.query_params) {
                payload[key] = this.field.foreign.query_params[key];
            }
        }

        this.field.loading = true;

        this.apiService.getSelectOptions(payload).then((response: GenericApiResponse) =>
        {
            if (response.Status === 'Ok')
            {
                this.field.options = response.data;
                this.selectedOption = response.data[0];

                // if (this.field.foreign.loadDefault && this.selectedOption)
                // {
                //     this.field.value = this.selectedOption[this.field.name];
                // }

                this.field.loading = false;
                this._cd.detectChanges();
            }
            else
            {
                this.field.loading = false;
            }

        }).catch( (error: any) =>
        {
            this.field.loading = false;
            console.log('Error loading foreign data', error);
        });
    }

    loadForeignData(search: string)
    {
        const payload = {
            columns: this.field.foreign.columns,
            slug: this.field.foreign.foreign_table,
            search: null,
            where: void 0,
            limit: void 0
        };

        if (search !== '')
        {
            payload.search = search;
        }

        if(this.where != null)
        {
            let whr = this.where;
            if (this.field.value != null) {
                const where = {group: 'or', children: [
                    this.where,
                    {column: this.field.foreign.foreign_column, search: this.field.value, op: 'eq'}
                ]};
                whr = where;
            }
            payload.where = whr;
        }

        if (this.field.foreign.query_params) {
            for (const key in this.field.foreign.query_params) {
                payload[key] = this.field.foreign.query_params[key];
            }
        }

        payload.limit = this.field.foreign.limit;

        this.field.loading = true;

        this.apiService.getSelectOptions(payload).then((response: GenericApiResponse) =>
        {
            if (response.Status === 'Ok')
            {
                this.field.options = response.data;
                this.field.loading = false;
            }
            else
            {
                this.field.loading = false;
            }
        }).catch( (error: any) => {
            this.field.loading = false;
            console.log('Error loading foreign data', error);
        });
    }
}
