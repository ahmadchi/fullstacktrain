import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { EcgChartAnalysisComponent } from './chart';
import { MaterialModule } from '../../material.module';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,

        MaterialModule
    ],
    declarations: [
        EcgChartAnalysisComponent
    ],
    exports: [
        EcgChartAnalysisComponent
    ],
})
export class EcgChartAnalysisModule
{
}
