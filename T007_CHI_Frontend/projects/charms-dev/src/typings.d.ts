interface CallerProfile {
    full_name: string;
    user_id: number;
    device_id: number;
    group_type: string;
    image: string;
}

interface CallConfig {
    ROOM_ID: string;
    TOKEN: string;
    CALL_TYPE: string;
    MAIN_DIV_ID: string;
    AUDIO_SAMPLE_RATE: number,
    WS_URL: string;
    WORK_URL_PREFIX: string;
    PROFILE: CallerProfile;
    SubjectClass: any;
}

interface CallAppListener {
    stop(): void;
    invite(): void;
}

declare class MainApp
{
    constructor(config: CallConfig, listener: CallAppListener);

    stop(): void;
    setupApp(): void;

}

declare class CComponent
{
    mInPins: any[];
    mOutPins: any[];
    mMute: boolean;

    toggleMute(): void;
    mute(): void;
    unMute(): void;
}

