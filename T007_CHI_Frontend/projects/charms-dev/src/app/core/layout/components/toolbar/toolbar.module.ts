import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseSharedModule, MaterialModule } from 'charms-lib';

import { ToolbarComponent } from './toolbar.component';


@NgModule({
    declarations: [
        ToolbarComponent
    ],
    imports     : [
        RouterModule,
        MaterialModule,
        FuseSharedModule,
    ],
    exports     : [
        ToolbarComponent
    ]
})
export class ToolbarModule
{
}
