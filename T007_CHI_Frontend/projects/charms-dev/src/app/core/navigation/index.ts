import { FuseNavigation } from 'charms-lib';


export const navigation: FuseNavigation[] = [
    {
        id       : 'dashboard',
        title    : 'Dashboard',
        type     : 'item',
        icon     : 'dashboard',
        url      : '/main-page',
        children : [
       ]
    },

    {
        id       : 'customers',
        title    : 'Customers',
        type     : 'collapsable',
        icon     : 'event_note',
        url      : '/general.module',
        children : [
            {
                id       : 'customers',
                title    : 'Customers',
                type     : 'item',
                url      : '/general.module/general.page-customer'
            },

             {
                id       : 'invoices',
                title    : 'Invoices',
                type     : 'item',
                url      : '/general.module/general.page-invoice'
            },

        ]
    },
    

    {
        id       : 'accounts',
        title    : 'Accounts',
        type     : 'collapsable',
        icon     : 'event_note',
        badge    : {
            title: '20',
            bg: 'red',
            fg: 'white'
        },
        children : [
            {
                id       : 'customers',
                title    : 'Customers',
                type     : 'item',
                url      : '/general.module/general.page-customer'
            },
            {
                id       : 'accounts',
                title    : 'Accounts',
                type     : 'item',
                url      : '/accounts/accounts'
            },
            {
                id       : 'transactions',
                title    : 'Transactions',
                type     : 'item',
                url      : '/accounts/transactions'
            },
            {
                id       : 'countries',
                title    : 'Countries',
                type     : 'item',
                url      : '/accounts/countries'
            },
            {
                id       : 'cities',
                title    : 'Cities',
                type     : 'item',
                url      : '/accounts/cities'
            },
        ]
    }
];
