
export const CountryControllerConfig = {
    slug: 'countries',
    key: 'country_id',
    title_s: 'Country',
    title_p: 'Countries',

    showHeader: true,
    showSearch: true,
    showAdd: true,
    showImport: false,
    showRowActions: true,
    inRoute: true,

    rowActions: [
        { icon: 'edit', toolTip: 'Edit', action: 'OnEdit', class: 'priamry-fg' },
        { icon: 'delete', toolTip: 'Delete', action: 'OnDelete', class: 'warn-fg' }

    ],

    table: {
        columns: [
            { name: 'country_id', visible: false },
            { name: 'country_name', title: 'Country Name',  sortable: true, sticky: true},
            { name: 'date_added', title: 'Added On', format:'datetime',  },
            { name: 'date_updated', title: 'Updated On', format:'datetime',  },
            { name: 'created_by_id', title: 'Created By',  },
            { name: 'created_by.full_name', title: 'Created By',  },

        ],
    },

    form: {
        columns:
        [
            { name: 'country_id', title: 'Id', type: 'hidden' },
            { name: 'country_name', title: 'Country Name', type: 'text', required: true },
        ]
    }
};
