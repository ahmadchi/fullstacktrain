
export const CityControllerConfig = {
    slug: 'cities',
    key: 'city_id',
    title_s: 'City',
    title_p: 'Cities',

    showHeader: true,
    showSearch: true,
    showAdd: true,
    showImport: false,
    showRowActions: true,
    inRoute: true,

    rowActions: [
        { icon: 'edit', toolTip: 'Edit', action: 'OnEdit', class: 'priamry-fg' },
        { icon: 'delete', toolTip: 'Delete', action: 'OnDelete', class: 'warn-fg' }

    ],

    table: {
        columns: [
            { name: 'city_id', visible: false },
            { name: 'city_name', title: 'City Name',  sortable: true},
            { name: 'country.country_name', title: 'Country', showInSearch: true, showFilter: true },
            { name: 'country.country_id', title: 'Country Id', },
            { name: 'date_added', title: 'Added On', format:'datetime',  },
            { name: 'date_updated', title: 'Updated On', format:'datetime',  },
            { name: 'created_by_id', title: 'Created By',  },
            { name: 'created_by.full_name', title: 'Created By',  },

        ],

        order: {column: 'country.country_name', dir: 'asc'},
    },

    form: {
        columns:
        [
            { name: 'city_id', title: 'Id', type: 'hidden', required: 'true' },
            { name: 'country_id', title: 'Country', type: 'foreign', required: true,
                foreign: {
                    mode: 'double', foreign_table: 'countries', foreign_column: 'country_id',
                    columns: ['country_name', 'created_by.full_name']
                }
            },
            { name: 'city_name', title: 'City Name', type: 'text', required: true },
        ]
    }
};
