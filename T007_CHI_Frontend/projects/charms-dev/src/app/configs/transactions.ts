
export const TransactionConfig = {
    slug: 'transactions',
    key: 'transaction_id',
    title_s: 'Transaction',
    title_p: 'Transactions',

    showHeader: true,
    showSearch: true,
    showAdd: true,
    showImport: false,
    showRowActions: true,
    inRoute: false,

    rowActions: [
        { icon: 'edit', toolTip: 'Edit', action: 'OnEdit', class: 'priamry-fg' },
        { icon: 'delete', toolTip: 'Delete', action: 'OnDelete', class: 'warn-fg' }

    ],

    table: {
        columns: [
            { name: 'transaction_id', visible: false },
            { name: 'from_account.account_name', title: 'From Account' },
            { name: 'to_account.account_name', title: 'To Account' },
            { name: 'amount', title: 'Amount' },
            { name: 'description', title: 'Description' },
            { name: 'date_added', title: 'Added On', format:'datetime',  },
            { name: 'date_updated', title: 'Updated On', format:'datetime',  },
            { name: 'created_by_id', title: 'Created By',  },
        ],

    },

    form: {
        columns:
        [
            { name: 'transaction_id', title: 'Id', type: 'hidden' },
            { name: 'from_account_id', title: 'From Account', type: 'foreign', required: false,
                foreign: {
                    mode: 'double', foreign_table: 'accounts', foreign_column: 'account_id',
                    columns: ['account_name', 'account_type.account_type_name']
                }
            },
            { name: 'to_account_id', title: 'To Account', type: 'foreign', required: true,
                foreign: {
                    mode: 'double', foreign_table: 'accounts', foreign_column: 'account_id',
                    columns: ['account_name', 'account_type.account_type_name']
                }
            },
            { name: 'amount', title: 'Amount', type: 'number', required: true },
            { name: 'description', title: 'Description', type: 'text' },
        ]
    }
};
