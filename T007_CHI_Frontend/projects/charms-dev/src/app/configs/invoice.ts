
export const InvoiceControllerConfig = {
    slug: 'invoices',
    key: 'invoice_id',
    title_s: 'Invoice',
    title_p: 'Invoices',

    showHeader: true,
    showSearch: true,
    showAdd: true,
    showImport: false,
    showRowActions: true,
    inRoute: true,

    rowActions: [
        { icon: 'edit', toolTip: 'Edit', action: 'OnEdit', class: 'priamry-fg' },
        { icon: 'delete', toolTip: 'Delete', action: 'OnDelete', class: 'warn-fg' }

    ],
    /*
    customer_id = CGColumn(Integer, primary_key = True)
    name = CGColumn(String(40))
    address = CGColumn(String(40))
    # address2 = Column(String(40))
    email = CGColumn(String(40))
    */
    table: {
        columns: [
            { name: 'invoice_id', visible: false },
            { name: 'amount', title: 'Invoice amount',  sortable: true},
            { name: 'date_added', title: 'Added On', format:'datetime',  },
            { name: 'date_updated', title: 'Updated On', format:'datetime',  },
            { name: 'created_by_id', title: 'Created By',  },
            { name: 'created_by.full_name', title: 'Created By',  },
        ],

        order: {column: 'amount', dir: 'asc'},
    },

    form: {
        columns:
        [
            { name: 'invoice_id', title: 'Id', type: 'hidden', required: true },
            { name: 'amount', title: 'Invoice amount', type: 'text', required: true },
           
        ]
    }
};
