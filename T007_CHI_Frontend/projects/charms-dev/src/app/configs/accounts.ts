
export const AccountControllerConfig = {
    slug: 'accounts',
    key: 'account_id',
    title_s: 'Account',
    title_p: 'Accounts',

    showHeader: true,
    showSearch: true,
    showAdd: true,
    showImport: false,
    showRowActions: true,
    inRoute: true,

    rowActions: [
        { icon: 'edit', toolTip: 'Edit', action: 'OnEdit', class: 'priamry-fg' },
        { icon: 'delete', toolTip: 'Delete', action: 'OnDelete', class: 'warn-fg' }

    ],

    table: {
        columns: [
            { name: 'account_id', visible: false },
            { name: 'account_name', title: 'Name',  sortable: true, sticky: true},
            { name: 'account_type.account_type_name', title: 'Account Type', showInSearch: true, showFilter: true },
            { name: 'account_type.account_type_id', title: 'Type Id', },
            { name: 'account_address', title: 'Address'},
            { name: 'account_balance', title: 'Balance'},
            { name: 'is_active', title: 'Active', format: 'bool' },
            { name: 'description', title: 'Description' },
            { name: 'date_added', title: 'Added On', format:'datetime',  },
            { name: 'date_updated', title: 'Updated On', format:'datetime',  },
            { name: 'created_by_id', title: 'Created By',  },
            { name: 'created_by.full_name', title: 'Created By',  },

        ],

        order: {column: 'account_type.account_type_name', dir: 'asc'},
    },

    form: {
        columns:
        [
            { name: 'account_id', title: 'Id', type: 'hidden' },
            { name: 'account_type_id', title: 'Account Type', type: 'foreign', required: true,
                foreign: {
                    mode: 'double', foreign_table: 'account_types', foreign_column: 'account_type_id',
                    columns: ['account_type_name', 'created_by.full_name']
                }
            },
            { name: 'account_name', title: 'Account Name', type: 'text', required: true },
            { name: 'account_address', title: 'Address', type: 'text', required: false },
            { name: 'account_balance', title: 'Balance', type: 'number', required: true },
            { name: 'description', title: 'Description', type: 'text' },
            { name: 'is_active', title: 'Is Active', type: 'switch' }
        ]
    }
};
