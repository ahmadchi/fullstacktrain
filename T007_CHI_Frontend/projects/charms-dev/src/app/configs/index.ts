
import { AccountTypeControllerConfig } from './account_types';
import { AccountControllerConfig } from './accounts';
import { TransactionConfig } from './transactions';
import { CountryControllerConfig } from './countries';
import { CityControllerConfig } from './cities';
import { CustomerControllerConfig } from './customers';

export const ControllerConfigs = [
    {name: 'account-types', ctrl: AccountTypeControllerConfig},
    {name: 'customers', ctrl: CustomerControllerConfig},
    {name: 'transactions', ctrl: TransactionConfig},
    {name: 'countries', ctrl: CountryControllerConfig},
    {name: 'cities', ctrl: CityControllerConfig}
    
];