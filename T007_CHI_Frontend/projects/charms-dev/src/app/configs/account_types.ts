
export const AccountTypeControllerConfig = {
    slug: 'account_types',
    key: 'account_type_id',
    title_s: 'Account Type',
    title_p: 'Account Types',

    showHeader: true,
    showSearch: true,
    showAdd: true,
    showImport: false,
    showRowActions: true,
    inRoute: true,

    rowActions: [
        { icon: 'edit', toolTip: 'Edit', action: 'OnEdit', class: 'priamry-fg' },
        { icon: 'delete', toolTip: 'Delete', action: 'OnDelete', class: 'warn-fg' }

    ],

    table: {
        columns: [
            { name: 'account_type_id', visible: false },
            { name: 'account_type_name', title: 'Name',  sortable: true, sticky: true},
            { name: 'date_added', title: 'Added On', format:'datetime',  },
            { name: 'date_updated', title: 'Updated On', format:'datetime',  },
            { name: 'created_by_id', title: 'Created By',  },
            { name: 'created_by.full_name', title: 'Created By',  },

        ],
    },

    form: {
        columns:
        [
            { name: 'account_type_id', title: 'Id', type: 'hidden' },
            { name: 'account_type_name', title: 'Account Type Name', type: 'text', required: true },
            { name: 'description', title: 'Description', type: 'text' },
        ]
    }
};
