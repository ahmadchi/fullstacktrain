import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: 'main-page',
        loadChildren: () => import('./pages/main-page/module').then(m => m.MainModule)
    },
    {
        path: 'general.module',
        loadChildren: () => import('./pages/general.module/module').then(m => m.GeneralPageInsideComponentModule)
    },
    {
        path      : '**',
        redirectTo: 'main-page'
    }
]

@NgModule({
    imports: [
        RouterModule.forRoot(routes),
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }
