import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule, GeneralModule, GeneralPageComponent, GeneralFormComponent } from 'charms-lib';
import { Routes, RouterModule } from '@angular/router';
//import { AccountTypePageComponent } from './account_type/account_type.component';

const routes: Routes = [
    { path: 'account-types', component: GeneralPageComponent, data: {name: 'account-types'} },
    { path: 'accounts', component: GeneralPageComponent, data: {name: 'accounts'} },
    { path: 'transactions', component: GeneralPageComponent, data: {name: 'transactions'} },
    { path: 'countries', component: GeneralPageComponent, data: {name: 'countries'} },
    { path: 'cities', component: GeneralPageComponent, data: {name: 'cities'} },
];

@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
        GeneralModule,

        RouterModule.forChild(routes)
    ],
    declarations: [
       //AccountTypePageComponent
    ],
    exports: [
        //AccountTypePageComponent
    ],
    providers: [],
})
export class AccountsModule { }
