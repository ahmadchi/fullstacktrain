import { OnInit } from '@angular/core';
import { Component } from '@angular/core';
import { ChiConfigService, ApiService, ControllerConfig } from 'charms-lib';
import { CustomerControllerConfig } from './customer.page.config';



@Component({
    selector: 'customer.page.component',
    templateUrl: 'customer.page.component.html',
    styleUrls: ['customer.page.component.scss']
})
export class CustomerPageComponent implements OnInit
{
    config: ControllerConfig;

    constructor(
        private apiService: ApiService,
        private configService: ChiConfigService)
    {
        this.config = new ControllerConfig(CustomerControllerConfig)
    }

    ngOnInit(): void
    {

    }
}

