
export const CustomerControllerConfig = {
    slug: 'customers',
    key: 'customer_id',
    title_s: 'The Customer',
    title_p: 'My Customers',

    showHeader: true,
    showSearch: true,
    showAdd: true,
    showImport: false,
    showRowActions: true,
    inRoute: true,

    rowActions: [
        { icon: 'edit', toolTip: 'Edit', action: 'OnEdit', class: 'priamry-fg' },
        { icon: 'delete', toolTip: 'Delete', action: 'OnDelete', class: 'warn-fg' }

    ],

    table: {
        columns: [
            { name: 'customer_id', visible: false },
            { name: 'name', title: 'Customer Name',  sortable: true},
            { name: 'address', title: 'Customer address',  sortable: true},
            { name: 'email', title: 'Customer email',  sortable: true},
            { name: 'date_added', title: 'Added On', format:'datetime',  },
            { name: 'date_updated', title: 'Updated On', format:'datetime',  },
            { name: 'created_by_id', title: 'Created By',  },
            { name: 'created_by.full_name', title: 'Created By',  },
        ],

        order: {column: 'name', dir: 'asc'},
    },

    form: {
        columns:
        [
            { name: 'customer_id', title: 'Id', type: 'hidden', required: true },
            { name: 'name', title: 'Customer Name', type: 'text', required: true },
            { name: 'address', title: 'Address', type: 'text', required: false },
            { name: 'email', title: 'Email', type: 'text', required: false},
        ]
    }
};
