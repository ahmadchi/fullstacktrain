import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule, GeneralModule} from 'charms-lib';
import { Routes, RouterModule } from '@angular/router';
import { AppRoutesGuard } from '../../route-guards/app.routes.guard';
import { CustomerPageComponent } from './general-page-customer/customer.page.component';
import { InvoicePageComponent } from './general-page-invoice/invoice.page.component';


const routes: Routes = [
   
     { path: 'general.page-customer', component: CustomerPageComponent},
      { path: 'general.page-invoice', component: InvoicePageComponent}
];

@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
        GeneralModule,
       
        RouterModule.forChild(routes),
     
    ],
    declarations: [
        CustomerPageComponent,
        InvoicePageComponent

    ],
    exports: [
        CustomerPageComponent,
        InvoicePageComponent
        
        

    ],
    providers: [],
})
export class GeneralPageInsideComponentModule { }
