import { OnInit } from '@angular/core';
import { Component } from '@angular/core';
import { ChiConfigService, ApiService, ControllerConfig } from 'charms-lib';
import { InvoiceControllerConfig } from '../../../configs/invoice';



@Component({
    selector: 'invoice.page.component',
    templateUrl: 'invoice.page.component.html',
    styleUrls: ['invoice.page.component.scss']
})
export class InvoicePageComponent implements OnInit
{
    config: ControllerConfig;

    constructor(
        private apiService: ApiService,
        private configService: ChiConfigService)
    {
        this.config = new ControllerConfig(InvoiceControllerConfig)
    }

    ngOnInit(): void
    {

    }
}

