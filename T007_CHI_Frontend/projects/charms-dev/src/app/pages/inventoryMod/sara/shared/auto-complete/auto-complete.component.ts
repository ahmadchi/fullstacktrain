import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormField, ApiService, GenericApiResponse, RVAlertsService } from 'charms-lib';
import { MatFormFieldControl } from '@angular/material/form-field';
import { MatMenuTrigger } from '@angular/material/menu';
import { HttpClient } from '@angular/common/http';
import { debounceTime, distinctUntilChanged, switchMap, filter } from 'rxjs/operators';


@Component({
  selector: 'chi-auto-complete',
  templateUrl: 'auto-complete.component.html',
  styleUrls: ['auto-complete.component.scss'],
  providers: [
    ApiService,
    {provide: MatFormFieldControl, useExisting: AutoCompleteComponent}
  ]
})
export class AutoCompleteComponent implements OnInit
{
    @Input() field: FormField;
    @Output() selected = new EventEmitter();

    @ViewChild(MatMenuTrigger) public menuTrigger: MatMenuTrigger;

    data: any;
    loading: boolean;
    selectedValue: string;

	constructor(private apiService: ApiService, private http: HttpClient)
	{
        this.field = null;

        this.data = null;
        this.loading = false;
        this.selectedValue = '';
	}
	
	ngOnInit(): void 
	{
        this.field.formControl.valueChanges.pipe(
            debounceTime(300),
            distinctUntilChanged(),
            filter(value => value != this.selectedValue),
            switchMap((value: string) => 
            {
                return this.getData(value);
            }))
            .subscribe(resp => {
                this.data = resp['data'];
                this.loading = false;

                if (this.data.length == 0)
                {
                    this.selected.emit({value: this.field.formControl.value});
    
                    // If no result then close menu after 1 second;
                    setTimeout(() => {
                        this.menuTrigger.closeMenu();
                    }, 1000);
                }
            }
        );
    }

    onMenuOpen(): void
    {
        this.loadData();
    }

    loadData(search=null): void
    {
        this.loading = true;
        this.data = null;

        const payload = {
            text_search: search
        }

        this.apiService.post(this.field.api, payload).then( (resp: GenericApiResponse) => {
            this.loading = false;
            this.data = resp.data;

            if (search && this.data.length == 0)
            {
                this.selected.emit({value: this.field.formControl.value});

                // If no result then close menu after 1 second;
                setTimeout(() => {
                    this.menuTrigger.closeMenu();
                }, 1000);
            }

        }, (error: GenericApiResponse) => {
            this.loading = false;
            RVAlertsService.error('Error loading auto-complete data.', error.ErrorMessage);
        });
    }

    getData(text: string)
    {
        this.loading = true;
        this.data = null;

        if (this.menuTrigger.menuClosed)
        {
            this.menuTrigger.openMenu();
        }

        const payload = {
            text_search: text
        }

        return this.http.post('api/' + this.field.api, payload)
    }

    onSelect(record: any): void
    {
        this.field.formControl.setValue(record.service_name);
        this.selected.emit({value: record.service_name});
        this.selectedValue = record.service_name;
    }
}
