import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule, GeneralModule, DatePickerModule, DateTimePickerModule, TimePickerModule, SelectModule, MultiSelectionModule} from 'charms-lib';
import { Routes, RouterModule } from '@angular/router';

import { DatePickerOneComponent } from './date-picker/date.picker.component';
import { DatePickerCustomComponent } from './date-picker.custom/date.picker.custom.component';
import {DateTimePickerCustomComponent} from './datetime-picker.custom/datetime.picker.custom.component'

import { NgxDropzoneModule } from 'ngx-dropzone';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { MatNativeDateModule } from '@angular/material/core';
import { TimePickerCustomComponent } from './time-picker/time.picker.component';
import { ChiFilePickerModule } from 'projects/charms-lib/src/lib/components/controls/file-picker/module';
import { FilePickerCustomComponent } from './file-picker/filepicker.component';
import { ForeignPickerCustomComponent } from './foreign-picker/foreignpicker.component';
import { MultiSelectCustomComponent } from './multi-select/multi-select-custom.component';
import { AllergyFormComponent } from './multi-select/add-allergy-form/form';
import { LineItemModule } from '../shared/line-item/module';
import { LineItemCustomComponent } from './line-item/line-item-custom.component';

const routes: Routes = [
  
    { path: 'one', component: DatePickerOneComponent},
    { path: 'custom', component: DatePickerCustomComponent},
    { path: 'datetime', component: DateTimePickerCustomComponent},
    { path: 'time', component: TimePickerCustomComponent},
    { path: 'filepicker', component: FilePickerCustomComponent},
    { path: 'foreignpicker', component: ForeignPickerCustomComponent},
    { path: 'multi-select', component: MultiSelectCustomComponent},
    { path: 'line-item', component: LineItemCustomComponent},
    //
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        LineItemModule,
        MaterialModule,
        GeneralModule,
        DatePickerModule,
        DateTimePickerModule,
        TimePickerModule,
        ChiFilePickerModule,
        ReactiveFormsModule,
        NgxDropzoneModule,
        SelectModule,
        MultiSelectionModule,
        LineItemModule,
        
        //Modules Custom date picker 
        MatDatepickerModule,
        MatNativeDateModule,
        RouterModule.forChild(routes),
     
    ],
    declarations: [
        DatePickerOneComponent,
        DatePickerCustomComponent,
        DateTimePickerCustomComponent,
        TimePickerCustomComponent,
        FilePickerCustomComponent,
        ForeignPickerCustomComponent,
        AllergyFormComponent,
        MultiSelectCustomComponent,
        LineItemCustomComponent

       

    ],
    exports: [
        DatePickerOneComponent,
        DatePickerCustomComponent,
        DateTimePickerCustomComponent,
        TimePickerCustomComponent,
        FilePickerCustomComponent,
        ForeignPickerCustomComponent,
        MultiSelectCustomComponent,
        AllergyFormComponent,
        LineItemCustomComponent

         

    ],
    providers: [],
})
export class ControlsModule { }
