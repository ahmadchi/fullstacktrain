import {
  ApiService,
} from 'charms-lib';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {
  Component,
  OnInit,
  ViewEncapsulation,
} from '@angular/core';
import { FormField, GenericApiResponse, RVAlertsService } from 'charms-lib';

@Component({
  selector: 'purchase-orders',
  templateUrl: 'purchase-orders.component.html',
  styleUrls: ['purchase-orders.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PurchaseOrdersComponent implements OnInit {
  purchase_requisition: FormField;
  date : FormField;
  supplier_quotation_date : FormField;
  supplier_quotation_number : FormField;
  payment_mode : FormField;
  supplier : FormField;
  document_mode : FormField;
  document_category : FormField;  
  PurchaseOrderForm : FormGroup;
  indeterminate = false;

  constructor(private apiService : ApiService){

    this.date = new FormField()
    this.supplier_quotation_date = new FormField()
    this.supplier_quotation_number = new FormField()
    this.supplier_quotation_date = new FormField()
    this.supplier_quotation_date = new FormField()
    this.supplier_quotation_date = new FormField()
    this.supplier_quotation_date = new FormField()
    this.supplier_quotation_date = new FormField()
    this.supplier_quotation_date = new FormField()
  }
  ngOnInit(): void {
    
    this.PurchaseOrderForm = new FormGroup({})
    this.PurchaseOrderForm.addControl('date', this.date.formControl);
    this.PurchaseOrderForm.addControl('supplier_quotation_date', this.supplier_quotation_date.formControl);
    this.PurchaseOrderForm.addControl('supplier_quotation_number', this.supplier_quotation_number.formControl);
    this.PurchaseOrderForm.addControl('payment_mode', this.payment_mode.formControl);
    this.PurchaseOrderForm.addControl('supplier', this.supplier.formControl);
    this.PurchaseOrderForm.addControl('remarks', this.date.formControl);
    this.PurchaseOrderForm.addControl('document_mode', this.document_mode.formControl);
    this.PurchaseOrderForm.addControl('document_category', this.document_category.formControl);
    // document_mode

  }

  onForeignSelected(ev: any) {
    console.log('ev');
  }

}
