import { Component, QueryList, ViewChildren } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';import {  FormField } from 'charms-lib';
import { LineItemComponent } from '../../shared/line-item/line-item.component';

@Component({
    selector: 'line-item-custom',
    templateUrl: 'line-item-custom.component.html',
    styleUrls: ['line-item-custom.component.scss']
})
export class LineItemCustomComponent
{

    time: FormField;
    @ViewChildren(LineItemComponent) lineItems: QueryList<LineItemComponent>;
    lineItemConfig: any;
    lineItemData = [];
    

    constructor()
    {
        this.time = new FormField({ name: 'date', title: 'Select Birth Date', type: 'time', required: true });
      
    }

    ngOnInit(): void
    {  
        this.lineItemConfig = {
            name: 'inventories',
            items: [
                {
                    editing: true,
                    fields: [
                        {
                            title: 'Inventory',
                            value: null,
                            name: 'inventory_id',
                            displayValue: null,
                            field: 'foreign',
                            required: true,
                            options: {
                                name: 'inventory_id',
                                title: 'Inventory',
                                required: true,
                                type: 'foreign',
                                foreign: {
                                    foreign_table: 'inventories',
                                    foreign_column: 'inventory_id',
                                    columns: ['inventory_name'],
                                },
                            },
                        },
                        {
                            title: 'Requested Quantity',
                            value: null,
                            field: 'input',
                            name: 'requested quantity',
                            type: 'number',
                            required: false,
                        },
                        {
                            title: 'Estimated_Rate',
                            value: null,
                            field: 'input',
                            name: 'est_rate',
                            type: 'number',
                            required: false,
                        },
                        {
                            title: 'Estimated Arrival Date',
                            value: null,
                            field: 'date',
                            name: 'est_arrival_date',
                            required: false,
                        },
                        {
                            title: 'Approved Quantity',
                            value: null,
                            field: 'input',
                            name: 'approved_quantity',
                            type: 'number',
                            required: false,
                        },
                    ],
                },
            ],
        };
     
    }

}