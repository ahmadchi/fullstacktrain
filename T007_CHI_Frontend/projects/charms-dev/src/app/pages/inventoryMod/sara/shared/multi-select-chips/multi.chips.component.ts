import { ControllerConfig, TableConfig, FormConfig, ApiService} from 'charms-lib';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {Component, Input, OnInit ,ElementRef, ViewChild} from '@angular/core';
import { FormField, GenericApiResponse, RVAlertsService, GeneralFormComponent } from 'charms-lib';
import {Observable} from 'rxjs';
import {MatAutocompleteSelectedEvent, MatAutocomplete} from '@angular/material/autocomplete';
import {MatChipInputEvent} from '@angular/material/chips';
import {map, startWith} from 'rxjs/operators';
import { AppointmentControllerConfig } from 'projects/charms-dev/src/app/pages/accounts/account_type/account_type-config';

@Component({
    selector: 'multi-select',
    templateUrl: 'multi.chips.component.html',
    styleUrls: ['multi.chips.component.scss'],
   
})

export class MultiSelectComponent implements OnInit
{
  visible: boolean = true;
  selectable: boolean = true;
  removable: boolean = true;
  addOnBlur: boolean = false;
  separatorKeysCodes = [ENTER, COMMA];
  ctrl = new FormControl();
  filteredIput: Observable<any[]>;

  input = [
    'Oppenheimer',
  ];
  resp : any;
  dropdown = [
    'Oppenheimer',
    'Nietzsche',
    'Vlad the Impaler',
    'Einstein',
    'Pierre de Fermat'
  ];
  @ViewChild('userInput') userInput: ElementRef;


  constructor(private apiService: ApiService)
  {
    this.filteredIput = this.ctrl.valueChanges.pipe(
        map((mInput: string | null) => mInput ? this.filter(mInput) : this.dropdown.slice()));

        console.log(this.filteredIput)
  }
    ngOnInit(){}


    add(event: MatChipInputEvent): void {
        const input = event.input;
        const value = event.value;
        // Add our mInput
        if ((value || '').trim()) {
        this.input.push(value.trim());
        console.log("==============================",value.trim())
        this.List()
        }
        // Reset the input value
        if (input) {
        input.value = '';
        }
        this.ctrl.setValue(null);
    }
    remove(mInput: any): void {
        const index = this.input.indexOf(mInput);

        if (index >= 0) {
        this.input.splice(index, 1);
        }
    }
    filter(name: string) {
    return this.dropdown.filter(mInput =>
        mInput.toLowerCase().indexOf(name.toLowerCase()) === 0);
    }
    selected(event: MatAutocompleteSelectedEvent): void {
        this.input.push(event.option.viewValue);
        this.userInput.nativeElement.value = '';
        this.ctrl.setValue(null);
        //git push origin master:ahmad
    }

    List(){
        {
          this.apiService.post('/formulas/List', {}).then( (resp: GenericApiResponse) =>
          {
              this.resp = resp.data;
              console.log(this.resp)
              
          }, (error: GenericApiResponse) =>
          {
              RVAlertsService.error('Error getting active manufacturers.', error.ErrorMessage);
          });
      }
      }


}

