import {
  ApiService,
} from 'charms-lib';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {
  Component,
  OnInit,
  ViewEncapsulation,
} from '@angular/core';
import {
  FormField,
  GenericApiResponse,
  RVAlertsService,
} from 'charms-lib';
import { Subject } from 'rxjs';

@Component({
  selector: 'units',
  templateUrl: 'units.component.html',
  styleUrls: ['units.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class UnitsComponent implements OnInit {

  tableConfig = {
    name:'units',
    topTitle:'Update Existing Units',
    subTitle:'Select Unit',
    columns: [{name:'Description', key:'description'}, {name:'Default Option', key:'is_default'}]
  }
  units: FormGroup;
  actions =  new Subject<any>()

  constructor(private apiService : ApiService){}
  ngOnInit(): void {

    this.units = new FormGroup({
      description: new FormControl('', [Validators.required]),
      is_default: new FormControl('', [Validators.required]),
    });
  }

  Save() {
    const payload = this.units.value;
    this.apiService.post('/units/Create', payload).then(
      (resp: GenericApiResponse) => {
        this.actions.next({type:'reload'});
        console.log(resp)
      })
  }
}
