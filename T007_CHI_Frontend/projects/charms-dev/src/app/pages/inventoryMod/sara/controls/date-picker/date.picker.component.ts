import { Component } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';import {  FormField } from 'charms-lib';

@Component({
    selector: 'date.picker.component',
    templateUrl: 'date.picker.component.html',
    styleUrls: ['date.picker.component.scss']
})
export class DatePickerOneComponent
{

    date: FormField;

    constructor()
    {
        this.date = new FormField({ name: 'date', title: 'Select Birth Date', type: 'date', required: true });
      
    }

    ngOnInit(): void
    {  
      
     
    }
}