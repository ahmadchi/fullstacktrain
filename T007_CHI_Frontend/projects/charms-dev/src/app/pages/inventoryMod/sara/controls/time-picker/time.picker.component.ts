import { Component } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';import {  FormField } from 'charms-lib';

@Component({
    selector: 'time.picker.component',
    templateUrl: 'time.picker.component.html',
    styleUrls: ['time.picker.component.scss']
})
export class TimePickerCustomComponent
{

    time: FormField;

    constructor()
    {
        this.time = new FormField({ name: 'date', title: 'Select Birth Date', type: 'time', required: true });
      
    }

    ngOnInit(): void
    {  
      
     
    }

}