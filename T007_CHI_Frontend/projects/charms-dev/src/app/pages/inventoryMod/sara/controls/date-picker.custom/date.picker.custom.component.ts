import { Component , Output, EventEmitter, ViewChild, Renderer2, AfterViewInit } from '@angular/core';



@Component({
    selector: 'date.picker.custom.component',
    templateUrl: 'date.picker.custom.component.html',
    styleUrls: ['date.picker.custom.component.scss']
})
export class DatePickerCustomComponent
{
    selectedDate: any;

    onSelect(event){
      console.log(event);
      this.selectedDate= event;

    }
}