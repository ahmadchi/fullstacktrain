import { Component, OnInit, Input, SimpleChanges, OnChanges, EventEmitter, Output } from '@angular/core';
import { LineItemModel } from './models';
import { FormField, RVAlertsService } from 'charms-lib';

import { LineItemSelectionEvent } from '../../../common/models';
import { MatCheckboxChange } from '@angular/material/checkbox';

import { Helpers } from '../../../common/helpers';


@Component({
  selector: 'line-item',
  templateUrl: 'line-item.component.html',
  styleUrls: ['line-item.component.scss'],
})
export class LineItemComponent implements OnInit, OnChanges
{
    /*
    {
        "name": "diagnostics",
        "items": [
            "editing": true,
            'fields': [
                {
                    title: 'Diagnostics',
                    value: null,
                    name: "diagnostic_id",
                    displayValue: null,
                    field: 'foreign',
                    options: { name: 'vital_id', title: 'Diagnostics', required: true, type: 'foreign',
                        foreign: { foreign_table: 'vitals', foreign_column: 'vital_id', columns: ['vital_name'],
                        where: new WhereData({
                            column: 'vital_category.vital_category_name',
                            search: ['Diagnostics'],
                            op: 'eq'
                        }) }
                    },
                    required: true
                },
                {
                    title: 'Remarks',
                    value: null,
                    field: 'input',
                    name: 'instructions',
                    type: 'text',
                    required: false
                }
            ]
        ]
    }
    */
    @Input() config: LineItemModel;

    @Input() data: any;

    @Input() readOnly: boolean;
    @Input() defaultEdit: boolean;

    @Input() enableSelection: boolean;

    // tslint:disable-next-line: no-output-native
    @Output() change: EventEmitter<any> = new EventEmitter<any>();
    @Output() selection: EventEmitter<LineItemSelectionEvent> = new EventEmitter<LineItemSelectionEvent>();
    @Output() removelineitem: EventEmitter<any> = new EventEmitter<any>();

    internalConfig: LineItemModel;

    selectedRecords = [];

    constructor()
    {
        this.config = null;
        this.data = null;
        this.internalConfig = null;

        this.readOnly = false;
        this.defaultEdit = true;
        this.enableSelection = false;
    }

    ngOnInit(): void
    {
        this.internalConfig = {name: this.config.name, items: []};
        if (this.data) {
            this.populateTable('OnInit');
        }
        else
        {
            this.onAddItem();
        }
        // if (!this.readOnly) {
        //     this.onAddItem();
        // }
    }

    ngOnChanges(changes: SimpleChanges): void
    {
        if (changes.readOnly && changes.readOnly.currentValue)
        {
            this.readOnly = changes.readOnly.currentValue;
        }

        if (changes.hasOwnProperty('data') && changes.data.currentValue)
        {
            this.data = changes.data.currentValue;
            this.populateTable('OnChanges');
        }
    }

    populateTable(from: string)
    {
        if (this.data && this.data.length > 0)
        {
            // Reset Config;
            this.internalConfig = {name: this.config.name, items: []};
            this.selectedRecords = [];

            for (let i=0; i<this.data.length; i++)
            {
                this.onAddItem();

                const internalItemToUpdate = this.internalConfig.items[i];
                const itemFromData = this.data[i];

                if (itemFromData.id == null || this.defaultEdit) {
                    internalItemToUpdate.editing = true;
                }
                else {
                    internalItemToUpdate.editing = false;
                }

                if (this.enableSelection)
                {
                    internalItemToUpdate.selected = true;
                    this.selectedRecords.push(internalItemToUpdate);
                }

                internalItemToUpdate.id = itemFromData.id;

                for (const field of internalItemToUpdate.fields)
                {
                    field.value = itemFromData[field.name];

                    if (field.field === 'foreign' || field.field === 'autocomplete')
                    {
                        field.options.formControl.setValue(itemFromData[field.name]);
                        field.displayValue = itemFromData[field.name + '_name'];
                    }
                }
            }
        }

        if (this.data.length === 0)
        {
            this.internalConfig = {name: this.config.name, items: []};
            this.onAddItem();
        }

        if (this.enableSelection)
        {
            this.emitSelection();
        }
    }

    getData(): any
    {
        const data = {name: this.internalConfig.name, items: []};

        for (const item of this.internalConfig.items)
        {
            const dynamicItem = {id: item.id, is_deleted: item.deleted};
            for (const field of item.fields)
            {
                if (!item.deleted && field.required && field.value == null)
                {
                    RVAlertsService.error('Invalid data', `Field ${field.title} is required.`);
                    return null;
                }
                dynamicItem[field.name] = field.value;
            }

            if (Object.keys(dynamicItem).length !== 0)
            {
                data.items.push(dynamicItem);
            }
        }

        return data;
    }

    getTitles(): string[]
    {
        const titles: any[] = [];

        if (this.enableSelection)
        {
            titles.push({title: 'Selection', type: 'checkbox'});
        }

        for (const field of this.config.items[0].fields)
        {
            titles.push({title: field.title, type: field.type, subText: field.subText});
        }

        return titles;
    }

    onAddItem(): void
    {
        const item = JSON.parse(JSON.stringify(this.config.items[0]));
        const internalItem = (this.readOnly) ? {id: null, editing: false, deleted: false, fields: []} : {id: null, editing: true, deleted: false, fields: []};

        for (const field of item.fields)
        {
            if (field.field === 'foreign' || field.field === 'autocomplete')
            {
                const formField = new FormField(field.options);
                field.options = formField;
            }

            internalItem.fields.push(field);
        }

        this.internalConfig.items.push(internalItem);
    }

    integerOnly(ev: any): boolean
    {
        return Helpers.IntegerOnly(ev);
    }

    alphaOnly(ev: any, field: any): boolean
    {
        return Helpers.AlphaOnly(ev, field.allowedCodes);
    };

    onEdit(record: any): void
    {
        record.editing = true;
    }

    onOk(record: any): void{
        record.editing = false;
    }

    onDeleteItem(record: any)
    {
        record.deleted = true;
        this.removelineitem.emit();
    }

    onForeignChange(ev: any, field: any, item: any)
    {
        field.value = field.options.value;
        field.displayValue = ev[field.options.foreign.columns[0]];
        // console.log('onForeignChange', ev);
        this.modelChange(ev, field, item);
    }

    onAutoCompleteSelection(ev: any, field: any)
    {
        // console.log('Selection Change AutoComplete =', ev);
        field.value = ev.value;
        field.displayValue = ev.value;
    }

    modelChange(ev: any, field: any, item: any)
    {
        this.change.emit({event: ev, field, item, name: this.config.name});
    }

    onRowCheckChange(ev: MatCheckboxChange, rec: any): void
    {
        console.log('Rec = ', rec);
        if (ev.checked)
        {
            this.selectedRecords.push(rec);
        }
        else
        {
            const idx = this.selectedRecords.findIndex(item => item.id === rec.id);
            if (idx >= 0)
            {
                this.selectedRecords.splice(idx, 1);
            }
        }

        this.emitSelection();
    }

    emitSelection(): void
    {
        const signal: LineItemSelectionEvent = {
            type: 'selected',
            data: this.selectedRecords
        };

        this.selection.emit(signal);
    }
}
