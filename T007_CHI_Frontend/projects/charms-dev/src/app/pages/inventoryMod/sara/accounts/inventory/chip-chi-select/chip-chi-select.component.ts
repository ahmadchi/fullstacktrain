import { Component, ChangeDetectorRef, Output, EventEmitter, Input} from '@angular/core';
import { ChiConfigService, RVAlertsService, GenericApiResponse, ApiService, FormField, RVAlertAction, WhereData } from 'charms-lib';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';

@Component({
    selector: 'chip-chi-select',
    templateUrl: './chip-chi-select.component.html',
    styleUrls: ['./chip-chi-select.component.scss']
})
export class ChipChiSelectorComponent
{
    account_type_field: any;
    indexColComp: any;
    where: any;
    @Output() selected = new EventEmitter<any>();
    @Output() dataEmitter = new EventEmitter<any>();
    @Input() position: string ="bottom";
    // allValues: any[] = [];
    // indexValues: number[] = [];
    @Input() dataDict: {};
    // @Output() selectedValues = new EventEmitter<any>();
     elements: string[] = [];
     elementIds: number[] = [];
    // @Input() Reset:boolean = true;
    @Input() field: FormField;

    constructor(
        private apiService: ApiService,
        private configService: ChiConfigService,
        protected dialogRef: MatDialogRef<any>,//SettingsObservationsFormComponent>, 
        protected dialog: MatDialog,
        private _cdr: ChangeDetectorRef, ) {
        
        
    }
    emitUpdateData()
    {
        this.dataEmitter.emit(this.elementIds);
    }
    
    onSelectSpeciality(ev: any)
    {
        console.log(ev);
        this.indexColComp = ev[this.dataDict['indexColName']];
        if (!this.elementIds.includes(ev[this.dataDict['indexColName']]) && ev[this.dataDict['indexColName']]) {
            this.elementIds.push(ev[this.dataDict['indexColName']]);
            this.elements.push(ev[this.dataDict['colName']]);
            console.log(this.elementIds, this.elements);
            this.emitUpdateData();
        }
        //this.loadSpeciality();
    }

    onSelect(event: any)
    {
        console.log(event);
    }

    // loadFormulasChip(ev: any)
    // {
    //     this.selectedFormulaIds = ev.ids;
    //     this.selectedFormulas = ev.values;
    // }
 
    // loadSpeciality()
    // {
    //     const payload: any = {};
    //     payload.column = this.dataDict['slug']+"."+this.dataDict['indexColName'];
    //     payload.search = [this.indexColComp];
    //     payload.op = 'eq';
    //     this.where = payload;
    //     this._cdr.detectChanges();
    // }

    // fetchAndPopulate()
    // {
    //     this.apiService.post(this.slug+'/List', { columns: [this.colName] }).then(response => {
    //         console.log(response);
    //         for (let i = 0; i < response.data.length; i++)
    //         {
    //             this.allValues.push(response.data[i][this.colName]); 
    //             this.indexValues.push(response.data[i][this.indexColName]);
    //         }
    //     }, (error: GenericApiResponse) => {
    //         RVAlertsService.error('Error Posting The Medicine', error.ErrorMessage)
    //     });
    // }
    // ngOnChanges()
    // {
    //     if (this.Reset)
    //     {
    //         this.elementIds = [];
    //         this.elements = [];
    //     }

    // }
    

    // ngOnInit(): void
    // {
    //     this.fetchAndPopulate();
    // }

    // onSelect(value: any)
    // {
    //     console.log(value);
    //         this.elements.push(value);
    //     }
    //     console.log(this.elements, this.elementIds);          
        
    // }

    deleteValue(value: any)
    {
        this.elements = this.elements.filter(item => item !== value);
        this.elementIds.splice(this.elements.indexOf(value),1);
        console.log(this.elements, this.elementIds);
        this.emitUpdateData();
    }

    

    
}