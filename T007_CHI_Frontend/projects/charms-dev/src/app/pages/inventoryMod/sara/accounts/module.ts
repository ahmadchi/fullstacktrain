import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule, DatePickerModule, GeneralModule, GeneralPageComponent, GeneralFormComponent, SelectModule, GeneralTableComponent } from 'charms-lib';

import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {ChiSearchComponent} from '../shared/chi-search/chi-search.component';


import {GoodsRecievedNoteComponent} from './goods-recieved-note/goods-recieved-note.component';
import {PurchaseRequisitionComponent} from './purchase-requisition/purchase-requisition.component'
import {PurchaseOrdersComponent} from './purchase-orders/purchase-orders.component'

// import {MatCheckboxModule} from '@angular/material'
import {StoresComponent} from './store/stores.component'
import {UnitsComponent} from './units/units.component'
import {LabelsComponent} from './labels/labels.component'
import { InventoryComponent } from 'projects/charms-dev/src/app/pages/accounts/inventory/inventory.component'
import { ChipChiSelectorComponent } from './inventory/chip-chi-select/chip-chi-select.component';
import { ChiTableCustomComponent }  from '../shared/chi-table-custom/chi-table-custom.component';
import {LineItemModule} from '../shared/line-item/module';

const routes: Routes = [
    { path :'inventory', component: InventoryComponent },
    { path :'manufacturers', component: GeneralPageComponent, data: {name: 'manufacturers'} },
    { path :'units', component: UnitsComponent},
    { path :'formulas', component: GeneralPageComponent, data: {name: 'formulas'} },
    { path :'labels', component: LabelsComponent, data: {name: 'labels'} },
    { path :'suppliers', component: GeneralPageComponent, data: {name: 'suppliers'} },
    { path :'inventories', component: GeneralPageComponent, data: {name: 'inventories'} },
    { path :'inventory-formulas', component: GeneralPageComponent, data: {name: 'inventory-formulas'} },
    { path :'inventory-suppliers', component: GeneralPageComponent, data: {name: 'inventory-suppliers'} },
    { path :'inventory-labels', component: GeneralPageComponent, data: {name: 'inventory-labels'} },
    { path :'formula-interactions', component: GeneralPageComponent, data: {name: 'formula-interactions'} },
    { path :'purchase-requisitions', component: PurchaseRequisitionComponent, data: {name: 'purchase-requisitions'} },
    { path :'purchase-requisitions-items', component: GeneralPageComponent, data: {name: 'purchase-requisitions-items'} },
    { path :'purchase-orders', component: PurchaseOrdersComponent},
    { path :'purchase-order-items', component: GeneralPageComponent, data: {name: 'purchase-order-items'} },
    { path :'item-batches', component: GeneralPageComponent, data: {name: 'item-batches'} },
    { path :'stores', component: StoresComponent, data: {name: 'stores'} },
    { path :'goods-received-notes', component: GoodsRecievedNoteComponent},
    { path :'grn-items', component: GeneralPageComponent, data: {name: 'grn-items'} },
    { path :'purchase-return-items', component: GeneralPageComponent, data: {name: 'purchase-return-items'} },
    { path :'purchase-returns', component: GeneralPageComponent, data: {name: 'purchase-returns'} },
    { path :'inventory-stocks', component: GeneralPageComponent, data: {name: 'inventory-stocks'} },
    { path :'stock-transfers', component: GeneralPageComponent, data: {name: 'stock-transfers'} },
    { path :'stock--transfer-items', component: GeneralPageComponent, data: {name: 'stock--transfer-items'} },
    { path :'sales-invoices', component: GeneralPageComponent, data: {name: 'sales-invoices'} },
    { path :'invoice-items', component: GeneralPageComponent, data: {name: 'invoice-items'} },
];

@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
        GeneralModule,
        SelectModule,
        DatePickerModule,
        MatCheckboxModule,
        LineItemModule,

        RouterModule.forChild(routes),
        ReactiveFormsModule
    ],
    declarations: [
        InventoryComponent,
        ChipChiSelectorComponent,
        ChiSearchComponent,
        ChiTableCustomComponent,
        UnitsComponent,
        StoresComponent,
        LabelsComponent,
        PurchaseRequisitionComponent,
        PurchaseOrdersComponent,
        GoodsRecievedNoteComponent

    ],
    exports: [
    ],
    providers: [],
})
export class AccountsModule { }
