import { Component, Output, EventEmitter, Input, OnChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
// import { CommonService } from '@app/common.service';
import { ApiService, RVAlertsService } from 'charms-lib';
import { MatDialogRef } from '@angular/material/dialog';


@Component({
    selector   : 'chi-allergy-form',
    templateUrl: './form.html',
    styleUrls  : ['./form.scss'],
    providers: [ApiService]
})
export class AllergyFormComponent implements OnChanges
{
    @Input() oid;
    @Input() actions: Subject<any>;
    @Output() signals = new EventEmitter();
    
    theForm: FormGroup;
    title: any;

    inDialog: boolean;
    psOptions = {};
    disableSaveBtn = false;

    constructor(
            // private _commonService: CommonService, 
            protected formBuilder: FormBuilder, 
            protected apiService: ApiService, 
            protected dialogRef: MatDialogRef<AllergyFormComponent>)
    {
        this.apiService.apiSlug = "acc_manufacturers";
        this.apiService.primaryKey = "manufacturer_id";

        this.title = 'Add Manufacturers';
        this.theForm = this.formBuilder.group({});
        this.oid = null;
        this.inDialog = false;

        this.actions = null;
    }

    ngOnInit(): void
    {
        this.theForm = this.formBuilder.group({
            manufacturer_id: [null],
            acc_manufacturers: [null, Validators.required],
        });
    }

    ngOnChanges(): void
    {
        if (this.oid != null)
        {
            this.title = 'Edit Manufacturers';
            this.getSingle();
        }
    }

    getSingle() {
        const postData = { oid: this.oid };

        this.apiService.getSingle(postData).then(response =>
        {
            this.theForm.patchValue(response.data);

        }, (error: any) =>
        {
            RVAlertsService.apiError('Error Loading Form Data', error.toString());
        });
    }

    get manufacturer_name()
    {
        return this.theForm.controls.manufacturer_name;
    }

    onSave(): void 
    {
        this.disableSaveBtn = true;
        let formData = this.theForm.value;

        if(this.oid != null)
        {
            this.apiService.doUpdate(formData).then((resp) => {
                this.onCancel();
                this.disableSaveBtn = false;
            }, (error: any) => {
                this.disableSaveBtn = false;
                RVAlertsService.error('Error Loading Form Data', error.toString());
            });
        }
        else
        {
            this.apiService.doCreate(formData).then((resp) => {
                    this.onCancel();
                    this.disableSaveBtn = false;
            }, (error: any) => {
                this.disableSaveBtn = false;
                RVAlertsService.error('Error Loading Form Data', error.toString());
            });
        }
    }

    onSaveAndNew(): void 
    {
        this.disableSaveBtn = true;
        let formData = this.theForm.value;

        if(this.oid != null)
        {
            this.apiService.doUpdate(formData).then((resp) => {
                this.onReload();
                this.disableSaveBtn = false;
            }, (error: any) => {
                this.disableSaveBtn = false;
                RVAlertsService.error('Error Loading Form Data', error.toString());
            });
        }
        else
        {
            this.apiService.doCreate(formData).then((resp) => {
                this.onReload();
                this.disableSaveBtn = false;
            }, (error: any) => {
                this.disableSaveBtn = false;
                RVAlertsService.error('Error Loading Form Data', error.toString());
            });
        }
    }

    onCancel(): void
    {
        if (this.inDialog) {
            this.dialogRef.close(true);
        } else {
            const d = { type: 'CloseForm' };
            this.signals.emit(d);
        }
    }

    onReload(): void
    {
        this.actions.next({ action: 'reload' });
        this.theForm.reset();
    }

}
