import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from 'charms-lib';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AutoCompleteComponent } from './auto-complete.component';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,

        MaterialModule,
        HttpClientModule,
    ],
    declarations: [
       AutoCompleteComponent
    ],
    exports: [
        AutoCompleteComponent
    ],
    providers: [],
})
export class AutoCompleteFieldModule { }
