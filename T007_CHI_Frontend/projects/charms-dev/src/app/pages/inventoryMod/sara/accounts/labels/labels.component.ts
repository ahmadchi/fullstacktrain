import {
  ApiService,
} from 'charms-lib';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {
  Component,
  OnInit,
  ViewEncapsulation,
} from '@angular/core';
import {
  GenericApiResponse,
  RVAlertsService,
} from 'charms-lib';
import { Subject } from 'rxjs';

@Component({
  selector: 'labels',
  templateUrl: 'labels.component.html',
  styleUrls: ['labels.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class LabelsComponent implements OnInit {

  tableConfig = {
    name:'labels',
    topTitle:'Update Existing Labels',
    subTitle:'Select Label',
    columns: [{name:'Description', key:'description'}, {name:'Default Option', key:'is_default'}]
  }
  labels: FormGroup;
  actions =  new Subject<any>();
  constructor(private apiService : ApiService){}
  ngOnInit(): void {
    this.labels = new FormGroup({
      description: new FormControl('', [Validators.required]),
      is_default: new FormControl('', [Validators.required]),
    });
  }

  Save() {
    const payload = this.labels.value
    this.apiService.post('/labels/Create', payload).then(
      (resp: GenericApiResponse) => {
        this.actions.next({type:'reload'});
        console.log(resp)
      })
  }
}
