import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ChiConfigService, RVAlertsService, GenericApiResponse, ApiService, FormField } from 'charms-lib';


@Component({
    selector: 'main-page',
    templateUrl: 'main.component.html',
    styleUrls: ['main.component.scss']
})
export class MainPageComponent
{
    theForm: FormGroup;
    constructor(
        private apiService: ApiService,
        private configService: ChiConfigService,
        private _fb: FormBuilder,)
    {
        
    }

    ngOnInit(): void
    {
        this.theForm = this._fb.group({
            'description':  new FormField({
                name : 'description',
                type: 'text',
                required : true,
                validators : [Validators.required]
    
            }).formControl
        });
    }

    onSave(){
        console.log(this.theForm.value)
    }
}