import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { MatDialog} from '@angular/material/dialog';
import {  MatDialogRef } from '@angular/material/dialog';

import {  FormField, ApiService} from 'charms-lib';
import { AllergyFormComponent } from './add-allergy-form/form';
// import { SettingsMeasurementsFormComponent } from './form/form';

@Component({
    selector: 'multi-select-custom',
    templateUrl: 'multi-select-custom.component.html',
    styleUrls: ['multi-select-custom.component.scss']
})
export class MultiSelectCustomComponent implements OnInit
{ 
 

    theForm : FormGroup;
    manufacturer: FormField;
    manufacturerIds : any[]
    manufacturer_ids : any[]
    idsArr: any[];
    measurementFormDialogRef: MatDialogRef<any>;

    constructor(private apiService : ApiService,
      protected dialogRef: MatDialogRef<MultiSelectCustomComponent>,
      protected dialog: MatDialog,){

      this.manufacturerIds = [];
      this.manufacturer_ids = [];
      this.idsArr = [];
    }

    ngOnInit(): void
    {  
        this.theForm = new FormGroup({})
        this.manufacturer = new FormField({
            name: 'manufacturer_id',
            title: 'Manufacturer/Company',
            type: 'foreign',
            required: true,
            foreign: {
              foreign_table: 'acc_manufacturers',
              foreign_column: 'manufacturer_id',
              columns: ['manufacturer_name'],
            },
          });

          this.theForm.addControl(
            'manufacturer_id',
            this.manufacturer.formControl
          );
    }

  onSelectionChange(ids: any) {
      this.manufacturer_ids = this.manufacturerIds;
  }

  onAddAllergy(ev: any) 
    {
        const dialogRef = this.dialog.open(AllergyFormComponent,
            {
                width: '30vw',
                maxWidth: '30vw',
            });

        dialogRef.componentInstance.oid = null;
        dialogRef.componentInstance.inDialog = true;

        dialogRef.afterClosed().subscribe( (resp) => {
            if(resp) {
                this.loadMeasurementsData(ev);
            }
        });
    }

    
    loadMeasurementsData(ev: any) {
      this.idsArr = [];
      this.idsArr = JSON.parse(JSON.stringify(this.manufacturerIds));
      const payload = {
          columns: ['manufacturer_name', 'manufacturer_id'],
      };

      if (ev.search) 
      {
          payload['search'] = ev.search;
      }

      this.apiService.post('acc_manufacturers/List', payload).then(response => {
          console.log(response)
          
      });
    }


    onIdsChange(ids: any) {
      this.manufacturerIds = ids;
  }

    onMultiSelectionSignals(e)
    {
        if (e.type === 'onFetchData')
        {
            this.loadMeasurementsData(e.data);
        }
        else if (e.type === 'onSelectionChange')
        {
            this.onSelectionChange(e.data);            
        }
        else if (e.type === 'onIdsChange')
        {
            this.onIdsChange(e.data);
        }
        else if (e.type === 'onAddClick')
        {
            this.onAddAllergy(e.data);
        }
        else if (e.type === 'onOkClick')
        {
        }
        else
        {
            console.log("not defined", '-')
        }
    }


}