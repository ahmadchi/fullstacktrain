import { Component, OnInit } from '@angular/core';
import { TableConfig } from 'charms-lib';
import { AppointmentControllerConfig } from './account_type-config';
import { MatDialog } from '@angular/material/dialog';


@Component({
    selector: 'appointment-page',
    templateUrl: 'account_type.component.html',
    styleUrls: ['account_type.component.scss'],
})
export class AccountTypePageComponent implements OnInit
{
    config: TableConfig;

    constructor(private dialog: MatDialog)
    {
        this.config = new TableConfig(AppointmentControllerConfig.config);
    }

    ngOnInit(): void
    {

    }

    onTableSignal(ev: any)
    {
        // console.log('Table Signal = ', ev);
    }
}