import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChiSearchComponent } from './chi-search.component';

describe('ChiSearchComponent', () => {
  let component: ChiSearchComponent;
  let fixture: ComponentFixture<ChiSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChiSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChiSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
