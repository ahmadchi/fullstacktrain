import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule, GeneralModule, SelectModule, DateTimePickerModule, DatePickerModule } from 'charms-lib';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MainPageComponent } from './main.component';
import { AppRoutesGuard } from '../../route-guards/app.routes.guard';

const routes: Routes = [
    { path: '', component: MainPageComponent, canActivate: [AppRoutesGuard]},
];

@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
        GeneralModule,
        FormsModule,
        ReactiveFormsModule,

        RouterModule.forChild(routes),
        DatePickerModule,
        DateTimePickerModule,
        SelectModule
    ],
    declarations: [
        MainPageComponent
    ],
    exports: [
        MainPageComponent
    ],
    providers: [],
})
export class MainModule { }
