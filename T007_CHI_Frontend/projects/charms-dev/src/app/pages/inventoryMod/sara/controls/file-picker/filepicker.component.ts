import { Component } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormControlName } from '@angular/forms';import {  ApiService, FormField, GenericApiResponse } from 'charms-lib';

@Component({
    selector: 'file-picker.component',
    templateUrl: 'filepicker.component.html',
    styleUrls: ['filepicker.component.scss']
})
export class FilePickerCustomComponent
{
  
     // ============ Form ===============
    UploadDocumentForm: FormGroup;
    document: FormField;
    formControl : FormControl
    //==================================
    files: File[] = [];
    endpoint: string;
    file_url : any
    fileTag: string;
    maxFiles: number;
    selectedFile: any;
    fileType: string;
    user_files: any[];
    payload = {
        columns: [
          'episode_ids',
          'notes',
          'attachments',
          'category_name',
          'report_date',
          'status',
          'date_added',
        ],
        search: null,
        params: {
          patient_id: 1,
          api_from: 'general_documents',
        },
        where: {
          column: 'episode_ids',
          search: [2],
          op: 'eq',
        },
        limit: 10,
        offset: 0,
        patient_id: 1,
        api_from: 'general_documents',
      };
  


    constructor(private apiService: ApiService) {
        this.endpoint = null;
        this.apiService.apiSlug = 'user_files';
        this.apiService.primaryKey = 'file_id';
      }

    ngOnInit(): void
    {  
       
        this.UploadDocumentForm = new FormGroup({
            notes: new FormControl(''),
        });
        this.document = new FormField({
            name: 'document_url',
            title: 'Upload a document or a page',
            type: 'file',
            required: true,
        });
        this.UploadDocumentForm.addControl(
            'file_url',
            this.document.formControl
        )  
        this.apiService
            .post('episode_tasks/GetAvailableGeneralDocuments',this.payload)
            .then((resp: GenericApiResponse) => {
              console.log('LIST RESPONSE', resp.data.data);
            });
        
    }
    getFileURL(){
      return this.file_url
  
    }
    onSelect(event) {
    
      this.files.push(...event.addedFiles);
      const formData = new FormData();
      for (var i = 0; i < this.files.length; i++) {
          formData.append('UserFile', this.files[i]);
      }
      this.file_url = this.apiService.post('user_files/Upload', formData).then((res) => {
          this.file_url = res["data"]["file_url"]
          this.getFileURL()
      })
    }

    OnFileUpload(ev: any) {
    }

    OnFileSelected(ev: any, field: FormField) {
        field.formControl.setValue(ev);
        console.log(ev)
      }

    onRemove(event) {
        this.files.splice(this.files.indexOf(event), 1);
      }
    onUploadSuccess(ev: any) {
      }

}