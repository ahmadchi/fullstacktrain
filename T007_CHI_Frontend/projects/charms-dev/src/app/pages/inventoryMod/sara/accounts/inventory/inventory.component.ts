import { ApiService } from 'charms-lib';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, QueryList, ViewChildren, ViewEncapsulation } from '@angular/core';
import { FormField, GenericApiResponse, RVAlertsService } from 'charms-lib';
import { LineItemComponent } from '../../shared/line-item/line-item.component';

@Component({
  selector: 'inventory',
  templateUrl: 'inventory.component.html',
  styleUrls: ['inventory.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class InventoryComponent implements OnInit {

  manufacturer: FormField;
  purchasingUnit: FormField;
  sellingUnit: FormField;
  storageUnit: FormField;

  formulaField: FormField;
  formulasConfig = {
    slug: 'formulas',
    indexColName: 'formula_id',
    colName: 'formula_name',
    title: 'Formula Name',
  };
  formulas = [];

  basicForm = true;
  inventoryForm: FormGroup;
  constructor(private apiService: ApiService) {
  }

  ngOnInit(): void {
    this.inventoryForm = new FormGroup({
      description: new FormControl('', [Validators.required]),
      barcode: new FormControl(''),
      formula: new FormControl(''),
      expiry_date_enabled: new FormControl('', [Validators.required]),
      maximum_level: new FormControl('', [Validators.required]),
      minimum_level: new FormControl('', [Validators.required]),
      LASA: new FormControl(false, [Validators.required]),
      narcotics: new FormControl(false, [Validators.required]),
      fridge_item: new FormControl(false, [Validators.required]),
      purchase_to_storage: new FormControl('', [Validators.required]),
      storage_to_selling: new FormControl('', [Validators.required]),
      item_consumption: new FormControl(''),
      remarks: new FormControl(''),
    });

    this.formulaField = new FormField({
      name: this.formulasConfig['indexColName'],
      title: this.formulasConfig['title'],
      type: 'foreign',
      required: false,
      foreign: {
        foreign_table: this.formulasConfig['slug'],
        foreign_column: this.formulasConfig['indexColName'],
        columns: [this.formulasConfig['colName']],
      },
    });

    this.inventoryForm.addControl(
      'formulas',
      this.formulaField.formControl
    );

    this.manufacturer = new FormField({
      name: 'manufacturer_id',
      title: 'Manufacturer/Company',
      type: 'foreign',
      required: true,
      foreign: {
        foreign_table: 'acc_manufacturers',
        foreign_column: 'manufacturer_id',
        columns: ['manufacturer_name'],
      },
    });
    this.inventoryForm.addControl(
      'manufacturer_id',
      this.manufacturer.formControl
    );

    this.purchasingUnit = new FormField({
      name: 'purchasing_unit',
      title: 'Purchasing Unit',
      type: 'foreign',
      required: true,
      foreign: {
        foreign_table: 'units',
        foreign_column: 'unit_id',
        columns: ['unit_name'],
      },
    });
    this.inventoryForm.addControl(
      'purchasing_unit_id',
      this.purchasingUnit.formControl
    );
    this.sellingUnit = new FormField({
      name: 'selling_unit',
      title: 'Selling Unit',
      type: 'foreign',
      required: true,
      foreign: {
        foreign_table: 'units',
        foreign_column: 'unit_id',
        columns: ['unit_name'],
      },
    });
    this.inventoryForm.addControl(
      'selling_unit_id',
      this.sellingUnit.formControl
    );
    this.storageUnit = new FormField({
      name: 'storage_unit',
      title: 'Storage Unit',
      type: 'foreign',
      required: true,
      foreign: {
        foreign_table: 'units',
        foreign_column: 'unit_id',
        columns: ['unit_name'],
      },
    });
    this.inventoryForm.addControl(
      'storage_unit_id',
      this.storageUnit.formControl
    );
  }
  onForeignSelected(ev: any) {}

  updateChipData(resp: any) {
    this.formulas = resp;
    console.log('resp', resp);
  }

  onClear():void {
    this.inventoryForm.reset();
  }

  onSave() {
    const payload = this.inventoryForm.value;
    payload.formulas = this.formulas;
    console.log('Inventory Payload:', payload);
    payload.inventory_name = 'Ali-Test';
    payload.inventory_type = 'Ali-type';
    payload.labels = [];

    this.apiService.post('inventories/createTester', payload).then(
      (resp: GenericApiResponse) => {
        console.log('Inventory Response', resp);
      },
      (error: GenericApiResponse) => {
        RVAlertsService.error(
          'Error getting active manufacturers.',
          error.ErrorMessage
        );
      }
    );
  }

  numbersOnly(ev: any): boolean {
    const key = ev.keyCode;
    return (key >= 48 && key <= 57) || key === 8 || key === 9;
  }
}
