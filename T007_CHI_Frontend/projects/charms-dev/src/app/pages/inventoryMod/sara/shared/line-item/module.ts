import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule, SelectModule } from 'charms-lib';
import { LineItemComponent } from './line-item.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AutoCompleteFieldModule } from '../auto-complete/module';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,

        MaterialModule,
        SelectModule,
        AutoCompleteFieldModule,
    ],
    declarations: [
       LineItemComponent
    ],
    exports: [
        LineItemComponent
    ],
    providers: [],
})
export class LineItemModule { }
