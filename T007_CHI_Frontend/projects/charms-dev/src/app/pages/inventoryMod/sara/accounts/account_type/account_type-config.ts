// import { ControllerConfig, TableConfig, FormConfig } from 'charms-lib';

export class AppointmentControllerConfig
{
    public static config = {
        slug: 'account_types',
        key: 'account_type_id',
        title_s: 'Accout Type',
        title_p: 'Account Types',

        showHeader: true,
        showSearch: true,
        showAdd: true,
        showImport: false,
        showRowActions: true,

        rowActions: [
            { icon: 'visibility', toolTip: 'View Details', action: 'OnViewDetails', class: 'priamry-fg', permission_action: 'view' },
        ],

        table: {
            columns: [
                { name: 'account_type_id', visible: false },
                { name: 'account_type_name', title: 'Type Name', showInSearch: true },
                { name: 'description', title: 'Description' },
                { name: 'date_added', title: 'Date', format:'datetime',  },

            ],

            order: {column: 'date_added', dir: 'desc'},
        },

        form: {
            columns:
            [
                { name: 'account_type_id', title: 'Id', type: 'hidden' },
                { name: 'account_type_name', title: 'Type Name', type: 'text', required: true },
                { name: 'description', title: 'Description', type: 'text' }
            ]
        },

    };
}
