import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'chi-search',
  templateUrl: './chi-search.component.html',
  styleUrls: ['./chi-search.component.scss'],
  animations: [
    trigger('searchContainerState', [
    state('collapsed', style({ width: '40px' })),
    state('expanded', style({ width: '400px' })),
    transition('expanded <=> collapsed', animate('250ms ease-in-out'))
    ]),
    ]
})
export class ChiSearchComponent implements OnInit {

  searchControl: FormControl;
  interactedWithSearch = false;

  constructor() { }

  ngOnInit(): void {
      this.searchControl = new FormControl(null,[])

      this.searchControl.valueChanges.pipe(debounceTime(400), distinctUntilChanged()).subscribe(query =>
      {
        this.onSearchData();
      });
  }

  toggleSearch(): void {
    this.interactedWithSearch = !this.interactedWithSearch;
  }

  onSearchData():void{
    console.log('My Search Value is', this.searchControl.value);
  }

}
