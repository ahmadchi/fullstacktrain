import {
  ApiService,
} from 'charms-lib';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {
  Component,
  OnInit,
  ViewEncapsulation,
} from '@angular/core';
import {
  FormField,
  GenericApiResponse,
  RVAlertsService,
} from 'charms-lib';
import { Subject } from 'rxjs';

@Component({
  selector: 'stores',
  templateUrl: 'stores.component.html',
  styleUrls: ['stores.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class StoresComponent implements OnInit {

  tableConfig = {
    name:'stores',
    topTitle:'Update Existing Stores',
    subTitle:'Select Store',
    columns: [{name:'Description', key:'description'}, {name:'Default Option', key:'is_default'}]
  }
  actions =  new Subject<any>();
  stores: FormGroup;
  payload : {}

  constructor(private apiService : ApiService){}
  ngOnInit(): void {

    this.stores = new FormGroup({
      description: new FormControl('', [Validators.required]),
      is_default: new FormControl('', [Validators.required]),
    });
  }


  Save() {
    const payload= this.stores.value
    this.apiService.post('/stores/Create', payload).then(
      (resp: GenericApiResponse) => {
        this.actions.next({type:'reload'})
      })
  }
}
