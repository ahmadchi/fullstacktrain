import {
  Component,
  ChangeDetectorRef,
  Input,
  OnInit,
} from '@angular/core';
import {
  ChiConfigService,
  RVAlertsService,
  GenericApiResponse,
  ApiService,
} from 'charms-lib';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { Subject } from 'rxjs';

@Component({
  selector: 'chi-table-custom',
  templateUrl: './chi-table-custom.component.html',
  styleUrls: ['./chi-table-custom.component.scss'],
})
export class ChiTableCustomComponent implements OnInit {
  @Input() config: any;
  data: any[];
  selectedRow: any;
  @Input() signals: Subject<any>;
  searchVal = '';
  isLoading = false;

  constructor(
    private apiService: ApiService,
    private configService: ChiConfigService,
    protected dialogRef: MatDialogRef<any>,
    protected dialog: MatDialog,
    private _cdr: ChangeDetectorRef
  ) {
    this.selectedRow = null;
  }

  ngOnInit(): void {
    this.loadData();
    this.signals.subscribe(signal=>{
        this.handleSignal(signal);
    })
  }

  handleSignal(signal): void{
      switch(signal.type){
          case 'reload':
              this.loadData();
              break;
      }
  }

  onColumnSearch(col: string, ev: any): void {
    this.isLoading = true;
    const slug = `${this.config.name}/List`;
    const payload ={columns: this.config?.columns.map(c => c.key), where:{column:col, search:ev.target.value, op: 'like'},}
    this.apiService
      .post(slug, payload)
      .then(
        (resp) => {
          this.data = resp.data;
          this.isLoading = false;
        },
        (error: GenericApiResponse) => {
          RVAlertsService.error(
            'Error Getting Response',
            error.ErrorMessage
          );
        }
      );
  }

  loadData() {
    this.isLoading = true;
    const slug = `${this.config.name}/List`
    this.apiService
      .post(slug, {columns: this.config?.columns.map(c => c.key)})
      .then(
        (resp) => {
          this.data = resp.data;
          this.isLoading = false;
        },
        (error: GenericApiResponse) => {
          RVAlertsService.error(
            'Error Getting Response',
            error.ErrorMessage
          );
        }
      );
  }

  onRowClick(row: any) {
    if (this.selectedRow === row) {
      this.selectedRow = null;
    } else {
      this.selectedRow = row;
    }
  }
}
