export interface LineItemModel
{
    name: string;
    items: LineItem[];
}

export interface LineItem
{
    id: any;
    selected?: boolean;
    editing: boolean;
    deleted: boolean;
    fields: ItemField[];
}

export interface ItemField
{
    name: string;
    title: string;
    subText: string;
    value: any;
    min:number;
    max:number;
    validate:string;
    displayValue: any;
    readOnlyInSelection: boolean;
    allowedCodes?: number[];
    field: string;
    type: string;
    options?: any;
    required: boolean;
    disabled:boolean;
}
