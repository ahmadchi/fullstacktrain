import { Component } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';import {  FormField } from 'charms-lib';

@Component({
    selector: 'foreignpicker.component',
    templateUrl: 'foreignpicker.component.html',
    styleUrls: ['foreignpicker.component.scss']
})
export class ForeignPickerCustomComponent
{

    inventoryForm : FormGroup;
    manufacturer: FormField;
    constructor(){}

    ngOnInit(): void
    {  
        this.inventoryForm = new FormGroup({})
        this.manufacturer = new FormField({
            name: 'manufacturer_id',
            title: 'Manufacturer/Company',
            type: 'foreign',
            required: true,
            foreign: {
              foreign_table: 'acc_manufacturers',
              foreign_column: 'manufacturer_id',
              columns: ['manufacturer_name'],
            },
          });

          this.inventoryForm.addControl(
            'manufacturer_id',
            this.manufacturer.formControl
          );
    }
    onForeignSelected(ev: any) {}

}