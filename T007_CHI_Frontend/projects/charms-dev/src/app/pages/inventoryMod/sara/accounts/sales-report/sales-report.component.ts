import {
  ApiService,
} from 'charms-lib';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {
  Component,
  OnInit,
  QueryList,
  ViewChildren,
  ViewEncapsulation,
} from '@angular/core';
import { FormField, GenericApiResponse, RVAlertsService } from 'charms-lib';
import { LineItemComponent } from '../../shared/line-item/line-item.component';

@Component({
  selector: 'purchase-requisition',
  templateUrl: 'purchase-requisition.component.html',
  styleUrls: ['purchase-requisition.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PurchaseRequisitionComponent implements OnInit {

  @ViewChildren(LineItemComponent) lineItems: QueryList<LineItemComponent>;

  purchaseRequistionItems: any;
  lineItemData: any;

  purchaseRequisition: FormField;
  date : FormField;
  purchaseRequisitionForm : FormGroup;


  constructor(private apiService : ApiService){
  }
  ngOnInit(): void {
    this.purchaseRequisitionForm =  new FormGroup({
      document_category: new FormControl('', []),
      remarks: new FormControl('', [])
    })

    this.date = new FormField({
      name: 'date',
      title: 'Date',
      type:'date',
      required: true
    })
    this.purchaseRequisition = new FormField({
      name: 'purchase_requisition',
      title: 'Purchase Requisition',
      type: 'foreign',
      required: false,
      foreign: {
        foreign_table: 'purchase_requisition',
        foreign_column: 'purchase_requisition_id',
        columns: ['purchase_requisition_name'],
      },
    });
    this.purchaseRequisitionForm.addControl(
      'purchase_requisition_id',
      this.purchaseRequisition.formControl
    );
    this.purchaseRequisitionForm.addControl('date', this.date.formControl);

    this.purchaseRequistionItems  = {
      name: 'line_items',
      items: [{
          editing: true,
          fields: [
            {
              title: 'Inventory',
              value: null,
              name: 'inventory_id',
              displayValue: null,
              field: 'foreign',
              options: { name: 'code', title: 'Inventory', required: true, type: 'foreign',
                  foreign: {
                      mode: 'double',
                      foreign_table: 'inventories', foreign_column: 'code',
                      columns: ['inventory_name', 'barcode']
                    }
                  },
              required: true
              },
              {
                  title: 'Quantity',
                  value: 0,
                  min: 0,
                  field: 'input',
                  name: 'quantity',
                  type: 'number',
                  required: false
              },
              {
                  title: 'Est Rate',
                  value: 0,
                  min:0,
                  field: 'input',
                  name: 'est_rate',
                  type: 'number',
                  required: false
              },
              {
                  title: 'Arrival Date',
                  value: null,
                  field: 'input',
                  name: 'est_arrival_date',
                  type: 'date',
                  required: false
              },
              {
                  title: 'Approved Quantity',
                  value: 0,
                  min:0,
                  field: 'input',
                  name: 'approved_quantity',
                  type: 'number',
                  required: false
              }
          ]
      }]
    };

    this.lineItemData = [];
  }

  onForeignSelected(ev: any) {
    console.log('ev');
  }


  onClear(){
  }
  onSave(){
    const lineData: any = {};
    const payload = this.purchaseRequisitionForm.value;
    for (const item of this.lineItems)
    {
        const data = item.getData();
        if (data == null)
        {
            return;
        }

        lineData[data.name] = data.items;
    }
    payload.items = lineData.line_items;
    console.log('Payload is:', payload);
  }
}
