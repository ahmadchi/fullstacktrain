import {
  ApiService,
} from 'charms-lib';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {
  Component,
  OnInit,
  ViewEncapsulation,
} from '@angular/core';
import { FormField, GenericApiResponse, RVAlertsService } from 'charms-lib';

@Component({
  selector: 'goods-recieved-note',
  templateUrl: 'goods-recieved-note.component.html',
  styleUrls: ['goods-recieved-note.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class GoodsRecievedNoteComponent implements OnInit {
  purchase_requisition: FormField;
  date : FormField;
  supplier_invoice_date : FormField;
  supplier_quotation_date : FormField;
  PurchaseOrderForm : FormGroup;
  indeterminate = false;
  

  constructor(private apiService : ApiService){

    this.date = new FormField()
    this.supplier_invoice_date = new FormField()
  }
  ngOnInit(): void {

    
    this.PurchaseOrderForm = new FormGroup({})
    this.PurchaseOrderForm.addControl('date', this.date.formControl);
    this.PurchaseOrderForm.addControl('supplier_invoice_date', this.date.formControl);
    this.PurchaseOrderForm.addControl('supplier_invoice_number', this.date.formControl);
    this.PurchaseOrderForm.addControl('payment_mode', this.date.formControl);
    this.PurchaseOrderForm.addControl('supplier', this.date.formControl);
    this.PurchaseOrderForm.addControl('store', this.date.formControl);
    this.PurchaseOrderForm.addControl('get_pass_number', this.date.formControl);
    this.PurchaseOrderForm.addControl('document_mode', this.date.formControl);
    this.PurchaseOrderForm.addControl('document_category', this.date.formControl);
    // document_mode

  }

  onForeignSelected(ev: any) {
    console.log('ev');
  }

}
