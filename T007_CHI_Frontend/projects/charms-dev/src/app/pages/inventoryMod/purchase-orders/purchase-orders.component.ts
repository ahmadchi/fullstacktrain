import { Component, OnInit } from '@angular/core';
import { ApiService } from 'charms-lib';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FormField, GenericApiResponse, RVAlertsService } from 'charms-lib';
import { QueryList, ViewChildren } from '@angular/core';
import { LineItemComponent } from '../../shared/line-item/line-item.component';


@Component({
    selector: 'purchase-orders-form',
    templateUrl: 'purchase-orders.component.html',
    styleUrls: ['purchase-orders.component.scss']
})
export class PurchaseOrderFormComponent implements OnInit
{
    theForm: FormGroup;
    supplierId: FormField;
    supplierQuotationDate: FormField;
    purchaseRequisitionId: FormField;

    @ViewChildren(LineItemComponent) lineItems: QueryList<LineItemComponent>;
    lineItemConfig: any;
    lineItemData = [];
    
    constructor(private apiService: ApiService)
    {
    }
    
    ngOnInit(): void
    {
         this.theForm = new FormGroup({
            status: new FormControl(''),
            remarks: new FormControl(''),
            supplier_quotation_number: new FormControl(''),
            payment_mode: new FormControl(''),
            document_mode: new FormControl(''),
         });
        this.supplierId = new FormField({
                        name: 'supplier_id',
                        title: 'Supplier', 
                        type: 'foreign',
                        foreign: {
                            foreign_table: 'suppliers',
                            foreign_column: 'supplier_id',
                            columns: ['supplier_name','created_by.full_name',]
                        }
        });
        this.theForm.addControl('supplier_id', this.supplierId.formControl);

        this.supplierQuotationDate = new FormField({name: 'date', title: 'Date', type: 'date', required: false});
        this.theForm.addControl('supplier_quotation_date', this.supplierQuotationDate.formControl);

        this.purchaseRequisitionId = new FormField({
                        name: 'purchase_requisition_id',
                        title: 'Purchase_Requisition', 
                        type: 'foreign',
                        foreign: {
                            foreign_table: 'purchase_requisitions',
                            foreign_column: 'purchase_requisition_number',
                            columns: ['purchase_requisition_number','created_by.full_name',]
                        }
        });
        this.theForm.addControl('purchase_requisition_id', this.purchaseRequisitionId.formControl);


        this.lineItemConfig  = {
            name: 'line_items',
            items: [{
                editing: true,
                fields: [
                    {
                        title: 'Inventory',
                        value: null,
                        name: 'inventory_id',
                        displayValue: null,
                        field: 'foreign',
                        required: true,
                        options: { name: 'inventory_id', title: 'Inventory', required: true, type: 'foreign',
                            foreign: {
                                foreign_table: 'inventories', foreign_column: 'inventory_id',
                                columns: ['inventory_id']
                            }
                        },
                    },
                    {
                        title: 'Quantity',
                        value: null,
                        field: 'input',
                        name: 'quantity',
                        type: 'number',
                        required: false
                    },
                    {
                        title: 'Rate',
                        value: null,
                        field: 'input',
                        name: 'rate',
                        type: 'number',
                        required: false
                    },
                    {
                        title: 'Discount',
                        value: null,
                        field: 'input',
                        name: 'discount',
                        type: 'number',
                        required: false
                    },
                    {
                        title: 'Discount Type',
                        value: null,
                        field: 'input',
                        name: 'discount_type',
                        type: 'text',
                        required: false
                    },
                    {
                        title: 'Estimated Arrival Date',
                        value: null,
                        field: 'date',
                        name: 'est_arrival_date',
                        required: false
                    },
                    {
                        title: 'Sales Tax',
                        value: null,
                        field: 'input',
                        name: 'sales_tax',
                        type: 'number',
                        required: false
                    },
                    {
                        title: 'Sales tax Type',
                        value: null,
                        field: 'input',
                        name: 'sales_tax_type',
                        type: 'text',
                        required: false
                    },
                ]
           }]
       };

    }
    
    onClear():void {
      this.theForm.reset();
    }

    onSave(): void
    {
        const payload = this.theForm.value;
        for (const item of this.lineItems) {
            const data = item.getData();
            if (data == null) {
              return;
            }
            payload.items = data.items;
        }

        this.apiService.post('purchase_orders/createMany2Many', payload).then((resp: GenericApiResponse) => {
            RVAlertsService.success('Success', 'Purchase Order Created').subscribe(resp => {
                this.theForm.reset();
            })
        }, (error: GenericApiResponse) => {
            RVAlertsService.error('Error Creating Purchase Order', error.ErrorMessage);
        })
    }
    
}

