import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule, GeneralModule, GeneralPageComponent, GeneralFormComponent, SelectModule, GeneralTableComponent } from 'charms-lib';

import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {ChiSearchComponent} from '../shared/chi-search/chi-search.component';
import {DatePickerModule} from '../../../../../charms-lib/src/lib/components/controls/date-picker/module'

import {GoodsReceivedNoteFormComponent} from './goods-received-notes/goods-received-notes.component';
import {PurchaseRequisitionFormComponent} from './purchase-requisition/purchase-requisitions.component'
import {PurchaseOrderFormComponent} from './purchase-orders/purchase-orders.component'

import { InventoryFormComponent } from 'projects/charms-dev/src/app/pages/inventory/inventories/inventories.component'
import { ChiN2NSelectComponent } from '../shared/chi-n2n-select/chi-n2n-select.component';
import { ChiTableCustomComponent }  from '../shared/chi-table-custom/chi-table-custom.component';
import {LineItemModule} from '../shared/line-item/module';

const routes: Routes = [
    { path :'add-inventory', component: InventoryFormComponent },
    { path :'manufacturers', component: GeneralPageComponent, data: {name: 'manufacturers'} },
    { path :'units', component:  GeneralPageComponent, data: {name: 'units'}},
    { path :'formulas', component: GeneralPageComponent, data: {name: 'formulas'} },
    { path :'labels', component: GeneralPageComponent, data: {name: 'labels'} },
    { path :'suppliers', component: GeneralPageComponent, data: {name: 'suppliers'} },
    { path :'inventories', component: GeneralPageComponent, data: {name: 'inventories'} },
    { path :'inventory-formulas', component: GeneralPageComponent, data: {name: 'inventory-formulas'} },
    { path :'inventory-suppliers', component: GeneralPageComponent, data: {name: 'inventory-suppliers'} },
    { path :'inventory-labels', component: GeneralPageComponent, data: {name: 'inventory-labels'} },
    { path :'formula-interactions', component: GeneralPageComponent, data: {name: 'formula-interactions'} },
    { path :'purchase-requisitions', component: PurchaseRequisitionFormComponent, data: {name: 'purchase-requisitions'} },
    { path :'purchase-requisitions-items', component: GeneralPageComponent, data: {name: 'purchase-requisitions-items'} },
    { path :'purchase-orders', component: PurchaseOrderFormComponent},
    { path :'purchase-order-items', component: GeneralPageComponent, data: {name: 'purchase-order-items'} },
    { path :'item-batches', component: GeneralPageComponent, data: {name: 'item-batches'} },
    { path :'stores', component: GeneralPageComponent, data: {name: 'stores'} },
    { path :'goods-received-notes', component: GoodsReceivedNoteFormComponent},
    { path :'grn-items', component: GeneralPageComponent, data: {name: 'grn-items'} },
    { path :'purchase-return-items', component: GeneralPageComponent, data: {name: 'purchase-return-items'} },
    { path :'purchase-returns', component: GeneralPageComponent, data: {name: 'purchase-returns'} },
    { path :'inventory-stocks', component: GeneralPageComponent, data: {name: 'inventory-stocks'} },
    { path :'stock-transfers', component: GeneralPageComponent, data: {name: 'stock-transfers'} },
    { path :'stock--transfer-items', component: GeneralPageComponent, data: {name: 'stock--transfer-items'} },
    { path :'sales-invoices', component: GeneralPageComponent, data: {name: 'sales-invoices'} },
    { path :'invoice-items', component: GeneralPageComponent, data: {name: 'invoice-items'} },
];

@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
        GeneralModule,
        SelectModule,
        DatePickerModule,
        MatCheckboxModule,
        LineItemModule,

        RouterModule.forChild(routes),
        ReactiveFormsModule
    ],
    declarations: [
        InventoryFormComponent,
        ChiN2NSelectComponent,
        ChiSearchComponent,
        ChiTableCustomComponent,
        PurchaseRequisitionFormComponent,
        PurchaseOrderFormComponent,
        GoodsReceivedNoteFormComponent

    ],
    exports: [
    ],
    providers: [],
})
export class InventoryModule { }
