import { Component, OnInit } from '@angular/core';
import { ApiService } from 'charms-lib';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FormField, GenericApiResponse, RVAlertsService } from 'charms-lib';


@Component({
    selector: 'purchase-requisitions-form',
    templateUrl: 'purchase-requisitions.component.html',
    styleUrls: ['purchase-requisitions.component.scss']
})
export class PurchaseRequisitionFormComponent implements OnInit
{
    theForm: FormGroup;

    inventories: FormField;
    
    constructor(private apiService: ApiService)
    {
    }
    
    ngOnInit(): void
    {
         this.theForm = new FormGroup({
            status: new FormControl(''),
            remarks: new FormControl(''),
         });
    this.inventories = new FormField({
      name: 'inventory_id',
      title: 'inventories',
      type: 'foreign',
      required: false,
      foreign: {
        foreign_table: 'inventories',
        foreign_column: 'inventory_id',
        columns: ['code', ],
      },
    });

    this.theForm.addControl('inventories', this.inventories.formControl);

    }
    
    onClear():void {
      this.theForm.reset();
    }

    onSave(): void 
    {
        const payload = this.theForm.value;

        this.apiService.post('purchase_requisitions/createMany2Many', payload).then((resp: GenericApiResponse) => {
            RVAlertsService.success('Success', 'Purchase Requisition Created').subscribe(resp => {
                this.theForm.reset();              
            })
        }, (error: GenericApiResponse) => {
            RVAlertsService.error('Error Creating Purchase Requisition', error.ErrorMessage);
        })
    }
    
}
