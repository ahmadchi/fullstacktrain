import { Component, OnInit } from '@angular/core';
import { ApiService } from 'charms-lib';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FormField, GenericApiResponse, RVAlertsService } from 'charms-lib';
import { QueryList, ViewChildren } from '@angular/core';
import { LineItemComponent } from '../../shared/line-item/line-item.component';


@Component({
    selector: 'goods-received-notes-form',
    templateUrl: 'goods-received-notes.component.html',
    styleUrls: ['goods-received-notes.component.scss']
})
export class GoodsReceivedNoteFormComponent implements OnInit
{
    theForm: FormGroup;
    supplierId: FormField;
    supplierChalanDate: FormField;
    purchaseOrderId: FormField;
    storeId: FormField;

    @ViewChildren(LineItemComponent) lineItems: QueryList<LineItemComponent>;
    lineItemConfig: any;
    lineItemData = [];

    constructor(private apiService: ApiService)
    {
    }

    ngOnInit(): void
    {
         this.theForm = new FormGroup({
            status: new FormControl(''),
            remarks: new FormControl(''),
            supplier_chalan_number: new FormControl(''),
            payment_mode: new FormControl(''),
            transaction_type: new FormControl(''),
         });
        this.supplierId = new FormField({
                        name: 'supplier_id',
                        title: 'Supplier', 
                        type: 'foreign',
                        foreign: {
                            foreign_table: 'suppliers',
                            foreign_column: 'supplier_id',
                            columns: ['supplier_name','created_by.full_name',]
                        }
        });
        this.theForm.addControl('supplier_id', this.supplierId.formControl);

        this.supplierChalanDate = new FormField({name: 'date', title: 'Date', type: 'date', required: false});
        this.theForm.addControl('supplier_chalan_date', this.supplierChalanDate.formControl);

        this.purchaseOrderId = new FormField({
                        name: 'purchase_order_id',
                        title: 'Purchase Order', 
                        type: 'foreign',
                        foreign: {
                            foreign_table: 'purchase_orders',
                            foreign_column: 'purchase_order_number_id',
                            columns: ['purchase_order_number_id','created_by.full_name',]
                        }
        });
        this.theForm.addControl('purchase_order_id', this.purchaseOrderId.formControl);

        this.storeId = new FormField({
                        name: 'store_id',
                        title: 'Store Id', 
                        type: 'foreign',
                        foreign: {
                            foreign_table: 'stores',
                            foreign_column: 'store_id',
                            columns: ['store_name','created_by.full_name',]
                        }
        });
        this.theForm.addControl('store_id', this.storeId.formControl);


        this.lineItemConfig  = {
            name: 'line_items',
            items: [{
                editing: true,
                fields: [
                    {
                        title: 'Item Batch',
                        value: null,
                        name: 'item_batches_id',
                        displayValue: null,
                        field: 'foreign',
                        required: true,
                        options: { name: 'item_batches_id', title: 'Item Batch', required: true, type: 'foreign',
                            foreign: {
                                foreign_table: 'item_batches', foreign_column: 'item_batches_id',
                                columns: ['item_batches_id']
                            }
                        },
                    },
                    {
                        title: 'Quantity',
                        value: null,
                        field: 'input',
                        name: 'quantity',
                        type: 'number',
                        required: false
                    },
                    {
                        title: 'Bonus',
                        value: null,
                        field: 'input',
                        name: 'bonus',
                        type: 'number',
                        required: false
                    },
                    {
                        title: 'Purchase Price',
                        value: null,
                        field: 'input',
                        name: 'purchase_price',
                        type: 'number',
                        required: false
                    },
                    {
                        title: 'Discount',
                        value: null,
                        field: 'input',
                        name: 'discount',
                        type: 'number',
                        required: false
                    },
                    {
                        title: 'Discount Type',
                        value: null,
                        field: 'input',
                        name: 'discount_type',
                        type: 'text',
                        required: false
                    },
                    {
                        title: 'Sales Tax',
                        value: null,
                        field: 'input',
                        name: 'sales_tax',
                        type: 'number',
                        required: false
                    },
                    {
                        title: 'Sales tax Type',
                        value: null,
                        field: 'input',
                        name: 'sales_tax_type',
                        type: 'text',
                        required: false
                    },
                    {
                        title: 'Loose Packing',
                        value: false,
                        field: 'checkbox',
                        name: 'loose_packing',
                        required: false,
                        label_hidden: true
                    },
                ]
           }]
       };

    }
    
    onClear():void {
      this.theForm.reset();
    }

    onSave(): void 
    {
        const payload = this.theForm.value;

        this.apiService.post('goods_received_notes/createMany2Many', payload).then((resp: GenericApiResponse) => {
            RVAlertsService.success('Success', 'Goods Received Note Created').subscribe(resp => {
                this.theForm.reset();              
            })
        }, (error: GenericApiResponse) => {
            RVAlertsService.error('Error Creating Goods Received Note', error.ErrorMessage);
        })
    }
    
}
