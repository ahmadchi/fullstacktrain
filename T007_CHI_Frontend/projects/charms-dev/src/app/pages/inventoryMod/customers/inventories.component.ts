import { Component, OnInit } from '@angular/core';
import { ApiService } from 'charms-lib';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FormField, GenericApiResponse, RVAlertsService } from 'charms-lib';


@Component({
    selector: 'inventories-form',
    templateUrl: 'inventories.component.html',
    styleUrls: ['inventories.component.scss']
})
export class InventoryFormComponent implements OnInit
{
    theForm: FormGroup;
    manufacturerId: FormField;
    purchaseUnitId: FormField;
    sellingUnitId: FormField;
    storageUnitId: FormField;

    formulas: FormField;
    labels: FormField;
    
    constructor(private apiService: ApiService)
    {
    }
    
    ngOnInit(): void
    {
         this.theForm = new FormGroup({
            barcode: new FormControl(''),
            inventory_name: new FormControl('', Validators.required),
            purchase_to_storage: new FormControl('', Validators.required),
            storage_to_selling: new FormControl('', Validators.required),
            is_expiry_date_enabled: new FormControl(false, Validators.required),
            is_narcotics: new FormControl(false, Validators.required),
            lasa: new FormControl(false, Validators.required),
            is_fridge_item: new FormControl(false, Validators.required),
            inventory_type: new FormControl(''),
            remarks: new FormControl(''),
         });
        this.manufacturerId = new FormField({
                        name: 'manufacturer_id',
                        title: 'Manufacturer', 
                        type: 'foreign',
                        foreign: {
                            foreign_table: 'manufacturers',
                            foreign_column: 'manufacturer_id',
                            columns: ['manufacturer_name',]
                        }
        });
        this.theForm.addControl('manufacturer_id', this.manufacturerId.formControl);

        this.purchaseUnitId = new FormField({
                        name: 'purchase_unit_id',
                        title: 'Purchase Unit', 
                        type: 'foreign',
                        required: true,
                        foreign: {
                            foreign_table: 'units',
                            foreign_column: 'unit_id',
                            columns: ['unit_name',]
                        }
        });
        this.theForm.addControl('purchase_unit_id', this.purchaseUnitId.formControl);

        this.sellingUnitId = new FormField({
                        name: 'selling_unit_id',
                        title: 'Selling Unit', 
                        type: 'foreign',
                        required: true,
                        foreign: {
                            foreign_table: 'units',
                            foreign_column: 'unit_id',
                            columns: ['unit_name',]
                        }
        });
        this.theForm.addControl('selling_unit_id', this.sellingUnitId.formControl);

        this.storageUnitId = new FormField({
                        name: 'storage_unit_id',
                        title: 'Storage Unit', 
                        type: 'foreign',
                        required: true,
                        foreign: {
                            foreign_table: 'units',
                            foreign_column: 'unit_id',
                            columns: ['unit_name',]
                        }
        });
        this.theForm.addControl('storage_unit_id', this.storageUnitId.formControl);

    this.formulas = new FormField({
      name: 'formula_id',
      title: 'formulas',
      type: 'foreign',
      required: false,
      foreign: {
        foreign_table: 'formulas',
        foreign_column: 'formula_id',
        columns: ['formula_name', ],
      },
    });

    this.theForm.addControl('formulas', this.formulas.formControl);

    this.labels = new FormField({
      name: 'label_id',
      title: 'labels',
      type: 'foreign',
      required: false,
      foreign: {
        foreign_table: 'labels',
        foreign_column: 'label_id',
        columns: ['label_name', ],
      },
    });

    this.theForm.addControl('labels', this.labels.formControl);

    }
    
    onClear():void {
      this.theForm.reset();
    }

    onSave(): void 
    {
        const payload = this.theForm.value;

        this.apiService.post('inventories/createMany2Many', payload).then((resp: GenericApiResponse) => {
            RVAlertsService.success('Success', 'Inventory Created').subscribe(resp => {
                this.theForm.reset();              
            })
        }, (error: GenericApiResponse) => {
            RVAlertsService.error('Error Creating Inventory', error.ErrorMessage);
        })
    }
    
}

