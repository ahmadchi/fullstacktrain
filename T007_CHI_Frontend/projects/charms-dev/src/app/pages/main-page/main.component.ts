import { Component } from '@angular/core';
import { ChiConfigService, RVAlertsService, GenericApiResponse, ApiService } from 'charms-lib';


@Component({
    selector: 'main-page',
    templateUrl: 'main.component.html',
    styleUrls: ['main.component.scss']
})
export class MainPageComponent
{
    profile: any;

    constructor(
        private apiService: ApiService,
        private configService: ChiConfigService)
    {
        this.profile = configService.getProfile();
    }

    ngOnInit(): void
    {

    }
}