import { Component, Inject, OnDestroy, OnInit, HostListener } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Platform } from '@angular/cdk/platform';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';


import {
    RVAlertsService, FuseConfigService, FuseNavigationService, FuseSidebarService,
    FuseSplashScreenService, ChiConfigService
} from 'charms-lib';
import { CommonService } from './common.service';

import { navigation } from './core/navigation';

import { ControllerConfigs } from './configs';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy
{
    private _unsubscribeAll: Subject<any>;

    fuseConfig: any;
    navigation: any;

    isOnline: boolean;
    isMessageRoute = false;
    isTabVisisble = true;

    @HostListener('window:blur', ['$event'])
    onBlur(event: any): void {
        if (this.isTabVisisble)
        {
            this.isTabVisisble = false;
        }
    }

    @HostListener('window:focus', ['$event'])
    onFocus(event: any): void {
        if (!this.isTabVisisble)
        {
            this.isTabVisisble = true;
        }
    }

    @HostListener('window:beforeunload', ['$event'])
    beforeubload(event) {
        if (localStorage.getItem(window.origin+'_masterTab') === sessionStorage.getItem(window.origin+'_tabIndex'))
        {
            localStorage.removeItem(window.origin+'_masterTab');
        }
    }

    constructor(
        @Inject(DOCUMENT) private document: any,
        private _configService: ChiConfigService,
        private _alertsService: RVAlertsService,
        private _commonService: CommonService,
        private _fuseConfigService: FuseConfigService,
        private _fuseNavigationService: FuseNavigationService,
        private _fuseSidebarService: FuseSidebarService,
        private _fuseSplashScreenService: FuseSplashScreenService,
        private _platform: Platform
    )
    {
        this.isOnline = false;

        // Get default navigation
        this.navigation = navigation;

        // Register the navigation to the service
        this._fuseNavigationService.register('main', this.navigation);

        // Set the main navigation as our current navigation
        this._fuseNavigationService.setCurrentNavigation('main');

        // Add is-mobile class to the body if the platform is mobile
        if ( this._platform.ANDROID || this._platform.IOS )
        {
            this.document.body.classList.add('is-mobile');
        }

        // Set the private defaults
        this._unsubscribeAll = new Subject();

        ControllerConfigs.forEach(config => {

            this._configService.registerController(config.name, config.ctrl);
        });

    }

    ngOnInit(): void
    {
        // Subscribe to config changes
        this._fuseConfigService.config
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe((config) => {

            this.fuseConfig = config;

            // Boxed
            if ( this.fuseConfig.layout.width === 'boxed' )
            {
                this.document.body.classList.add('boxed');
            }
            else
            {
                this.document.body.classList.remove('boxed');
            }

            // Color theme - Use normal for loop for IE11 compatibility
            for ( const className of this.document.body.classList)
            {
                if ( className.startsWith('theme-') )
                {
                    this.document.body.classList.remove(className);
                }
            }

            this.document.body.classList.add(this.fuseConfig.colorTheme);
        });

        this._commonService.callDialogSub.pipe(takeUntil(this._unsubscribeAll)).subscribe(notification =>
        {
            if (notification === 'online_users')
            {
                this.isOnline = true;
            }
            else {
                this.isOnline = false;
            }
        });
    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    toggleSidebarOpen(key): void
    {
        this._fuseSidebarService.getSidebar(key).toggleOpen();
    }

    get selectedSubjects()
    {
        return [];
    }
}
