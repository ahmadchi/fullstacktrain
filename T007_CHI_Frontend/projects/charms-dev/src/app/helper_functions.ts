import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';


@Injectable({
    providedIn: 'root'
})
export class HelperFunctions
{
    private static _instance: HelperFunctions = null;

    constructor()
    {
        if (HelperFunctions._instance == null)
        {
            HelperFunctions._instance = this;
        }
    }

    public static getTime(date: Date): number
    {
        let timestamp = date.getTime() / 1000;
        timestamp = Math.floor(timestamp);
        return timestamp;
    }

    public static previewImage(img: string): string
    {
        if (img === void 0 || img === null || img == '')
        {
            img = '/assets/images/avatar.jpg';
        }
        return img;
    }

    public static getTimezoneOffset(): number 
    {
        let d = new Date()
        let n = Math.abs(d.getTimezoneOffset() * 60);

        return n;
    }

    public static getDefaultScoreSettings(): any
    {
        return {
            time_duration: 2,
            ranges: [
                {
                    min: 0,
                    max: 2,
                    status: 'Normal',
                    color: '#00ff00'
                },
                {
                    min: 2,
                    max: 4,
                    status: 'Normal',
                    color: '#00ff00'
                },
                {
                    min: 4,
                    max: 6,
                    status: 'Warning Low',
                    color: '#e0b300'
                },
                {
                    min: 6,
                    max: 8,
                    status: 'Warning High',
                    color: '#e0b300'
                },
                {
                    min: 8,
                    max: 10,
                    status: 'Critical Low',
                    color: '#ff0000'
                },
                {
                    min: 10,
                    max: 12,
                    status: 'Critical High',
                    color: '#ff0000'
                }
            ]
        };
    }
}