import { NgModule, APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { MatDialogRef } from '@angular/material/dialog';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
  MaterialModule,
  FuseModule,
  FuseProgressBarModule,
  FuseSharedModule,
  FuseSidebarModule,
  ApiService,
  ChiConfigService,
  chiConfigFactoryProvider,
} from 'charms-lib';

import { fuseConfig } from './core/fuse-config';
import { LayoutModule } from './core/layout/layout.module';
import { AppRoutesGuard } from './route-guards/app.routes.guard';

import { RootSharedModule } from 'shared/module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,

    MaterialModule,

    // Fuse modules
    FuseModule.forRoot(fuseConfig),
    FuseProgressBarModule,
    FuseSharedModule,
    FuseSidebarModule,
    LayoutModule,

    RootSharedModule,
  ],
  providers: [
    ApiService,
    {
      provide: MatDialogRef,
      useValue: {},
    },
    ChiConfigService,
    AppRoutesGuard,
    // {
    //   provide: APP_INITIALIZER,
    //   useFactory: chiConfigFactoryProvider,
    //   deps: [ChiConfigService],
    //   multi: true,
    // },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
