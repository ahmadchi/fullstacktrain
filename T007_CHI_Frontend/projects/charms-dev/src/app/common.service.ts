import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { GenericApiResponse, RVAlertsService, ApiService } from 'charms-lib';


@Injectable({ providedIn: 'root' })
export class CommonService {

    public updateCount: Subject<any>;
    public onManageObservation: Subject<any>;
    public callDialogSub: BehaviorSubject<any>;
    public episodetaskCount: number;
    public pendingtaskCount: number;
    public onlineUsersCount: number;
    public orderVerificationCount: number;
    public unhandledObservationsCount: number;
    public stethscopeResultableData: Subject<any>;

    constructor(private apiService: ApiService, private dialog: MatDialog)
    {
        this.updateCount = new Subject();
        this.onManageObservation = new Subject();
        this.callDialogSub = new BehaviorSubject(null);
        this.stethscopeResultableData = new Subject();
        this.episodetaskCount = 0;
        this.pendingtaskCount = 0;
        this.onlineUsersCount = 0;
        this.orderVerificationCount = 0;
        this.unhandledObservationsCount = 0;
    }

    public doTranslation(key: any, val: any, isHtmlEditor?: boolean)
    {
        // const dialogRef = this.dialog.open(SsTransaltionsPageComponent,
        //     {
        //         width: '80vw',
        //         maxWidth: '80vw',
        //         panelClass: 'chi-dialog-container',
        //         autoFocus: false
        //     }
        // );

        // dialogRef.componentInstance.transaltion_key = key;
        // dialogRef.componentInstance.transaltionFor = val;
        // dialogRef.componentInstance.isHtmlEditor = isHtmlEditor;
    }
}
