import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, Router, RouterStateSnapshot } from "@angular/router";
import { ChiConfigService } from 'charms-lib';
import { environment } from '../../environments/environment';
import { CommonService } from '../common.service';


@Injectable()
export class AppRoutesGuard implements CanActivate
{
    constructor(public router: Router,
        private _configService: ChiConfigService,
        private _commonService: CommonService) 
    {
        
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean 
    {
        console.log(route, state);
        if (state.hasOwnProperty('url'))
        {
            const url: string = state.url;
            // console.log(url);

            // if (environment.production) {
            //     // check route guard
            //     if (this._configService.getProfile().group_type === 'Patient') {
            //         return this.checkRoute(url, this._commonService.makePatientMenu());
            //     }
            //     else if (this._configService.getProfile().group_type === 'Chi Admin') {
            //         return this.checkRoute(url, this._commonService.makeChiAdminMenu());
            //     }
            //     else if (this._configService.getProfile().group_type === 'Clinic Admin') {
            //         return this.checkRoute(url, this._commonService.makeClinicAdminMenu());
            //     }
            //     else if (this._configService.getProfile().group_type === 'Clinic Doctor') {
            //         return this.checkRoute(url, this._commonService.makeClinicDoctorMenu());
            //     }
            //     else if (this._configService.getProfile().group_type === 'Receptionist') {
            //         return this.checkRoute(url, this._commonService.makeReceptionistMenu());
            //     }
            //     else if (this._configService.getProfile().group_type === 'Package Manager') {
            //         return this.checkRoute(url, this._commonService.makePackageManagerMenu());
            //     }

            //     return false;
            // }

            return true;
        }
        else 
        {
            return true;
        }
    }

    checkRoute(url: string, menu: any[]) {
        for (const m of menu) {
            if (url === '/' + m.url) {
                return true;
            }
        }

        console.error('401 UnAuthorized', 'you’re just not supposed to access this particular page.');
        return false;
    }
}