import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css'],
})
export class InvoiceComponent implements OnInit {
  myname = 'Ahmads';
  tableData = [];

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    console.log('Invoice Initialized !!! ');

    this.http
      .post<any>('api/invoices/List', { columns: ['amount'] })
      .subscribe({
        next: (data) => {
          console.log(data);
          this.myname = 'Ali';
          this.tableData = data;
        },
        error: (error) => {
          //this.errorMessage = error.message;
          console.error('There was an error!', error);
        },
      });
  }
}
