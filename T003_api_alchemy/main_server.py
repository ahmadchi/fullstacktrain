import tornado.ioloop
from tornado.web import Application as App
from tornado.web import RequestHandler

from modules.customer_controller import CustomerController
from process import insertAndRead, insertAndReadUser, testInvoices

import json


class MainHandler(RequestHandler):
    def get(self):
        # self.write("<h1>Hello, world 9999</h1>")
        # items = ["Item 1", "Item 2", "Item 3"]
        self.render("templates/invoices.html", title="My title")

class InvoiceHandler(RequestHandler):
    def get(self):
        # self.write("<h1>Hello, world 9999</h1>")
        # items = ["Item 1", "Item 2", "Item 3"]
        value, r = testInvoices()
        self.write(r)



class MainHandler2(RequestHandler):
    def get(self):
        value = testInvoices()
        self.write(value)


class CustomerHandler(RequestHandler):
    def get(self):
        value = insertAndRead()
        self.write(value)

class UserHandler(RequestHandler):
    def get(self):
        value, result = insertAndReadUser()
        
        self.write(result)


class MyNewApp(App):

    def __init__(self):
        hand = self.makeHandlers()
        App.__init__(self, hand)

    def makeHandlers(self):
        handlers = [
            (r"/root", MainHandler),
            (r"/test", MainHandler2),
            (r"/customers", CustomerHandler),
            (r"/users", UserHandler),
            (r"/invoices", InvoiceHandler),
        ]
        return handlers


if __name__ == "__main__":
    app = MyNewApp()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()
