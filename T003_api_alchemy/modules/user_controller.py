from .models.users import Users
from .base.base_controller import BaseController

class UserController(BaseController):

    def __init__(self, dbEngine):
        BaseController.__init__(self, dbEngine)
        self.model = Users

