from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Table, Column, Integer, String, MetaData
from .base import Base, Serializer

class Users(Base, Serializer):
   __tablename__ = 'users'
   
   id = Column(Integer, primary_key = True)
   firstname = Column(String(40))
   lastname = Column(String(40))
   

# print("customers imported !..................")