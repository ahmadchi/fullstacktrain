from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey
from .base import Base, Serializer
from sqlalchemy.orm import relationship
from .customers import Customers

class Invoice(Base, Serializer):
   __tablename__ = 'invoices'
   
   id = Column(Integer, primary_key = True)
   custid = Column(Integer, ForeignKey('customers.id'))
   invno = Column(Integer)
   amount = Column(Integer)
   customer = relationship("Customers", back_populates = "invoices1")

Customers.invoices1 = relationship("Invoice", order_by = Invoice.id, back_populates = "customer")

