from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy.inspection import inspect
from sqlalchemy.orm.collections import InstrumentedList
from decimal import Decimal


Base = declarative_base()


class Serializer:
    @staticmethod
    def get_foreign_keys():
        return {}

    def serializeRoot(self):
        obj = {}

        for c in inspect(self).attrs.keys():
            val = getattr(self, c)

            if isinstance(val, InstrumentedList):
                continue
            elif hasattr(val, 'serialize'):
                continue
            elif hasattr(val, '_toJson'):
                obj[c] = val._toJson()
            elif isinstance(val, Decimal):
                obj[c] = float(val)
            else:
                obj[c] = val

        return obj

    def serialize(self, originalClass=None, level=0, childAdded=None, rootRels=None, maxLevel=2):
        obj = {}

        if originalClass is None:
            originalClass = self.__class__
        if childAdded is None:
            childAdded = []
        if rootRels is None:
            rootRels = self.__mapper__.relationships._data.keys()

        allKeys = inspect(self).attrs.keys()
        allKeys = sorted(allKeys)

        for c in allKeys:
            val = getattr(self, c)

            if isinstance(val, InstrumentedList):
                continue
            elif hasattr(val, 'serialize'):
                # if val.__class__ != originalClass and level < maxLevel:
                if level < maxLevel:
                    if val.__class__ == originalClass:
                        obj[c] = val.serializeRoot()
                    elif level == 0:
                        childAdded.append(c)
                        obj2 = val.serialize(originalClass, level + 1, childAdded, rootRels, maxLevel)
                        obj[c] = obj2
                    elif c not in childAdded and c not in rootRels:
                        childAdded.append(c)
                        obj2 = val.serialize(originalClass, level + 1, childAdded, rootRels, maxLevel)
                        obj[c] = obj2

            elif hasattr(val, '_toJson'):
                obj[c] = val._toJson()
            elif isinstance(val, Decimal):
                obj[c] = float(val)
            else:
                obj[c] = val

        return obj

    @staticmethod
    def serialize_list(l):
        return [m.serialize() for m in l]

