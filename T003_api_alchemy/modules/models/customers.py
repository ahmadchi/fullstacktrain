from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey
from .base import Base, Serializer
from sqlalchemy.orm import relationship
# from .invoices import Invoice

class Customers(Base, Serializer):
   __tablename__ = 'customers'
   
   id = Column(Integer, primary_key = True)
   name = Column(String(40))
   address = Column(String(40))
   # address2 = Column(String(40))
   email = Column(String(40))
   # invoices = relationship("Invoice", order_by = Invoice.id, back_populates = "customer")

# Customer.invoices = relationship("Invoice", order_by = Invoice.id, back_populates = "customer")
# print("customers imported !..................")


# class Customer(Base):
#    __tablename__ = 'customers'

#    id = Column(Integer, primary_key = True)
#    name = Column(String)
#    address = Column(String)
#    email = Column(String)
