from .models.customers import Customers
from .base.base_controller import BaseController
from .models.invoices import Invoice

class CustomerController(BaseController):

    def __init__(self, dbEngine):
        BaseController.__init__(self, dbEngine)
        self.model = Customers