from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey
import sqlalchemy as db
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import relationship
from sqlalchemy_utils import database_exists, create_database, drop_database

Base = declarative_base()
# engine = db.create_engine("mysql+mysqlconnector://admin:1234@localhost:3301/chi-HMS",echo = True)

db_config = {
   'user': "ahmad",
   'password': "ahmad",
   'host': "localhost",
   'database': "chiHMS"
}   
# db_url = 'mysql+mysqlconnector://{user}:{password}@{host}/{database}'.format(**db_config)

 

db_url='mysql+mysqlconnector://{user}:{password}@{host}/{database}'.format(**db_config)
engine = db.create_engine(db_url,echo = False)




if not database_exists(engine.url):
	create_database(engine.url)

class Customers(Base):
   __tablename__ = 'customers'
   
   id = Column(Integer, primary_key = True)
   name = Column(String(40))
   address = Column(String(40))
   email = Column(String(40))

class Invoice(Base):
   __tablename__ = 'invoices'
   
   id = Column(Integer, primary_key = True)
   custid = Column(Integer, ForeignKey('customers.id'))
   invno = Column(Integer)
   amount = Column(Integer)
   #customer = relationship("Customers", back_populates = "invoices1")


#Customers.invoices1 = relationship("Invoice", order_by = Invoice.id, back_populates = "customer")
Base.metadata.create_all(engine)   



Session = sessionmaker(bind = engine)
session = Session()

# c1 = Customers(name = 'Ravi Kumar', address = 'Station Road Nanded', email = 'ravi@gmail.com')

# session.add(c1)
# session.commit()

# session.add_all([
#    Customers(name = 'Komal Pande', address = 'Koti, Hyderabad', email = 'komal@gmail.com'), 
#    Customers(name = 'Rajender Nath', address = 'Sector 40, Gurgaon', email = 'nath@gmail.com'), 
#    Customers(name = 'S.M.Krishna', address = 'Budhwar Peth, Pune', email = 'smk@gmail.com')]
# )

# session.commit()


result = session.query(Customers).all()

for row in result:
   print ("Name: ",row.name, "Address:",row.address, "Email:",row.email)

#c1 = Customers(name = "Gopal Krishna", address = "Bank Street Hydarebad", email = "gk@gmail.com")

inv = Invoice(invno = 10, amount = 15000, custid = 1);session.add(inv)
inv = Invoice(invno = 14, amount = 3850, custid = 1);session.add(inv)
inv = Invoice(invno = 13, amount = 5850, custid = 2);session.add(inv)
inv = Invoice(invno = 13, amount = 5850);session.add(inv)

session.commit()

print("=-----------------0")
#METHOD1 : IMPLICIT JOIN
qResult = session.query(Customers, Invoice).filter(Customers.id == Invoice.custid).all()
for c, i in qResult:
  print (c.id, c.name, i.invno, i.amount)


# EXPLICIT JOIN 1. We get primarily list of customers with array of invoices, null if no invoice
print("=-----------------1")
qResult = session.query(Customers, Invoice).join(Invoice).all()

for cust, inv in qResult:
#   for inv in row.invoices1:
      print (cust.id, cust.name, inv.amount)


# EXPLICIT JOIN 2. We get primarily list of invoices, null if no invoice
print("=-----------------2")
qResult = session.query(Invoice, Customers).outerjoin(Customers).all()

for inv, cust in qResult:
	if cust == None:
		csid = "NULL"
	else:
		csid = cust.name

	print (csid, inv.amount)   
      