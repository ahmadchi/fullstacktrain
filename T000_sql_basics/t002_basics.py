
# from sqlalchemy import create_engine

# db_config = {
#             'user': "ahmad",
#             'password': "ahmad",
#             'host': "localhost",
#             'database': "chiHMS"
#         }

# dbUrl = 'mysql+mysqlconnector://{user}:{password}@{host}/{database}'.format(**db_config)

import mysql.connector as db

mydb = db.connect(
  host="localhost",
  user="ahmad",
  password="ahmad",
  database="chiHMS"
)

mycursor = mydb.cursor()


sql = "INSERT INTO customers (name, address) VALUES (%s, %s)"
val = ("John", "Highway 21")
mycursor.execute(sql, val)

mydb.commit()

print(mycursor.rowcount, "record inserted.")


mycursor.execute("select * from customers")

for record in mycursor:
    print(record)