from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Table, Column, Integer, String, MetaData
import sqlalchemy as db
from sqlalchemy.orm import sessionmaker
from sqlalchemy_utils import database_exists, create_database, drop_database

Base = declarative_base()

class Customers(Base):
   __tablename__ = 'customers'
   
   id = Column(Integer, primary_key = True)
   name = Column(String(40))
   address = Column(String(40))
   email = Column(String(40))


class DBEngine():
	def __init__(self):
		engine = db.create_engine("mysql+mysqlconnector://admin:1234@localhost:3301/FSTrain",echo = False)
		if not database_exists(engine.url):
			create_database(engine.url)
		Session = sessionmaker(bind = engine)
		self.session = Session()
		Base.metadata.create_all(engine)  

class BaseController:

	def __init__(self, dbEngine):
		self.db = dbEngine
		self.model = "UNDEFINED"

	def insert(self, data):
		#c1 = self.model(name = data["name"], address = data["address"], email = data["email"])
		c1 = self.model(**data)
		self.db.session.add(c1)

	def read(self):
		result = self.db.session.query(self.model).all()
		return result

class CustomerController(BaseController):

	def __init__(self, dbEngine):
		BaseController.__init__(self, dbEngine)
		self.model = Customers


def main():
	dbEngine = DBEngine()
	custController = CustomerController(dbEngine)

	custController.insert({
		"name" : 'Khan Pande33', 
		"address" : 'Koti, Hyderabad', 
		"email" : 'komal@gmail.com'
	})

	result = custController.read()

	for row in result:
		   print ("Name: ",row.name, "Address:",row.address, "Email:",row.email)

	dbEngine.session.commit()


main()	