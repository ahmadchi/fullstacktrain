import tornado.ioloop
from tornado.web import Application as App
from tornado.web import RequestHandler


class MainHandler(RequestHandler):
    def get(self):
        print("main handler")
        self.write("<h1>Hello, world1</h1>")

class MainHandler2(RequestHandler):
    def get(self):
        print("main handler2")
        self.write("<h1>Hello, world2</h1>")

if __name__ == "__main__":
    app = App([
        ("/", MainHandler),
        ("/api", MainHandler2),
    ])
    app.listen(8888)
    print("starting")
    tornado.ioloop.IOLoop.current().start()
    
