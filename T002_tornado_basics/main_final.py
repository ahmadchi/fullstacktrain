import tornado.ioloop
from tornado.web import Application as App
from tornado.web import RequestHandler

class MainHandler(RequestHandler):
    def get(self):
        self.write("<h1>Hello, world1</h1>")

class MainHandler2(RequestHandler):
    def get(self):
        self.write("<h1>Hello, world3</h1>")

class MyNewApp(App):

    def __init__(self):
        hand = self.makeHandlers()
        App.__init__(self, hand)

    def makeHandlers(self):
        handlers = [
            (r"/", MainHandler),
            (r"/api", MainHandler2),
        ]
        return handlers


if __name__ == "__main__":
    app = MyNewApp()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()

#test