from sqlalchemy import Column, Integer, String
from sqlalchemy import create_engine

db_config = {
   'user': "ahmad",
   'password': "ahmad",
   'host': "localhost",
   'database': "chiHMS"
} 

db_url='mysql+mysqlconnector://{user}:{password}@{host}/{database}'.format(**db_config)

engine = create_engine(db_url,echo = True)


# engine = create_engine('sqlite:///sales.db', echo = True)

from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()

class Customers(Base):
   __tablename__ = 'customers'
   id = Column(Integer, primary_key=True)

   name = Column(String(40))
   address = Column(String(40))
   email = Column(String(40))

Base.metadata.create_all(engine)