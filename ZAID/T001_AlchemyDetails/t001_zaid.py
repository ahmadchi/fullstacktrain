from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey
import sqlalchemy as db
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import relationship
from sqlalchemy_utils import database_exists, create_database, drop_database
from sqlalchemy import Column, Integer, String
from sqlalchemy.engine import create_engine
from sqlalchemy.orm import sessionmaker

Base = declarative_base()
# engine = db.create_engine("mysql+mysqlconnector://admin:1234@localhost:3301/chi-HMS",echo = True)

db_config = {
   'user': "ahmad",
   'password': "ahmad",
   'host': "localhost",
   'database': "chiHMS"
}   
# db_url = 'mysql+mysqlconnector://{user}:{password}@{host}/{database}'.format(**db_config)

class Shop(Base):
    __tablename__ = 'shops'

    id = Column(Integer, primary_key=True)
    name = Column(String(10))
    address = Column(String(32))
    speciality = Column(String(16))
    def __repr__(self):
       return "<Shop(name='%s', address='%s', speciality='%s')>" % (
                            self.name, self.address, self.speciality)

mcc_shop = Shop(name='MCC', address='Islamabad', speciality='Chocolate')
sbb_shop = Shop(name='SBB', address='Islamabad', speciality='Books')
sb_shop = Shop(name="SB", address='Faislabad', speciality='Smell')
zs_shop = Shop(name="ZS", address='Faislabad', speciality='Chocolate')
rm_shop = Shop(name="RM", address='Gujrat', speciality='Books')
gr_shop = Shop(name="GR", address='Faisalabad', speciality='Books')

print(gr_shop)

db_url  = 'mysql+mysqlconnector://{user}:{password}@{host}/{database}'.format(**db_config)
engine  = db.create_engine(db_url,echo = False)
Base.metadata.create_all(engine)

Session = sessionmaker(bind=engine)
session = Session()


print(Shop.__table__)

# session.add_all([
#     User(name='wendy', fullname='Wendy Williams', nickname='windy'),
#     User(name='mary', fullname='Mary Contrary', nickname='mary'),
#     User(name='fred', fullname='Fred Flintstone', nickname='freddy')])


session.add(gr_shop)
session.commit()
session.add_all([
    sbb_shop,
    sb_shop,
    zs_shop,
    rm_shop,
    mcc_shop
])
print(session.new)
gr_shop.address = 'Gujrat'
print(session.dirty)




# ur_user = session.query(Shop).filter_by(name='MCC').first() 