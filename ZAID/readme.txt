============
ONE TO MANY
============
Option 1:
----------
class Parent(Base):
    __tablename__ = 'parent'
    id = Column(Integer, primary_key=True)

    children1x = relationship("Child", back_populates="parent1x")  <-------

class Child(Base):
    __tablename__ = 'child'
    id = Column(Integer, primary_key=True)
    parent_id = Column(Integer, ForeignKey('parent.id'))

    parent1x = relationship("Parent", back_populates="children1x")  <-------

Option 2:
----------
class Parent(Base):
    __tablename__ = 'parent'
    id = Column(Integer, primary_key=True)

    children = relationship("Child", backref="Parent")     <-------

class Child(Base):
    __tablename__ = 'child'
    id = Column(Integer, primary_key=True)
    parent_id = Column(Integer, ForeignKey('parent.id'))


FOR OTHER RELATIONSHIPS, REFER TO : 
    https://docs.sqlalchemy.org/en/14/orm/basic_relationships.html



============
ONE TO MANY
============
Option 1:
----------
class Parent(Base):
    __tablename__ = 'parent'
    id = Column(Integer, primary_key=True)

    children1x = relationship("Child", back_populates="parent1x")  <-------

class Child(Base):
    __tablename__ = 'child'
    id = Column(Integer, primary_key=True)
    parent_id = Column(Integer, ForeignKey('parent.id'))

    parent1x = relationship("Parent", back_populates="children1x")  <-------

A problem I ran into:\
========================
- We cannot assume that after deleting records, the ids will start from 1
  > e.g. ids for Nurse, Doctor, etc, started from 103, ....
