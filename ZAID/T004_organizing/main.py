from grpc import services
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from operator import or_

from modules.models.base import Base
from modules.base.dbengine import db
from modules.models import *


# etype1 = EmployeeType(type = "Nurse")
# etype2 = EmployeeType(type = "Lab Tech")
# db.session.add(etype2)
# db.session.commit()

# rows = [
#     Department(name = "Pediatrics"),
#     Department(name = "Othopaedic"),
# ]

# r = Room(name = "Xray")
# print(r)
# db.session.add(r)
# db.session.commit()

 
rows = [
    EmployeeType(type = "Lab Tech"),
    EmployeeType(type = "Nurse"),
    EmployeeType(type = "Doctor"),
#     Department(name = "pediatrics"),
#     Department(name = "second department"),
#     Employee(
#         name = "Ahsan", 
#         employee_type_id = 1, 
#         department_id = 1,
#     ),
#     Employee(
#         name = "Abdullah", 
#         employee_type_id = 2, 
#         department_id = 2,
#     ),
#     Employee(
#         name = "Taha", 
#         employee_type_id = 3, 
#         department_id = 1,
#     ),
#     Service(name = "TEMP"),
#     Service(name = "GASTRO"),
#     Service(name = "OPT"),
#     Room(name = "CR1", current_service_id = 1),
#     Room(name = "GR1"),
#     Room(name = "CR2"),
#     Patient(name = "Taha"),
#     Patient(name = "Ali"),
#     Patient(name = "Ahsan"),
]

db.session.add_all(rows)
db.session.commit()

# employee1 = db.session.query(Employee).get(1)
# employee1.services = [db.session.query(Service).get(1), 
#                       db.session.query(Service).get(2)]
# employee1.current_room_id = db.session.query(Service).get(2).id
# db.session.commit()

# employees = db.session.query(Employee)

# db.session.add_all(rows)
# db.session.commit()
# exit()
# query = db.session.query(Employee).all()
# print(query)
# 

# print("====================== Employee Types: ")
# x = db.session.query(EmployeeType).all()
# print(x)

# print("====================== Employees:") 
# x = db.session.query(Employee).all()
# print(x)
# exit()

# print("====================== Employees and Services:") 
# ec = EmployeeController(db)
# print(ec.readEmployees())

# exit()
# for obj in query.join(EmployeeType):
#     print(obj)


#===========================================================
# ADDING SERVICES
#===========================================================
# x = db.session.query(Employee).get(1)
# print(type(x))
# x.services = [db.session.query(Service).get(1), db.session.query(Service).get(3)]

# print(db.session.query(Employee).get(1).services)

# x = db.session.query(Employee).get(2)
# print(type(x))
# x.services = [db.session.query(Service).get(2), db.session.query(Service).get(3)]

# x = db.session.query(Employee).get(2)
# print(type(x))
# x.services = [db.session.query(Service).get(1), db.session.query(Service).get(2)]
# x.services.append(db.session.query(Service).get(3))

# db.session.commit()


# exit()
# x = db.session.query(Employee).get(2)
# x.services = [db.session.query(Service).get(2), db.session.query(Service).get(3)]
# print(x)
# exit()

# x = db.session.query(Employee).get(3)
# x.services =   [db.session.query(Service).get(1), 
#                 db.session.query(Service).get(2), 
#                 db.session.query(Service).get(3)]
# print(x)



# y = db.session.query(Employee).filter(or_(Employee.id == 1).all())
# x[0].services


# stmt = employee.update().where(employee.c.lastname == 'Zaid')\
#         .values(department_id = '2')

# db.session.execute(stmt)

# db.session.add_all(rows)
# db.session.commit()

# empCont = EmployeeController(db)
# r = empCont.readEmployees()
# # print(r)

# for emp, type in r:
#     print(emp["name"], type["type"])


# from models.scheduler import Schedular
# from models.clinic import *     
from commands.shift import shift1_setup
from helpers.command_manager import CommandManager

def main():
    
    clinic = None
    schedular = None
    # clinic : Clinic = Clinic("DUBAI CHI CLINIC") 
    # schedular : Schedular = Schedular(clinic)
    commandMgr : CommandManager = CommandManager(clinic, schedular, session=db.session)
   
    if 1:
        for command in shift1_setup:
            # clinic.showScreen()
            print(command)
            commandMgr.execute(command)
            # clinic.showScreen()
            # input()
        
        db.session.commit()

        # clinic.showScreen()

        # clinic.checkConsistency()
        prevCommand = {"cmdType" : ""}
        # for command in shift1_workflow:
        #     print(">>>>>>>>>>>>>>>>>>>>>>>>>> yes, " + prevCommand["cmdType"])
        #     if prevCommand["cmdType"] != 'showScreen':
                
        #         clinic.showScreen()
        #     else:
        #         print(">>>>>>>>>>>>>>>>>>>>>>>>>> yes, desired case")
        #     print(prevCommand)
        #     v = input(command)
        #     if v =="q": break
        #     commandMgr.execute(command)
        #     # print("\n Last command :: ", command)
        #     # input()
        #     prevCommand = command
    # clinic.showScreen()
    # commandMgr.commandLoop()
  

main()