from modules.base.dbengine import DBEngine
from modules.customer_controller import CustomerController
from modules.user_controller import UserController
from modules.invoice_controller import InvoiceController
import json

def insertAndRead():

    dbEngine = DBEngine()
    #exit()
    custController = CustomerController(dbEngine)

    custController.insert({
        "name" : 'Pathan Pande24', 
         "address" : 'Koti, Hyderabad', 
         # "address2" : 'Lahore',
         "email" : 'komal@gmail.com'
    })

    result = custController.read()

    for row in result:
        print ("Name: ",row["name"], "Address:",row["address"], "Email:",row["email"])

    dbEngine.session.commit()
    return "<h1>Hello, world55</h1>"


def insertAndReadUser():

    dbEngine = DBEngine()
    #exit()
    userController = UserController(dbEngine)

    userController.insert({
         "firstname" : 'Pathan Pande2', 
         "lastname" : 'Koti, Hyderabad'
    })

    result = userController.read()

    for row in result:
        print ("Name: ",row["firstname"], "lastname:",row["firstname"])

    dbEngine.session.commit()
    # print(result[0].__dict__)
    # exit()
    r = json.dumps(result)
    return "<h1>Hello, world5555</h1>", r

def testInvoices1():
    dbEngine = DBEngine()
    #exit()
    invoiceController = InvoiceController(dbEngine)

    invoiceController.insert({
         "custid" : 2, 
         "invno" : 12,
         "amount" : 5000
    })

    result = invoiceController.read()

    for row in result:
        print ("custid: ",row["custid"], "invno:",row["invno"], "amount:",row["amount"])

    dbEngine.session.commit()
    r = json.dumps(result)
    return "<h1>Hello, world5555</h1>", r

def testInvoices():
    dbEngine = DBEngine()
    
    invoiceController = InvoiceController(dbEngine)

    result = invoiceController.readInvoices()
    
    for inv, cust in result:
        print ("inv_id: ",inv["id"], "custName:",cust["name"], "amount:",inv["amount"])
    exit()
    r = json.dumps(result)
    return "<h1>Hello, world5555</h1>", r
    
    # c1 = Customer(name = "Gopal Krishna", address = "Bank Street Hydarebad", email = "gk@gmail.com")
    # c1.invoices = [Invoice(invno = 10, amount = 15000), Invoice(invno = 14, amount = 3850)]
    # session.add(c1)
    # session.commit()
