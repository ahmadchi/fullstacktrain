
from grpc import services
from sqlalchemy import Column, ForeignKey, Integer, String

from modules.models.service.service import Service
from ..base import Base, Serializer
from sqlalchemy.orm import relationship
from modules.models.room_service.room_service import room_services
from modules.models.room.room import Room
from modules.base.base_controller import BaseController
from modules.models.department.department_controller import DepartmentController

class RoomController(BaseController):
    
    def __init__(self, dbEngine):
        self.model = Room
        super().__init__(dbEngine)

    def addService(self, command):
        
        name = command["room"]
        query = self.db.session.query(Room).filter(Room.name == name)
        
        query[0].services = [self.db.session.query(Service).get(command["service"])]
        self.db.session.commit()
        return