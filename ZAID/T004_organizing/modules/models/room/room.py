
from grpc import services
from sqlalchemy import Column, ForeignKey, Integer, String
from ..base import Base, Serializer
from sqlalchemy.orm import relationship
from modules.models.room_service.room_service import room_services


class Room(Base, Serializer):
   __tablename__ = 'room'
   
   id = Column(Integer, primary_key = True)
   name = Column(String(40))
   current_service_id = Column(Integer, ForeignKey('service.id'))

   employees = relationship("Employee", backref="Room")
   services  = relationship('Service',
                       secondary=room_services,
                       back_populates='rooms')


   def __repr__(self) -> str:
       st = "room: " + self.name
       return st
