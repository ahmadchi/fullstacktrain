from sqlalchemy import Column, ForeignKey, Table
from ..base import Base
# from modules.models.service import Service

employee_services = Table('employee_services', Base.metadata,
                            Column('employee_id', ForeignKey('employee.id'), primary_key=True),
                            Column('service_id',  ForeignKey('service.id'), primary_key=True))
