from modules.base.base_controller import BaseController
from modules.models.department.department import Department

class DepartmentController(BaseController):
    
    def __init__(self, db):
        self.model = Department
        BaseController.__init__(self, db)


    def getDeptId(self, deptName):
        qResult = self.db.session.query(self.model).filter(Department.name == deptName)
        v = qResult[0] # query was valid and we have at least one
        return v.id 
    