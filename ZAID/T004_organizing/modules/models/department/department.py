from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey
from ..base import Base, Serializer
from sqlalchemy.orm import relationship
# from .invoices import Invoice

class Department(Base, Serializer):
    __tablename__ = 'department'

    id = Column(Integer, primary_key = True)
    name = Column(String(40))

    employees = relationship("Employee", backref="Department")
    employees = relationship("Service", backref="Department")
