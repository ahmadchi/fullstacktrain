from sqlalchemy import Column, ForeignKey, Table
from ..base import Base
# from modules.models.service import Service

room_services = Table('room_services', Base.metadata,
                       Column('room_id', ForeignKey('room.id'), primary_key=True),
                       Column('service_id',  ForeignKey('service.id'), primary_key=True))
