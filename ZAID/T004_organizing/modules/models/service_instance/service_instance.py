
from sqlalchemy import Column, Integer, String, ForeignKey, DateTime
from ..base import Base, Serializer
from sqlalchemy.orm import relationship
# from .invoices import Invoice

class ServiceInstance(Base, Serializer):
    __tablename__ = 'service_instance'

    id = Column(Integer, primary_key = True)
    type = Column(String(40))

    employee   = Column(Integer, ForeignKey('employee.id'))
    department = Column(Integer, ForeignKey('department.id'))
    room       = Column(Integer, ForeignKey('room.id'))
    patient    = Column(Integer, ForeignKey('patient.id'))
    service    = Column(Integer, ForeignKey('service.id'))

    start_time = Column(DateTime)

    def __repr__(self) -> str:
        st = "Employee_Type: " + self.type
        return st
