from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Float, Table, Column, Integer, String, MetaData, ForeignKey
from sqlalchemy import Boolean

from modules.models.department.department import Department
from ..base import Base, Serializer
from sqlalchemy.orm import relationship
from modules.models.employee_service.employee_service import employee_services
from modules.models.room_service.room_service import room_services

# from .invoices import Invoice

class Service(Base, Serializer):
    __tablename__ = 'service'

    id = Column(Integer, primary_key = True)
    name = Column(String(40))
    price = Column(Float)
    emp_share = Column(Float)
    duration = Column(Integer)
    isParallel = Column(Boolean)
    department = Column(Integer, ForeignKey('department.id'))

    employees = relationship('Employee',
                            secondary=employee_services,
                            back_populates='services')

    rooms  = relationship('Room',
                    secondary=room_services,
                    back_populates='services')

    current_rooms  = relationship('Room', backref='Service')


    def insert(self, data):
        print(data)
        exit()


    def __repr__(self) -> str:
        st = ""
        st += "Service Name: " + self.name +"\n"
        return st
