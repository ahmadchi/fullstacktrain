from modules.base.base_controller import BaseController
from modules.models.patient.patient import Patient
from modules.models.employee_type.employee_type import EmployeeType
from modules.models.gender.gender import Gender


class EmployeeTypeController(BaseController):
    def __init__(self, db):
        self.model = EmployeeType
        BaseController.__init__(self, db)


