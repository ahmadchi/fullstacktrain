
from sqlalchemy import Boolean, Column, Integer, String
from ..base import Base, Serializer
from sqlalchemy.orm import relationship


class Patient(Base, Serializer):
    __tablename__ = 'patient'
    
    id = Column(Integer, primary_key = True)
    name = Column(String(40))
    gender = Column(Boolean)
    is_onsite = Column(Boolean)
 
    employees = relationship("Employee", backref="Patient")
 
    def __repr__(self) -> str:
        st = "patient: " + self.name
        return st
