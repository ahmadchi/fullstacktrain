from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import  Boolean, Column, Integer, String, ForeignKey, DateTime, Float
from ..base import Base, Serializer
from sqlalchemy.orm import relationship
from modules.models.employee_service.employee_service import employee_services
# from .invoices import Invoice



class Employee(Base, Serializer):
    __tablename__ = 'employee'

    id = Column(Integer, primary_key = True)
    name = Column(String(40))
    department = Column(Integer, ForeignKey('department.id'))
    current_room_id = Column(Integer, ForeignKey('room.id'))
    current_patient_id = Column(Integer, ForeignKey('patient.id'))
    employee_type = Column(Integer, ForeignKey('employee_type.id'))
    gender = Column(Integer, ForeignKey('gender.id'))
    onsite = Column(Boolean)
    time_arrival = Column(DateTime)
    emp_share = Column(Float)

    services = relationship('Service',
                         secondary=employee_services,
                         back_populates='employees')

    def __repr__(self):
        st = ""
        st += "Name: " + self.name + "\n"
        st += "Employee Type: " + str(self.employee_type_id)+"\n"
        st += "Department Type: " + str(self.department_id)+"\n"
        st += "Current Patient: " + str(self.department_id)+"\n"
        st += "Current room: " + str(self.current_room_id)+"\n"
        if len(self.services) != 0:
            st += "(Services: "+"\n"
            for service in self.services:
                st += "    " + str(service)
            st += ") "+"\n"
        return st


