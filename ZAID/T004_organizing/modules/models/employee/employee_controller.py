from modules.base.base_controller import BaseController
from modules.models.employee.employee import Employee
from modules.models.employee_type.employee_type import EmployeeType


class EmployeeController(BaseController):
    def __init__(self, db):
        self.model = Employee
        BaseController.__init__(self, db)

    def readEmployees(self):
        qResult = self.db.session.query(self.model, EmployeeType).outerjoin(EmployeeType).all()
        outResult = []
        for inv, cust in qResult:
            inv1 = inv.serialize()
            cust1 = cust.serialize()
            outResult.append((inv1, cust1))

        # exit()
        return outResult
    
    def insert(self, data):
        data
        BaseController.insert(self, data)
        return 