from sqlalchemy import Column, Integer, String
from ..base import Base, Serializer
from sqlalchemy.orm import relationship

class Gender(Base, Serializer):
    
   __tablename__ = 'gender'
   
   id = Column(Integer, primary_key = True)
   gender = Column(String(40))

   employees = relationship("Employee", backref="Gender")

   def __repr__(self) -> str:
       st = "Gender: " + self.gender
       return st