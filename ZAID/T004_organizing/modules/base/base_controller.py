#from .dbengine import Base

class BaseController:

    def __init__(self, dbEngine):
        self.db = dbEngine

    def insert(self, data):
        c1 = self.model(**data)
        self.db.session.add(c1)
        self.db.session.commit()
        
    def read(self):
        results = self.db.session.query(self.model).all()

        resultDictArr = []
        for result in results:
            r = result.serialize()
            resultDictArr.append(r)
            # print(r)
            # print(result)
            # exit()
        # for result in results:
        # 	data = {}
        # 	data["firstname"] = result.firstname
        # 	data["lastname"] = result.lastname
        # 	resultDictArr.append(data)
        
        return resultDictArr
