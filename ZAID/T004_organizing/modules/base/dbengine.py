import sqlalchemy as dbs
from sqlalchemy.orm import sessionmaker
# from sqlalchemy_utils import database_exists, create_database, drop_database
from ..models.base import Base

from modules.models import *

class Database:
    def __init__(self):
        db_config = {
                    'user': "admin",
                    'password': "1234",
                    'host': "localhost",
                    'database': "FullStackTrain"
                }


        db_url='mysql+mysqlconnector://{user}:{password}@{host}/{database}'.format(**db_config)
        engine = dbs.create_engine(db_url,echo = False)
        Session = sessionmaker(bind = engine)
        self.session = Session()

        Base.metadata.create_all(engine) 
        print("tables created")

db = Database()