
# from T005_Service_instances.models.clinic import Employee


shift1_setup = [

    # {"cmdType": "addDepartment", "name" : "GASTRO"},
    # {"cmdType": "addDepartment", "name" : "GENPRACT"},


    # {"cmdType": "addService", "department": "GASTRO", "name" : "PRE-CONSULT", "price":    0, "emp_share": 0.1,  'duration': 10},
    # {"cmdType": "addService", "department": "GASTRO", "name" : "CBC",         "price":  500,  "emp_share": 0.1, "isParallel": False, 'duration': 13},
    # {"cmdType": "addService", "department": "GASTRO", "name" : "CONSULT",     "price": 2000,  "emp_share": 0.1, 'duration': 15},
    # {"cmdType": "addService", "department": "GASTRO", "name" : "ENDOCRIN",    "price": 3200,  "emp_share": 0.1, 'duration': 18},
    # {"cmdType": "addService", "department": "GASTRO", "name" : "GASTRO",      "price": 2800,  "emp_share": 0.1, 'duration': 8},
    # {"cmdType": "addService", "department": "GENPRACT", "name" : "X_RAY",       "price": 1500, "emp_share": 0.1, 'duration': 5},
    # {"cmdType": "addService", "department": "GENPRACT", "name" : "DRIP",        "price":  800, "emp_share": 0.1, "isParallel": True},
    # {"cmdType": "addService", "department": "GENPRACT", "name" : "INJECTION",   "price":  300, "emp_share": 0.1, 'duration': 7},

    # {"cmdType": "addRoom", "name": "GR1"},
    # {"cmdType": "addRoom", "name": "GR2"},
    # {"cmdType": "addRoom", "name": "GR3"},
    # {"cmdType": "addRoom", "name": "CR1"},
    # {"cmdType": "addRoom", "name": "EMGY"},
    # {"cmdType": "addRoom", "name": "XR-RM"},
    # {"cmdType": "addRoom", "name": "LB1"},

    # {"cmdType": "addServiceToRoom", "room" : "GR1", "service" : 1},
    # {"cmdType": "addServiceToRoom", "room" : "GR1", "service" : 3},
    # {"cmdType": "addServiceToRoom", "room" : "GR1", "service" : 4},
    # {"cmdType": "addServiceToRoom", "room" : "GR1", "service" : 5},
    # {"cmdType": "addServiceToRoom", "room" : "GR2", "service" : 1},
    # {"cmdType": "addServiceToRoom", "room" : "GR2", "service" : 3},
    # {"cmdType": "addServiceToRoom", "room" : "GR3", "service" : 3},
    # {"cmdType": "addServiceToRoom", "room" : "CR1", "service" : 4},
    # {"cmdType": "addServiceToRoom", "room" : "CR1", "service" : 5},
    # {"cmdType": "addServiceToRoom", "room" : "XR-RM"  , "service" : 6},


    # {"cmdType": "addEmployee", "dept": 9, "name": "LabTech1", "id" : "1", "type" : "3", "isOnsite": True},
    # {"cmdType": "addEmployee", "dept": 9, "name": "NurseA", "id" : "2", "type" : "2", "isOnsite": True},
    # {"cmdType": "addEmployee", "dept": 9, "name": "Dr.Hamed", "id" : "3", "type" : "1", "empShare": .7},
    # {"cmdType": "addEmployee", "dept": 10, "name": "Dr.Arif", "id" : "4", "type" : "1", "isOnsite": False},
    # {"cmdType": "addEmployee", "dept": 10, "name": "Dr.Faisal", "id" : "5", "type" : "1"},
    {"cmdType": "addEmployee", "dept": "GENPRACT", "name": "Dr.Fatima", "gender": "F", "id" : "6", "type" : "1"},

    # {"cmdType": "addRoomToEmployee", "employee" : "3"  , "room" : "CR1" },
    # {"cmdType": "addRoomToEmployee", "employee" : "4"  , "room" : "CR1" },
    # {"cmdType": "addRoomToEmployee", "employee" : "4"  , "room" : "GR1" },
    # {"cmdType": "addRoomToEmployee", "employee" : "6"  , "room" : "GR2" },
    # {"cmdType": "addRoomToEmployee", "employee" : "5"  , "room" : "GR3" },

    # {"cmdType": "addRoomToEmployee", "employee" : "1"  , "room" : "GR1" },
    # {"cmdType": "addRoomToEmployee", "employee" : "1"  , "room" : "GR2" },
    # {"cmdType": "addRoomToEmployee", "employee" : "2"  , "room" : "GR1" },


    # {"cmdType": "addServiceToEmployee", "employee" : "3"  , "service" : "4" },
    # {"cmdType": "addServiceToEmployee", "employee" : "3"  , "service" : "5" },
    # {"cmdType": "addServiceToEmployee", "employee" : "4"  , "service" : "4" },
    # {"cmdType": "addServiceToEmployee", "employee" : "4"  , "service" : "5" },
    # {"cmdType": "addServiceToEmployee", "employee" : "5"  , "service" : "3" },
    # {"cmdType": "addServiceToEmployee", "employee" : "5"  , "service" : "8" },
    # {"cmdType": "addServiceToEmployee", "employee" : "1"  , "service" : "2" },
    # {"cmdType": "addServiceToEmployee", "employee" : "1"  , "service" : "1" },
    # {"cmdType": "addServiceToEmployee", "employee" : "2"  , "service" : "1" },
    # {"cmdType": "addServiceToEmployee", "employee" : "2"  , "service" : "7" },
    # {"cmdType": "addServiceToEmployee", "employee" : "2"  , "service" : "8" },
    # {"cmdType": "addServiceToEmployee", "employee" : "6"  , "service" : "3" },

    # {"cmdType": "addPatient", "name": "Bilal", "id" : "1"},
    # {"cmdType": "addPatient", "name": "Ahmad", "id" : "2", 'isOnsite' : False},
    # {"cmdType": "addPatient", "name": "Ayesha", "gender": "F", "id" : "3"},
    # {"cmdType": "addPatient", "name": "Faisal", "id" : "4"},
    # {"cmdType": "addPatient", "name": "Ahsan", "id" : "5"},
    # {"cmdType": "addPatient", "name": "Imran", "id" : "6"},
]


shift1_workflow = [
 

    # {'cmdType': 'addServiceInstance', "id": "1", 'patient': '1', 'service': '3','employee': '6'},

    {"cmdType": "addExpense", "expenseType": "Misc", "amount": 7600},  #restore to requeted gender/emloyee

    {'cmdType': "addServiceInstance", "id": "1", 'patient': '1', 'service': '4',  "gender": "M", "employee": "4"},
    {"cmdType": "addServiceInstance", "id": "2", "patient" : "2", "service" : "8"},
    {"cmdType": "addServiceInstance", "id": "3", "patient" : "4", "service" : "3"},
    {"cmdType": "addServiceInstance", "id": "4", "patient" : "4", "service" : "3", "bookAt": "10:05"},
    
    {"cmdType": "addServiceInstance", "id": "5", "patient" : "3", "service" : "3", "gender": "F", "bookAt": "12:45"},
    {"cmdType": "addServiceInstance", "id": "6", "patient" : "5", "service" : "5", "gender": "M", "bookAt": "15:30", "employee": "4"},
    {"cmdType": "addServiceInstance", "id": "7", "patient" : "1", "service" : "5", "bookAt": "19:21"},
    {"cmdType": "addServiceInstance", "id": "8", "patient" : "6", "service" : "2", "bookAt": "19:11"},
    # {"cmdType": "clear", "id": "1",}, #clear gender and employee
    {"cmdType": "undo", "id": "1",},  #restore to requeted gender/emloyee
    {"cmdType": "clear", "id": "1",},  #restore to requeted gender/emloyee
    {"cmdType": "schedule", "id": "1",},  #restore to requeted gender/emloyee

    {"cmdType": "assign", "id": "1", "room": "LB1"},  #restore to requeted gender/emloyee
    {"cmdType": "assign", "id": "1", "employee": "3"},  #restore to requeted gender/emloyee
    #{"cmdType": "assign", "id": "1", "employee": "3", "room": "LB1"},  #restore to requeted gender/emloyee

    # {"cmdType": "prop", "id": "6", "room" : "GR3", "doctor": "1"},
    # {"cmdType": "ready", "id": "6",},

    # {"cmdType": "addServiceInstance", "id": "7", "patient" : "4", "service" : "5", "gender": "M", "employee": "4"},
    # {"cmdType": "addServiceInstance", "id": "8", "patient" : "6", "service" : "3", "gender": "M"},

    {"cmdType": "changeServInstState", "id": "2", "state" : "ACCEPTED"},
    {"cmdType": "changeServInstState", "id": "1", "state" : "ACCEPTED"},
    {"cmdType": "changeServInstState", "id": "3", "state" : "ACCEPTED"},
    
    {"cmdType": "changeServInstState", "id": "2", "state" : "ACK"},
    {"cmdType": "changeServInstState", "id": "2", "state" : "STARTED"},
    {"cmdType": "changeServInstState", "id": "2", "state" : "DONE"},

    {"cmdType": "changeServInstState", "id": "1", "state" : "ACK"},
    {"cmdType": "changeServInstState", "id": "1", "state" : "STARTED"},
    {"cmdType": "changeServInstState", "id": "1", "state" : "DONE"},

    {"cmdType": "changeServInstState", "id": "3", "state" : "ACK"},
    {"cmdType": "changeServInstState", "id": "3", "state" : "STARTED"},
    {"cmdType": "changeServInstState", "id": "3", "state" : "DONE"},

    {"cmdType": "changeServInstState", "id": "4", "state" : "ACCEPTED"},
    {"cmdType": "changeServInstState", "id": "4", "state" : "ACK"},
    {"cmdType": "changeServInstState", "id": "4", "state" : "STARTED"},
    {"cmdType": "changeServInstState", "id": "4", "state" : "DONE"},    
    
    {"cmdType": "changeServInstState", "id": "5", "state" : "ACCEPTED"},
    {"cmdType": "changeServInstState", "id": "5", "state" : "ACK"},
    {"cmdType": "changeServInstState", "id": "5", "state" : "STARTED"},
    {"cmdType": "changeServInstState", "id": "5", "state" : "DONE"},    

{"cmdType": "changeServInstState", "id": "6", "state" : "ACCEPTED"},
{"cmdType": "changeServInstState", "id": "6", "state" : "ACK"},
{"cmdType": "changeServInstState", "id": "6", "state" : "STARTED"},
{"cmdType": "changeServInstState", "id": "6", "state" : "DONE"},    

{"cmdType": "changeServInstState", "id": "7", "state" : "ACCEPTED"},
{"cmdType": "changeServInstState", "id": "7", "state" : "ACK"},
{"cmdType": "changeServInstState", "id": "7", "state" : "STARTED"},
{"cmdType": "changeServInstState", "id": "7", "state" : "DONE"},    

{"cmdType": "changeServInstState", "id": "8", "state" : "ACCEPTED"},
{"cmdType": "changeServInstState", "id": "8", "state" : "ACK"},
{"cmdType": "changeServInstState", "id": "8", "state" : "STARTED"},
{"cmdType": "changeServInstState", "id": "8", "state" : "DONE"},    
    # c{"cmdType": "reassign", "id": "4", "Room" : "2"},


    # {"cmdType": "addServiceInstance", "patient" : "2", "service" : "8" , "room": "EMGY", "employee" : "2"},
    # {"cmdType": "addServiceInstance", "patient" : "4", "service" : "3" , "room": "GR1", "employee" : "5"},
]

