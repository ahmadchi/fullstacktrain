# from models.clinic import *
# from models.scheduler import Schedular
# import time
from modules.base.dbengine import db
from modules.models import *
from datetime import datetime, timezone

class CommandManager:
    # def __init__(self, clinic: Clinic, schedular: Schedular):
    def __init__(self, clinic, schedular, session):
        self.clinic = clinic
        self.schedular = schedular
        self.menu = []
        self.makeMenu()
        self.session = session

    def execute(self, command):
        print("CCC", command)
        # if cmdType == "showScreen":
        #     self.clinic.showScreen(command["screenNo"])
        # elif cmdType == "showMenu":
        #     self.showMenu()
        cmdType = command["cmdType"]
        del command['cmdType']
        if cmdType == "addDepartment":
            self.addDepartment(command)
        elif cmdType == "addService":
            self.addService(command)
        elif cmdType == "addRoom":
            self.addRoom(command)
        elif cmdType == "addPatient":
            self.addPatient(command)
        elif cmdType == "addEmployee":
            self.addEmployee(command)
        elif cmdType == "addServiceToRoom":
            self.addServiceToRoom(command)
        elif cmdType == "addServiceToEmployee":
            self.addServiceToEmployee(employee = command["employee"], service = command["service"])
        elif cmdType == "addRoomToEmployee":
            self.addRoomToEmployee(command)
        elif cmdType == "addServiceInstance":
            sInst = self.addServiceInstance(command)
            self.clinic.showScreen()
            # infoPrint("Going to SCHEDULE ===== ")
            self.schedular.scheduleInstanceDedicatedService(sInst)
        elif cmdType == "changeServInstState":
            # self.changeServiceInstanceState(self.clinic, command)
            serviceInst = self.clinic.serviceInstances.get(command["id"], False)
            if serviceInst == False: 
                infoPrint("ServiceInstance not found with ID: "+command["id"])
                return
            serviceInst.changeState(command["state"])

            if command["state"] == "DONE":
                # if queue has space schedule
                self.clinic.showScreen()
                input("A command has completed"+ str(command))
                self.schedular.schedule()

        elif cmdType == "undo":
            serviceInst = self.clinic.serviceInstances[command["id"]]
            serviceInst.undo()
            # self.clinic.showScreen()

        elif cmdType == "clear":
            serviceInst = self.clinic.serviceInstances[command["id"]]
            serviceInst.clear()
            # self.clinic.showScreen()

        elif cmdType == "schedule":
            serviceInst = self.clinic.serviceInstances.get(command["id"], False)
            if serviceInst == False: 
                errorPrint("ServiceInstance not found with ID: "+command["id"])
                return
            self.schedular.scheduleInstanceDedicatedService(serviceInst)
            # self.clinic.showScreen()

        elif cmdType == "assign":
            # input("Going to assign")
            serviceInst = self.clinic.serviceInstances.get(command["id"], False)
            if serviceInst == False: 
                errorPrint("ServiceInstance not found with ID: "+command["id"])
                return

            serviceInst.assign(command)
            # self.schedular.scheduleInstanceDedicatedService(serviceInst)
            # self.clinic.showScreen()
        elif cmdType == "addExpense":
            self.clinic.addExpense(command["expenseType"], command["amount"])
            
        else:
            print("Command not found: ", command)
    
    def addServiceInstance(self, command):

        id = command["id"]
        patient = self.clinic.patients[command["patient"]] 
        service = self.clinic.services[command["service"]]
        
        gender = command.get("gender", "-")
        bookAt = command.get("bookAt", None)

        if ("employee" in command):
            employee =  self.clinic.employees[command["employee"]]
        else:
            employee = None

        if ("room" in command):
            room =  self.clinic.rooms[command["room"]]
        else:
            room = None

        return ServiceInstance(self.clinic, id, patient, service, bookAt, gender, room, employee)

    def addDepartment(self, command):
        # name = command["name"]
        
        cont = DepartmentController(db)
        cont.insert(command)
        # self.session.add(Department(name = name))

    def addService(self, command):
        # dept = command["dept"]
        # name = command["name"]
        # isParallel = command.get("isParallel", False)
        # duration = command.get("duration", 10)
        # price = command.get("price", 0)
        # empShare = command.get("empShare", 0.0)

        # self.session.add(Service(name=name, price=price, emp_share=empShare, duration=duration, isParallel=isParallel, department=dept))
        cont = ServiceController(db)
        cont.insert(command)

    def addRoom(self, command):
        self.session.add(Room(name=command["name"]))

    def addPatient(self, command):
        gender = genderChoosenOne(command.get("gender", "M"))
        print(gender)
        isOnsite = command.get("isOnsite", True)
        print(isOnsite)
        
        self.session.add(
            Patient(name = command["name"], gender = gender, is_onsite = isOnsite)
        )

    def addEmployee(self, command):
        ec = EmployeeController(db)

        self.session.add(
            Employee(name = command["name"], employee_type_id = command["type"], department_id = dept, onsite = isOnsite, gender = gender, time_arrival = datetime.now(), emp_share = empShare)
        )

    def addServiceToRoom(self, command):
        room = command["room"]
        service = command["service"]
        self.clinic.rooms[room].addService(self.clinic.services[service])

    def addServiceToEmployee(self, employee, service):
        self.clinic.employees[employee].addService(self.clinic.services[service])

    def addRoomToEmployee(self, command):
        employee = command["employee"]
        room = command["room"]
        print(f"addRoomToEmployee, employee = {self.clinic.employees[employee].name}, room = {room}")
        self.clinic.employees[employee].addRoom(self.clinic.rooms[room])

    def addServiceToEmployee(self, employee, service):
        self.clinic.employees[employee].addService(self.clinic.services[service])


    def makeMenu(self):
        self.menu.append({"id":  "0", "command": 'Menu', "func": self.makeShowMenu})
        self.menu.append({"id":  "1", "command": 'Financial Screen', "func": self.makeShowScreen0})
        self.menu.append({"id":  "2", "command": 'Time Screen', "func": self.makeShowScreen1})
        self.menu.append({"id":  "3", "command": 'Add Department', "func": self.makeDepartmentCommand})
        self.menu.append({"id":  "4", "command": 'Add Service', "func": self.makeServiceCommand})
        self.menu.append({"id":  "5", "command": 'Add Room', "func": self.makeRoomCommand})
        self.menu.append({"id":  "6", "command": 'Add Patient', "func": self.makePatientCommand})
        self.menu.append({"id":  "7", "command": 'Add Service Instance', "func": self.makeServiceInstanceCommand})
        self.menu.append({"id":  "8", "command": 'Change Serv Inst State', "func": self.makeServiceStateCommand})
        self.menu.append({"id":  "9", "command": 'Undo', "func": self.makeUndo})
        self.menu.append({"id": "10", "command": 'Clear', "func": self.makeClear})
        self.menu.append({"id": "11", "command": 'Schedule', "func": self.makeSchedule})
        self.menu.append({"id": "12", "command": 'Add Expense', "func": self.makeExpense})
        self.menu.append({"id":  "q", "command": 'Quit', "func": exit})


    def showMenu(self):
        for menu in self.menu:
            print(menu["id"]+".", menu["command"])


    def commandLoop(self):
        for i in range(150):
            # self.clinic.showScreen()

            com = input("Please enter command (showMenu, checkConsistency, ): ")
            flag = False
            for menu in self.menu:
                if com == menu["id"] or com == menu["command"]:
                    command = menu["func"]()
                    flag = True

            if flag == False:
                errorPrint("Unknown command: Please enter valid command ....!  ")
                self.showMenu()
                continue

            self.execute(command)

            if com != "0" and com != '1' and com != "2":
                self.clinic.showScreen()


    def getParam(self, command, key, prompt, required = False):
        req = "(required)" if required == True else ""

        v = input(prompt + req + ": ")
        if v.strip() != "": 
            command[key] = v.strip()

        else: 
            if required == True:
                while v.strip() == "":
                    v = input(prompt + req + ">>>>  : ")
                command[key] = v.strip()
        return command

    def makeShowScreen0(self):
        command = {"cmdType": "showScreen", "screenNo": 0}
        return command

    def makeShowScreen1(self):
        command = {"cmdType": "showScreen", "screenNo": 1}
        return command


    def makeShowMenu(self):
        command = {"cmdType": "showMenu"}
        return command

    def makeDepartmentCommand(self):
        command = {"cmdType": "addDepartment"}
        self.getParam(command, "id", "Department id : ", required = True)
        self.getParam(command, "name", "Department Name : ", required = True)
        return command

    def makeServiceCommand(self):
        command = {"cmdType": "addService"}# "name" : "INJECTION", "id": "8", 'duration': 7},
        self.getParam(command, "name", "Service Name : ", required = True)
        self.getParam(command, "id", "Service id : ", required = True)
        self.getParam(command, "price", "Service Price : ", required = True)
        self.getParam(command, "duration", "Service Duration : ", required = False)
        command["price"] = int(command["price"])
        return command

    def makeRoomCommand(self):
        command = {"cmdType": "addRoom"}
        self.getParam(command, "id", "Room id : ", required = True)
        return command


    def makePatientCommand(self):
        command = {"cmdType": "addPatient"}
        self.getParam(command, "name", "Patient Name : ", required = True)
        self.getParam(command, "id", "Patient id : ", required = True)
        return command
        
    def makeServiceInstanceCommand(self):
        command = {"cmdType": "addServiceInstance"}
        self.getParam(command, "id", "ServiceInstance Id : ", required = True)
        self.getParam(command, "patient", "Patient Id : ", required = True)
        self.getParam(command, "service", "Service Id : ", required = True)
        self.getParam(command, "gender", "Requirement of Gender of Service Provider : ")
        self.getParam(command, "bookAt", "Please enter Time of booking (Ignore, if Walkin): ")
        self.getParam(command, "employee", "employee id (if want a specific provider): ")
        return command

    def makeServiceStateCommand(self):
        command = {"cmdType": "changeServInstState"}
        self.getParam(command, "id", "Service Id : ", required = True)
        print('    ("ACCEPTED", "ACK", "STARTED", "DONE")')
        self.getParam(command, "state", "State : ", required = True)
        return command


    def makeUndo(self):
        command = {"cmdType": "undo"}
        self.getParam(command, "id", "Service Id : ", required = True)
        return command

    def makeClear(self):
        command = {"cmdType": "clear"}
        self.getParam(command, "id", "Service Id : ", required = True)
        return command

    def makeSchedule(self):
        command = {"cmdType": "schedule"}
        self.getParam(command, "id", "Service Id : ", required = True)
        return command

    def makeExpense(self):
        command = {"cmdType": "addExpense"}
        print("     (Rent, Utilities, Consumables, Misc, Salaries, Consultant Share) ")
        self.getParam(command, "expenseType", "Expense Type: ", required = True)
        self.getParam(command, "amount", "Expense Amount: ", required = True)
        command['amount'] = int(command['amount'])
        # print(command)
        # exit()
        return command

def genderChoosenOne(gender):
    if gender == 'F':
        return True
    elif gender == 'M':
        return False
    else:
        return None
