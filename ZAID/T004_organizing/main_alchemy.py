from grpc import services
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from operator import or_

from modules.models.base import Base
from modules.base.dbengine import db
from modules.models import *
from modules.models.employee_type.employee_type_controller import EmployeeTypeController
from modules.models.room.room_controller import RoomController
from modules.models.gender.gender_controller import GenderController


# etype1 = EmployeeType(type = "Nurse")
# etype2 = EmployeeType(type = "Lab Tech")
# db.session.add(etype2)
# db.session.commit()

# rows = [
#     Department(name = "Pediatrics"),
#     Department(name = "Othopaedic"),
# ]

# r = Room(name = "Xray")
# print(r)
# db.session.add(r)
# db.session.commit()

 
# rows = [
#     EmployeeType(type = "Lab Tech"),
#     EmployeeType(type = "Nurse"),
#     EmployeeType(type = "Doctor"),
#     Department(name = "pediatrics"),
#     Department(name = "second department"),
#     Employee(
#         name = "Ahsan", 
#         employee_type_id = 1, 
#         department_id = 1,
#     ),
#     Employee(
#         name = "Abdullah", 
#         employee_type_id = 2, 
#         department_id = 2,
#     ),
#     Employee(
#         name = "Taha", 
#         employee_type_id = 3, 
#         department_id = 1,
#     ),
#     Service(name = "TEMP"),
#     Service(name = "GASTRO"),
#     Service(name = "OPT"),
#     Room(name = "CR1", current_service_id = 1),
#     Room(name = "GR1"),
#     Room(name = "CR2"),
#     Patient(name = "Taha"),
#     Patient(name = "Ali"),
#     Patient(name = "Ahsan"),
# ]

# db.session.add_all(rows)
# db.session.commit()

# employee1 = db.session.query(Employee).get(1)
# employee1.services = [db.session.query(Service).get(1), 
#                       db.session.query(Service).get(2)]
# employee1.current_room_id = db.session.query(Service).get(2).id
# db.session.commit()

# employees = db.session.query(Employee)

# db.session.add_all(rows)
# db.session.commit()
# exit()
# query = db.session.query(Employee).all()
# print(query)
# 

# print("====================== Employee Types: ")
# x = db.session.query(EmployeeType).all()
# print(x)

# print("====================== Employees:") 
# x = db.session.query(Employee).all()
# print(x)
# exit()

# print("====================== Employees and Services:") 
# ec = EmployeeController(db)
# print(ec.readEmployees())

# exit()
# for obj in query.join(EmployeeType):
#     print(obj)


#===========================================================
# ADDING SERVICES
#===========================================================
# x = db.session.query(Employee).get(1)
# print(type(x))
# x.services = [db.session.query(Service).get(1), db.session.query(Service).get(3)]

# print(db.session.query(Employee).get(1).services)

# x = db.session.query(Employee).get(2)
# print(type(x))
# x.services = [db.session.query(Service).get(2), db.session.query(Service).get(3)]

# x = db.session.query(Employee).get(2)
# print(type(x))
# x.services = [db.session.query(Service).get(1), db.session.query(Service).get(2)]
# x.services.append(db.session.query(Service).get(3))

# db.session.commit()


# exit()
# x = db.session.query(Employee).get(2)
# x.services = [db.session.query(Service).get(2), db.session.query(Service).get(3)]
# print(x)
# exit()

# x = db.session.query(Employee).get(3)
# x.services =   [db.session.query(Service).get(1), 
#                 db.session.query(Service).get(2), 
#                 db.session.query(Service).get(3)]
# print(x)



# y = db.session.query(Employee).filter(or_(Employee.id == 1).all())
# x[0].services


# stmt = employee.update().where(employee.c.lastname == 'Zaid')\
#         .values(department_id = '2')

# db.session.execute(stmt)

# db.session.add_all(rows)
# db.session.commit()

# empCont = EmployeeController(db)
# r = empCont.readEmployees()
# print(r)

# for emp, type in r:
#     print(emp["name"], type["type"])


# dc = DepartmentController(db)
# r = dc.getDeptId("GASTRO")
# print(r)

# dc = DepartmentController(db)
# dc.insert({"name": "GENPRACT"})
# dc.insert({"name": "GASTRO"})
# dc.insert({"name": "ORTHO"})

# sc = ServiceController(db)
# sc.insert({"department": "GASTRO", "name" : "PRE-CONSULT", "price":    0, "emp_share": 0.1,  'duration': 10})

# rc = RoomController(db)
# rc.insert({"name": "GR1"})

# rc = RoomController(db)
# rc.addService({"room" : "GR1", "service" : 1})

empCont = EmployeeController(db)
empCont.insert(
    {"deparment": "GENPRACT", "name": "Dr.Fatima", "gender": 1, "id" : "6", "type" : 1},
)

# gc = GenderController(db)
# gc.insert({"gender": "M"})
# exit()
# gc.insert({"gender": "F"})
# gc.insert({"gender": "-"})


# empCont = EmployeeController(db)
# empCont.insert(
#     {"department": 2, "name": "Dr.Fatima", "gender": 2, "employee_type" : 1}
# )
