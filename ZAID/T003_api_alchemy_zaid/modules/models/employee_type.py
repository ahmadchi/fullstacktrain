from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, MetaData, ForeignKey
from .base import Base, Serializer
from sqlalchemy.orm import relationship
# from .invoices import Invoice

class EmployeeType(Base, Serializer):
   __tablename__ = 'employee_type'
   
   id = Column(Integer, primary_key = True)
   type = Column(String(40))

   employees = relationship("Employee", backref="EmployeeType")

   def __repr__(self) -> str:
       st = "Employee_Type: " + self.type
       return st
