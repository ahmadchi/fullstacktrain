from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey
from .base import Base, Serializer
from sqlalchemy.orm import relationship
from modules.models.employee_service import employee_services
# from .invoices import Invoice



class Service(Base, Serializer):
   __tablename__ = 'service'
   
   id = Column(Integer, primary_key = True)
   name = Column(String(40))
   employees = relationship('Employee',
                            secondary=employee_services,
                            back_populates='services')

   def __repr__(self) -> str:
        st = ""
        st += "Service Name: " + self.name +"\n"
        return st
