from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey
from .base import Base, Serializer
from sqlalchemy.orm import relationship
from modules.models.employee_service import employee_services
# from .invoices import Invoice



class Employee(Base, Serializer):
    __tablename__ = 'employee'

    id = Column(Integer, primary_key = True)
    name = Column(String(40))
    employee_type_id = Column(Integer, ForeignKey('employee_type.id'))
    department_id = Column(Integer, ForeignKey('department.id'))

    services = relationship('Service',
                         secondary=employee_services,
                         back_populates='employees')

    def __repr__(self):
        st = ""
        st += "Name: " + self.name + "\n"
        st += "Employee Type: " + str(self.employee_type_id)+"\n"
        st += "Department Type: " + str(self.department_id)+"\n"
        return st


