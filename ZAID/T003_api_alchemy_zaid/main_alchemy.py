from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from operator import or_


from modules.base.dbengine import db

from modules.models.employee_type import EmployeeType
from modules.models.employee import Employee
from modules.models.service import Service
from modules.models.department import Department

class BaseController:
    def __init__(self, db):
        self.db = db
        self.dbs = db.session

    def read(self):

        result = self.dbs.query(self.model).filter(self.model.id>0).all()
        return result


class EmployeeController(BaseController):
    def __init__(self, db):
        self.model = Employee
        BaseController.__init__(self, db)

    def readEmployees(self):
        qResult = self.db.session.query(self.model, EmployeeType).outerjoin(EmployeeType).all()
        outResult = []
        for inv, cust in qResult:
            inv1 = inv.serialize()
            cust1 = cust.serialize()
            outResult.append((inv1, cust1))

        # exit()
        return outResult

# db.session.add_all(rows)
# db.session.commit()

# etype1 = EmployeeType(type = "Nurse")
# etype2 = EmployeeType(type = "Lab Tech")

# db.session.add(etype2)
# db.session.commit()


# emp1 = Employee(name = "Zainab", employee_type_id = 1)

# db.session.add(emp1)
# db.session.commit()

# rows = [
#     Department(name = "Pediatrics"),
#     Department(name = "Othopaedic"),
# ]

rows = [
    # EmployeeType(type = "Lab Tech"),
    # EmployeeType(type = "Nurse"),
    # EmployeeType(type = "Doctor"),
    # Department(name = "pediatrics"),
    # Department(name = "second department"),
    # Employee(
    #     name = "Ahsan", 
    #     employee_type_id = 1, 
    #     department_id = 1,
    # ),
    # Employee(
    #     name = "Abdullah", 
    #     employee_type_id = 2, 
    #     department_id = 2,
    # ),
    # Employee(
    #     name = "Taha", 
    #     employee_type_id = 3, 
    #     department_id = 1,
    # ),
    # Service(name = "TEMP"),
    # Service(name = "GASTRO"),
    # Service(name = "OPT"),
]

# db.session.add_all(rows)
# db.session.commit()
# exit()
# query = db.session.query(Employee).all()
# print(query)
# 
ec = EmployeeController(db)
print(ec.readEmployees())

exit()
# for obj in query.join(EmployeeType):
#     print(obj)


#===========================================================
# ADDING SERVICES
#===========================================================
# x = db.session.query(Employee).get(1)
# print(type(x))
# x.services = [db.session.query(Service).get(1), db.session.query(Service).get(3)]

# print(db.session.query(Employee).get(1).services)

# x = db.session.query(Employee).get(2)
# print(type(x))
# x.services = [db.session.query(Service).get(2), db.session.query(Service).get(3)]

# x = db.session.query(Employee).get(2)
# print(type(x))
# x.services = [db.session.query(Service).get(1), db.session.query(Service).get(2)]
# x.services.append(db.session.query(Service).get(3))

# db.session.commit()


exit()
x = db.session.query(Employee).get(2)
x.services = [db.session.query(Service).get(2), db.session.query(Service).get(3)]
print(x)
exit()

x = db.session.query(Employee).get(3)
x.services =   [db.session.query(Service).get(1), 
                db.session.query(Service).get(2), 
                db.session.query(Service).get(3)]
print(x)



y = db.session.query(Employee).filter(or_(Employee.id == 1).all())
x[0].services




db.session.commit()

x = db.session.query(EmployeeType).all()
print(x)

x = db.session.query(Employee).all()
print(x)

# stmt = employee.update().where(employee.c.lastname == 'Zaid')\
#         .values(department_id = '2')

# db.session.execute(stmt)

# db.session.add_all(rows)
# db.session.commit()

# empCont = EmployeeController(db)
# r = empCont.readEmployees()
# # print(r)

# for emp, type in r:
#     print(emp["name"], type["type"])
