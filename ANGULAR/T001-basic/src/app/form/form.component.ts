import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// import { BasicService1 } from 'projects/t005-basic-service/src/app/basic-service.service';
import { Person } from '../person/person';

@Component({
  selector: 'sample-form',
  templateUrl: 'form.component.html',
  styleUrls: ['form.component.scss'],
  //   providers: [BasicService1],
})
export class SampleFormComponent implements OnInit {
  cFormGroup: FormGroup;
  //   persons = [];
  @Output() personAdded1 = new EventEmitter<Person>();
  @Input() selectedPerson: any; //= new EventEmitter();

  constructor(private _fb: FormBuilder) {
    console.log('this is here');
    this.selectedPerson = new Person('NoName', 'NoName', 12);
  }

  ngOnInit(): void {
    this.cFormGroup = this._fb.group({
      fname1: ['', Validators.required],
      lname: ['', Validators.required],
      age: [null, Validators.required],
    });
    // this.selectedPerson = new Person("Sara", "Qarab", 20);
    // this.cFormGroup.value= this.selectedPerson;
  }

  onSave() {
    let data = this.cFormGroup.value;
    let person = new Person(data['fname1'], data['lname'], data['age']);
    // this.persons.push(person);
    // console.log(this.persons);
    // this.basicService.addPerson(person);
    this.personAdded1.emit(person);
    console.log(this.selectedPerson);
  }
}
