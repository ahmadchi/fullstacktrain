export class Person {
  first_name: string;
  last_name: string;
  age: number;

  constructor(fname: string, lname: string, age: number) {
    this.first_name = fname;
    this.last_name = lname;
    this.age = age;
  }
}
