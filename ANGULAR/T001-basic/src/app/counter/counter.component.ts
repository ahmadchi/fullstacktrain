import { Component } from '@angular/core';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.css'],
})
export class CounterComponent {
  //   title = 'counter';
  counter: number = 0;

  buttonPressed() {
    this.counter += 1;
    console.log(this.counter);
  }
}
