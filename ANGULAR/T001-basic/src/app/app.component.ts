import { Component } from '@angular/core';
import { Person } from './person/person';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'T001-basic';

  onPersonAdd(person: Person) {
    console.log(person);
  }
}
