import { Component, OnInit } from "@angular/core";
import { BasicService1 } from "projects/t005-basic-service/src/app/basic-service.service";
import { Person } from "shared/person/person";

@Component({
  selector: "app-root",
  templateUrl: "data-binding.component.html",
  styleUrls: ["data-binding.component.scss"],
  providers: [BasicService1],
})
export class DataBindingPracticeComponent implements OnInit {
  personElement: Person;

  constructor() {
    this.personElement = new Person("Ahmad", "ALI", 22);
  }

  ngOnInit(): void {}

  onChangeName() {
    this.personElement.last_name = "Hassan";
  }

  onPersonAdd(person: Person) {
    console.log(person);
    this.personElement = person;
  }
}
