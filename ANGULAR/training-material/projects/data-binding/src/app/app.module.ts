import { BrowserModule } from "@angular/platform-browser";
import { ModuleWithProviders, NgModule } from "@angular/core";

// import { AppRoutingModule } from "./app-routing.module";
import { DataBindingPracticeComponent } from "./data-binding.component";
import { FlexLayoutModule, FlexModule } from "@angular/flex-layout";
import { ReactiveFormsModule } from "@angular/forms";
import { MatCardModule } from "@angular/material/card";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatButtonModule } from "@angular/material/button";
import { FormSharedModule } from "shared/form/module";

@NgModule({
  declarations: [DataBindingPracticeComponent],
  imports: [
    BrowserModule,
    FormSharedModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    FlexModule,
    FlexLayoutModule,
    // AppRoutingModule,
  ],
  providers: [],
  bootstrap: [DataBindingPracticeComponent],
  exports: [],
  // bootstrap: []
})
export class DataBindingModule {
  static forRoot(): ModuleWithProviders<DataBindingModule> {
    return {
      ngModule: DataBindingModule,
      providers: [],
    };
  }
}
