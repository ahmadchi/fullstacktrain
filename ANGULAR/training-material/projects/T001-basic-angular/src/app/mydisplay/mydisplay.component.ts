import { Component, Input, OnInit } from "@angular/core";

@Component({
  selector: "app-mydisplay",
  templateUrl: "./mydisplay.component.html",
  styleUrls: ["./mydisplay.component.css"],
})
export class MydisplayComponent implements OnInit {
  @Input() myCount: number;
  constructor() {
    this.myCount = 88;
  }

  ngOnInit(): void {}
}
