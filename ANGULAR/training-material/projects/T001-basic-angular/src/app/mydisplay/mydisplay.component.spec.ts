import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MydisplayComponent } from './mydisplay.component';

describe('MydisplayComponent', () => {
  let component: MydisplayComponent;
  let fixture: ComponentFixture<MydisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MydisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MydisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
