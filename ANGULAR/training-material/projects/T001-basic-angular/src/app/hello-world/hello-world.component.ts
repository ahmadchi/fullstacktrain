import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./hello-world.component.html",
  styleUrls: ["./hello-world.component.css"],
})
export class HelloWorldComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
