import { Component, EventEmitter, OnInit, Output } from "@angular/core";

@Component({
  selector: "app-mybutton",
  templateUrl: "./mybutton.component.html",
  styleUrls: ["./mybutton.component.css"],
})
export class MybuttonComponent implements OnInit {
  count = 0;
  @Output() countEmitter = new EventEmitter<number>();
  constructor() {}

  ngOnInit(): void {}
  onClick() {
    this.count += 1;
    console.log(this.count);
    this.countEmitter.emit(this.count);
    // this.arr.push(this.count);
  }
}
