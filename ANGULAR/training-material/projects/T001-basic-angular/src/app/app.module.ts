import { RouterModule } from "@angular/router";
import { BrowserModule } from "@angular/platform-browser";
import { ModuleWithProviders, NgModule } from "@angular/core";

// import { AppComponent } from './app.component';
import { HelloWorldComponent } from "./hello-world/hello-world.component";

@NgModule({
  declarations: [
    // AppComponent,
    HelloWorldComponent,
  ],
  imports: [BrowserModule],
  providers: [],
  bootstrap: [HelloWorldComponent],
})
export class HelloModule {
  static forRoot(): ModuleWithProviders<HelloModule> {
    return {
      ngModule: HelloModule,
      providers: [],
    };
  }
}
