import { Subject } from "rxjs";
import { Person } from "shared/person/person";

export class ObservablesDataTransferService {
  persons = new Array();
  person: Person;
  personAdded = new Subject<Person>();

  addPerson(person: Person) {
    this.persons.push(person);
    this.person = person;
  }

  getPersons() {
    return this.persons;
  }

  getLastAddedPerson() {
    return this.person;
  }
}
