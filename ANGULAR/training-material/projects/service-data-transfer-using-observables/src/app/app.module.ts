import { BrowserModule } from "@angular/platform-browser";
import { ModuleWithProviders, NgModule } from "@angular/core";

// import { AppRoutingModule } from './app-routing.module';
import { ServiceDataTransferObservablesPracticeComponent } from "./service-data-transfer-observables.component";
import { SampleForm3PracticeComponent } from "./form/form.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ReactiveFormsModule } from "@angular/forms";
import { MatCardModule } from "@angular/material/card";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatTableModule } from "@angular/material/table";
import { FlexLayoutModule, FlexModule } from "@angular/flex-layout";

@NgModule({
  declarations: [
    ServiceDataTransferObservablesPracticeComponent,
    SampleForm3PracticeComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    FlexModule,
    FlexLayoutModule,
    // AppRoutingModule
  ],
  providers: [],
  bootstrap: [ServiceDataTransferObservablesPracticeComponent],
})
export class ServiceDataTransferObservablesModule {
  static forRoot(): ModuleWithProviders<ServiceDataTransferObservablesModule> {
    return {
      ngModule: ServiceDataTransferObservablesModule,
      providers: [],
    };
  }
}
