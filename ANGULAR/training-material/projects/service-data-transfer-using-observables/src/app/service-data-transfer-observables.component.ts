import { Component, OnDestroy, OnInit } from "@angular/core";
import { Subscription } from "rxjs";
import { Person } from "shared/person/person";
import { MatTableDataSource } from "@angular/material/table";
import { ObservablesDataTransferService } from "./service-data-transfer-observables.service";

@Component({
  selector: "app-root",
  templateUrl: "service-data-transfer-observables.component.html",
  styleUrls: ["service-data-transfer-observables.component.scss"],
  providers: [ObservablesDataTransferService],
})
export class ServiceDataTransferObservablesPracticeComponent
  implements OnInit, OnDestroy {
  personElement: Person;
  persons: Person[];
  dsPersons: MatTableDataSource<Person>;
  dcPersons: string[] = ["first_name", "last_name", "age"];

  private isPersonAdded: Subscription;

  constructor(
    private observablesDataTransferService: ObservablesDataTransferService
  ) {
    this.personElement = new Person("AA", "BB", 33);
    // this.persons = this.observablesDataTransferService.getPersons();
    this.dsPersons = new MatTableDataSource<Person>();

    this.isPersonAdded = this.observablesDataTransferService.personAdded.subscribe(
      (person: Person) => {
        console.log(this);
        this.personElement = person;
        this.persons = this.observablesDataTransferService.getPersons();
        this.dsPersons.data = this.persons;
      }
    );
  }

  subFunc(person: Person) {
    console.log(this);
    this.personElement = person;
    this.persons = this.observablesDataTransferService.getPersons();
    this.dsPersons.data = this.persons;
  }

  ngOnDestroy(): void {
    this.isPersonAdded.unsubscribe();
  }

  ngOnInit(): void {
    this.dsPersons.data = this.persons;
  }
}
