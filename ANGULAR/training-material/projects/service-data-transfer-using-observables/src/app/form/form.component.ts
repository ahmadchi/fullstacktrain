import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Person } from "shared/person/person";
import { ObservablesDataTransferService } from "../service-data-transfer-observables.service";

@Component({
  selector: "sample-form-3",
  templateUrl: "form.component.html",
  styleUrls: ["form.component.scss"],
})
export class SampleForm3PracticeComponent implements OnInit {
  cForm: FormGroup;
  fname: string;
  lname: string;
  age: number;
  person: Person;

  constructor(
    private _fb: FormBuilder,
    private observablesDataTransferService: ObservablesDataTransferService
  ) {
    this.fname = "";
    this.lname = "";
    this.age = null;
    this.person = new Person("AA", "BB", 44);
  }

  ngOnInit(): void {
    this.cForm = this._fb.group({
      fname: ["", Validators.required],
      lname: ["", Validators.required],
      age: [null, Validators.required],
    });
  }

  onSave() {
    this.person.first_name = this.cForm.value["fname"];
    this.person.last_name = this.cForm.value["lname"];
    this.person.age = this.cForm.value["age"];
    this.observablesDataTransferService.addPerson(this.person);
    this.observablesDataTransferService.personAdded.next(this.person);
  }
}
