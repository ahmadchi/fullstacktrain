import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { ModuleWithProviders } from "@angular/core";

// import { AppRoutingModule } from "./app-routing.module";
import { BasicServicePracticeComponent } from "./basic-service.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ReactiveFormsModule } from "@angular/forms";
import { MatCardModule } from "@angular/material/card";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatTableModule } from "@angular/material/table";
import { FlexLayoutModule, FlexModule } from "@angular/flex-layout";
import { MatButtonModule } from "@angular/material/button";
import { FormSharedModule } from "shared/form/module";
// import { HelloModule } from "projects/T001-basic-angular/src/app/app.module";
// import { dModule } from "shared/form/module";

@NgModule({
  declarations: [BasicServicePracticeComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormSharedModule,
    // HelloModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatButtonModule,
    FlexModule,
    FlexLayoutModule,
    // AppRoutingModule,
  ],
  providers: [],
  bootstrap: [BasicServicePracticeComponent],
})
export class BasicServiceModule {
  static forRoot(): ModuleWithProviders<BasicServiceModule> {
    return {
      ngModule: BasicServiceModule,
      providers: [],
    };
  }
}
