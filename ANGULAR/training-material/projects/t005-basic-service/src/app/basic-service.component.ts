import { Component, OnInit } from "@angular/core";
import { MatTableDataSource } from "@angular/material/table";
import { Person } from "shared/person/person";
import { BasicService1 } from "./basic-service.service";

@Component({
  selector: "app-root",
  templateUrl: "basic-service.component.html",
  styleUrls: ["basic-service.component.scss"],
  providers: [BasicService1],
})
export class BasicServicePracticeComponent implements OnInit {
  dsPersons: MatTableDataSource<Person>;
  dcPersons: string[] = ["first_name", "last_name", "age"];

  constructor(private basicService: BasicService1) {
    this.dsPersons = new MatTableDataSource<Person>();
  }

  ngOnInit(): void {
    this.dsPersons.data = this.basicService.getPersons();
  }

  onPersonAdded2(person: Person) {
    console.log("new Person added", person);
    this.dsPersons.data = this.basicService.getPersons();
    console.log(this.dsPersons.data);
  }
}
