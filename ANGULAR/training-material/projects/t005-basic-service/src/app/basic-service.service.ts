import { Person } from "shared/person/person";

export class BasicService1 {
  persons = new Array();

  addPerson(person: Person) {
    this.persons.push(person);
  }

  getPersons() {
    return this.persons;
  }
}
