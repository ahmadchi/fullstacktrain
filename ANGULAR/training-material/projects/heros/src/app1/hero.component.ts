import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./hero.component.html",
  styleUrls: ["./hero.component.css"],
})
export class HeroComponent {
  title = "heros";
}
