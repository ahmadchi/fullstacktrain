import { BrowserModule } from "@angular/platform-browser";
import { ModuleWithProviders, NgModule } from "@angular/core";

import { HeroComponent } from "./hero.component";

@NgModule({
  declarations: [HeroComponent],
  imports: [BrowserModule],
  providers: [],
  bootstrap: [HeroComponent],
})
export class HeroModule {
  static forRoot(): ModuleWithProviders<HeroModule> {
    return {
      ngModule: HeroModule,
      providers: [],
    };
  }
}
