import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/material_modules/material_module';
import { SampleFormPracticeComponent } from './form.component';



@NgModule({
    declarations : [SampleFormPracticeComponent],
    imports: [
        ReactiveFormsModule,
        MaterialModule
    ],
    exports: [
    SampleFormPracticeComponent
        
    ]
})
export class FormSharedModule {}
