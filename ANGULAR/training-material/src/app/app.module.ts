import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NaviagtionComponent } from "./naviagtion/naviagtion.component";
import { MaterialModule } from "./material_modules/material_module";
import { RouterModule, Routes } from "@angular/router";
import { BasicServicePracticeComponent } from "projects/t005-basic-service/src/app/basic-service.component";
//import { BasicServiceSharedModule } from "projects/t005-basic-service/src/app/app.module";
import { BasicServiceModule } from "projects/t005-basic-service/src/app/app.module";
import { DataBindingModule } from "projects/data-binding/src/app/app.module";
import { ServiceDataTransferObservablesModule } from "projects/service-data-transfer-using-observables/src/app/app.module";
import { ServiceDataTransferObservablesPracticeComponent } from "projects/service-data-transfer-using-observables/src/app/service-data-transfer-observables.component";
import { DataBindingPracticeComponent } from "projects/data-binding/src/app/data-binding.component";
import { HeroComponent } from "projects/heros/src/app/app.component";
import { HeroModule } from "projects/heros/src/app/app.module";
import { HelloModule } from "projects/T001-basic-angular/src/app/app.module";
import { HelloWorldComponent } from "projects/T001-basic-angular/src/app/hello-world/hello-world.component";

const routes: Routes = [
  {
    path: "hello",
    component: HelloWorldComponent,
  },
  {
    path: "heroproject",
    component: HeroComponent,
  },
  {
    path: "services2",
    component: BasicServicePracticeComponent,
  },
  {
    path: "dbinding",
    component: DataBindingPracticeComponent,
  },
  {
    path: "data-transfer-observables",
    component: ServiceDataTransferObservablesPracticeComponent,
  },
];

@NgModule({
  declarations: [AppComponent, NaviagtionComponent],
  imports: [
    BrowserModule,
    MaterialModule,
    BrowserAnimationsModule,
    // BasicServiceSharedModule.forRoot(),
    HelloModule.forRoot(),
    HeroModule.forRoot(),
    BasicServiceModule.forRoot(),
    DataBindingModule.forRoot(),
    ServiceDataTransferObservablesModule.forRoot(),
    RouterModule.forRoot(routes),
  ],

  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
