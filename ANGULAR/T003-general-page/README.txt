
Forms mature, (time, date, validation, )
Table mature (filtering, sorting, formating)
Services
Routing, Route Guarding, Lazy loading
Material

(keydown)="numberOnly($event)"


          <div *ngIf="theForm.get(field.name).errors.pattern">
            Digits are not allowed.
          </div>

          <div *ngIf="theForm.get(field.name).errors.minlength">
            {{ field.title }} must be minimum of
            {{ field.minLength }} characters.
          </div>

===========================================
- Static table/form
- Dynamic table/form
- Services
- Form validation
- New form fields
- make two copies of general page in the root module
- Add filtering in General Table
- Route
=======================================
- get data from HTTP / database
- pass filters to http, get filtered data
==========================================





12:10 start
12:17 hierarchy working, hard coded table working
12:31: break
12:35: hard coded 2 person defined.
12:45: 

Problem Statement:
Make a table which displays data captured in a form.

- Planning / Strategy
  parent, two childs, data transfer via input/output
- first data array/store in parent
- ngOnInit data populated in table
- data emitted from form, caught in parent
- data of form sent to the other child (table)
  > 


Life cycle of comp:
	- constructor,
	- ngOnChanges(Input),
	- ngOnInit(after input, initialization),
	- ngAfterViewInit (after this we can use: getDocumentbyId)
	- ngOnDestroy

INPUT:
  Parent sends data to child
  @Input() tableData: any[];
  
  <app-general-table [childData]="parentData"> </app-general-table>

OUTPUT: 
    - To send data from child to parent (received in function in parent)
	- import {Output, EventEmitter } from '@angular/core';
	- @Output() signal = new EventEmitter();
	- this.signal.emit(dataTobeEmitted)
	 <app-general-form (signal)="onDataEmitted($event)"> </app-general-form>

Reactive forms 40%
	- Module import in app.module.ts 
	- add html form/fields standard 
	- in ts add "form:FormGroup;"
		this.form = new FormGroup({
		name1: new FormControl(),
		age1: new FormControl(),
		education1: new FormControl(),
		}); 
	- bind form group to html <form [formGroup]="form">
	- bind formControlName with each input field
		<input .... formControlName="name1"/><br /><br />

	- validator
	- custom validator
	- Adding a field/control dynamically
	- observable, for realtime observation to take action, e.g. search