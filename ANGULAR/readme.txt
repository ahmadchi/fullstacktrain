
ng new app  //new workspace
ng genernate application //new project
ng generate component //new component (in app directory)
ng generate module app-routing --flat --module=app

<div *ngIf="selectedHero">
<li *ngFor="let hero of heroes" (click)="onSelect(hero)">

- If we need to display something in Angular, we make a component.
- The selector of the component is defined in parent component.

=============================================

yarn install
yarn start //// ng serve
ng serve --project=project_name
fuser -k 4200/tcp


<sample-form fxFlex="50" (personAdded1)="onPersonAdded2()" [selectedPerson]="personElement"></sample-form>

@Output() personAdded1 = new EventEmitter<Person>();
@Input() selectedPerson: Person;

(output) = funcInParent()   <<<<<< Event binding (output) 
[variableInChild] = variableInParent   <<<<<< Property binding 
value="{{ selectedPerson.last_name }}"   string interpolation
[(ngModel)]="selectedPerson.last_name " << two way binding

=======================================
Started project from scratch
  app/counter/...
  declarations: [AppComponent, CounterComponent],

  in app.component.html <app-counter></app-counter>


====================================================

@Injectable({
  providedIn: 'root',
})


