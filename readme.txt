test abc
mysql -u root -p    --- does not work
sudo mysql -u root -p   -- works



- join query (ORM, select.join, text)

- using CHI base code
- Angular 


T000 Python basics
	- list, dictionary
	- classes, inheritance
	- args, kwargs

T001_alchemy_basics
    - Using direct tables
    - Using ORM

T002_Tornado_basics
    - Simple server
    - App()

T003_api_alchemy
    - Using complete structure of controller, model
    - HTML page render and then update table from DB data
    - adding input fields in HTML for adding a row in user table (Assignment)
    - rendering from Python templates (assignment)

Monday: Alchemy, Tornado  
Tuesday: BaseController/CodeStructure, Templates/Front end integration
Wednesday: Joins.
    - just look at the desired report (see what columns are present)
    - Normally, one of the tables columns would be dominant
    - Write the query function in the dominant table controller
        e.g. invoices table is dominant in case of invoice/customer example
        in normal sql, we would write select **** from invoices join customer ...
        in alchemy,  session.query(Invoice, Customer).outjoin(Customer).all()
           outerjoin will give those invoices also which have NULL customer
           join will give only those rows which havea valid customer
    - basic intro to CHI backend, and used the existing table "formulas"
        > added two entries in formula table by PHPadmin
        > used postman to call the list API

Thurday:
    - Made Customer and invoice table in CHI backend
       > implemented readInvoices (which implemented a join Invoice->Customer)
       > wrote a main() function which can call the readInvoices directly
       > ... the readInvoices() is an ASYNC function.
    - Will try to use CHI angular library to insert entry and show list
    - Made a custom page for Customers and Invoices.


    


Questions
- How does alchemy deal with the dictionary we are passing as **{}

- How does BASE know which classes are inherited from it, and how does it access its variable and their names, etc.


=================================================================
add(c1)
add_all([])
query(Customers).all()
query(Customers).get(2)  //id

x = session.query(Customers).first()

UPDATE
=======
query(Customers).filter(Customers.id! = 2).
update({Customers.name:"Mr."+Customers.name}, synchronize_session = False)

synchronize_session ?? read again

FILTERS
=======
result = session.query(Customers).filter(Customers.id>2)
result = session.query(Customers).filter(Customers.name.like('Ra%'))
result = session.query(Customers).filter(Customers.id.in_([1,3]))
Result = session.query(Customers).filter(Customers.id>2, Customers.name.like('Ra%'))

from sqlalchemy import or_
result = session.query(Customers).filter(or_(Customers.id>2, Customers.name.like('Ra%')))

one()  -- ???

scalar() -- ??

session.query(Customers).from_statement(text("SELECT * FROM customers")).all()
=============================================
BACKEND CHI CODEBASE TRAINING
=============================================
Steps to make tables / handlers, etc in CHI code.

- Model
  chi/data/mymodels/modelfile(s).py

  whenever we add a model, we need to run the setup_server.py file
  which drops the database and creates tables again.

- Controller
  chi/modules/controller(s).py
  Just override CHIBaseController and implement required methods
  e.g. customerController, invoiceController.

  To add another custom method, just:
    self.PublicMethods = ['readInvoices']  # customAPI
=============================================
BACKEND CHI CODEBASE TRAINING
=============================================
- projects/charms-dev/src/app/pages/........
  Add a component 
  Import component in module.ts
  (For general page : Create config) 
  Pass config to table in component.ts
  Add path in module.ts for respective component
  Import respective mnodule in app.routing.module.ts 
  app.routing.module.ts will have the first part of the route
  the second part of the route will be defined in the module's respective module.ts
  navigation

Frontend:
=========
